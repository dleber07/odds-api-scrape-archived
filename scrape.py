import components.validate as validate
from components.ModuleManager import ModuleManager
import components.writer as writer
import settings
import time

# Instance of ModuleManager must be persistant
module_manager = ModuleManager()

def scrape(redisdb, params, verboseprint):

    status = {
        'error': '',
        'fetcher_start': time.time(),
        'raw_events_cnt': 0,
        'valid_events_cnt': 0,
    }

    scrape_config = params['scrape_config']
    sport_config = params['sport_config']
    site_config = params['site_config']
    scrape_key = scrape_config['key']

    params = {
        'sport_config': sport_config,
        'site_config': site_config,
        'scrape_config': scrape_config,
    }

    # TODO could fetcher be a generator? if looping through events pages?
    modules = module_manager.get_modules(scrape_config['fetcher'], scrape_config['reader'])

    if not modules:
        verboseprint(scrape_key, 'unable to load modules')
        status['error'] = 'Unable to import ' + scrape_config['fetcher'] + ' or ' + scrape_config['reader']
        finish(redisdb, sport_config, scrape_config, status)
        return

    fetcher = modules['fetcher']
    reader = modules['reader']

    try:
        response = fetcher.fetch(site_config, scrape_config)
    except Exception as e:
        verboseprint(scrape_key, 'fetcher failed ' + str(e))
        status['error'] = 'fetcher failed ' + str(e)
        finish(redisdb, sport_config, scrape_config, status)
        return

    status['reader_start'] = time.time()
    try:
        events = getattr(reader, scrape_config['reader_method'])(response, params, False)
    except Exception as e:
        verboseprint(scrape_key, 'reader failed ' + str(e))
        status['error'] = 'reader failed ' + str(e)
        finish(redisdb, sport_config, scrape_config, status)
        return

    status['raw_events_cnt'] = len(events)
    status['validation_start'] = time.time()
    verboseprint(scrape_key, 'raw events cnt', status['raw_events_cnt'])

    valid_events = validate.validateEvents(params, events)
    status['valid_events_cnt'] = len(valid_events)
    verboseprint(scrape_key, 'valid events cnt', status['valid_events_cnt'])

    if site_config['event_creator'] and status['valid_events_cnt']:
        update_next_to_go_time(redisdb, sport_config['key'], valid_events)
        verboseprint(scrape_key, 'updated next_to_go_time')

    if status['valid_events_cnt'] > 0:
        status['zero_event_cnt'] = 0

        # Write events
        status['writer_start'] = time.time()
        try:
            write_result = writer.write_db(params, valid_events)
        except Exception as e:
            verboseprint(scrape_key, 'db write failed ' + str(e))
            status['error'] = 'db write failed ' + str(e)
            finish(redisdb, sport_config, scrape_config, status)
            return

    finish(redisdb, sport_config, scrape_config, status)


def finish(redisdb, sport_config, scrape_config, status):
    scrape_key = scrape_config['key']

    if sport_config.get('sleeping', False):
        # verboseprint(scrape_key, 'sport is sleeping. Not touching interval')
        pass
    else:
        status['interval'] = get_interval(redisdb, status, sport_config, scrape_config)
        # verboseprint(scrape_key, 'calculated interval', status['interval'])

    status['scrape_end'] = time.time()
    write_status(redisdb, scrape_key, status)

def write_status(redisdb, scrape_key, status):
    redisdb.hmset('status:' + scrape_key, status)
    redisdb.set('last_scrape_write', time.time())

def zero_event_cnt(redisdb, scrape_key):
    return float(redisdb.hincrby('status:' + scrape_key, 'zero_event_cnt') or '0')

def update_next_to_go_time(redisdb, sport, events):
    # NOTE assumes events is ordered
    next_to_go_time = events[0]['commence_time']

    existing_next_to_go_time = float(redisdb.get('next_to_go:' + sport) or '0')
    if existing_next_to_go_time and existing_next_to_go_time < next_to_go_time:
        # if a site doesn't list live events, prevent overwritting of next to go
        return

    expiry_sec = settings.NEXT_TO_GO_EXPIRY
    redisdb.setex('next_to_go:' + sport, expiry_sec, next_to_go_time)

def get_interval(redisdb, status, sport_config, scrape_config):

    if status['valid_events_cnt'] == 0:

        zero_events = zero_event_cnt(redisdb, scrape_config['key'])

        if zero_events >= settings.VERY_SLOW_SCRAPE_ZERO_EVENT_THRESHOLD:
            return settings.VERY_SLOW_SCRAPE_SECONDS

        if zero_events >= settings.SLOW_SCRAPE_ZERO_EVENT_THRESHOLD:
            return settings.SLOW_SCRAPE_SECONDS

    sport = sport_config['key']

    next_to_go_time = float(redisdb.get('next_to_go:' + sport) or '0')

    if not next_to_go_time:
        return scrape_config['interval_max']

    if time.time() < (next_to_go_time - settings.FAST_SCRAPE_PREMATCH_SECONDS):
        # match is far away
        return scrape_config['interval_max']

    if time.time() > next_to_go_time:
        # match has started
        return scrape_config['interval_min']

    f = float((next_to_go_time - time.time()) / settings.FAST_SCRAPE_PREMATCH_SECONDS)
    return round(scrape_config['interval_min'] + f * (scrape_config['interval_max'] - scrape_config['interval_min']), 1)
