
from connectors.mongo_connect import Mongo
from bson import CodecOptions, SON
from collections import OrderedDict

collection = Mongo['test'].test

doc = OrderedDict()
doc['field1'] = 123
doc['field2'] = 123
doc['field3'] = 123
doc['field4'] = 123


collection.insert(doc)

"""
    {'fiedl1', 'hello'},
    {'fiedl2', 'hello'},
    {'fiedl3', 'hello'},
    {'fiedl4', 'hello'},
    {'fiedl5', 'hello'},
"""
