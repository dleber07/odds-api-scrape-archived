import importlib
from components.Configs import Configs
import components.validate as validate
from snip.mongo_connect import Mongo
import settings
import json
import click

@click.command()
@click.option('--sport', '-sp', prompt='Sport', help="Example basketball_nba",)
@click.option('--site', '-si', prompt='Site', help="Example ubet")
@click.option('--from-file', '-ff', default=False, help='Get cached url response from file instead of sending request')
@click.option('--validate-param', '-v', default=True, help='Validate events against known list')
def main(sport, site, from_file, validate_param):
    
    validate_events = (validate_param == True)
    write_events = False
    fetch_from_file = from_file

    sport_config_override = {}
    site_config_override = {
        # 'proxy': {
        #     'http': 'http://lum-customer-hl_c3cedc8d-zone-static-country-gb:i1smdtt3bxl3@zproxy.lum-superproxy.io:22225'
        # }
    }
    scrape_config_override = {}
    #category_ids=["71955b54-62f6-4ac5-abaa-df88cad0aeef"]
    response_file_path = 'snip/response.txt'

    configs = Configs()
    configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

    sport_config = configs.get_sports().get(sport, {}) or {}
    site_config = configs.get_sites().get(site, {}) or {}
    scrape_config = configs.get_scrape(sport + ':' + site) or {}

    sport_config.update(sport_config_override)
    site_config.update(site_config_override)
    scrape_config.update(scrape_config_override)

    # scrape_config['url'] = 'http://icanhazip.com'
    # scrape_config['fetcher'] = 'default'

    # TODO considr google ua
    #site_config['User-Agent'] = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'

    if not site_config:
        print 'Site config empty (headers missing). Site probably has active==false. Exiting'
        exit()

    if scrape_config is None:
        print 'No scrape config for ', sport, site
        exit()

    fetcher = importlib.import_module('fetchers.' + scrape_config.get('fetcher', 'default'))
    reader = importlib.import_module('readers.' + scrape_config.get('reader', site))

    if fetch_from_file:
        with open(response_file_path, 'r') as myfile:
            response = myfile.read().replace('\n', '')
    else:
        response = fetcher.fetch(site_config, scrape_config)


    if not fetch_from_file:
        with open(response_file_path, 'w') as text_file:
            text_file.write(response.encode('utf-8'))
        print
        print 'Wrote response to file. Feel free to use in future reqs'
        print
    else:
        print
        print 'Fetched html/json from file'
        print

    params = {
        'sport_config': sport_config,
        'site_config': site_config,
        'scrape_config': scrape_config,
    }

    # Raw Scrape Result
    events = getattr(reader, scrape_config['reader_method'])(response, params, True)
    print
    print 'Raw:', len(events)
    for event in events:
        print event

    if validate_events:


        valid_cnt = 0
        valid_events = [] 

        for event in events:
            print
            print('Raw', event['teams'])

            valid_event = validate.validateEvent(params, event)
            if valid_event:
                valid_cnt += 1
                valid_events.append(valid_event)
                print('Valid', valid_event['teams'])
            else:
                print('Invalid')

        print
        print('Successfully validated:', valid_cnt, 'out of ', len(events))
        print


    else:
        valid_events = events

    if validate_events:

        print
        print 'Validated events'
        print valid_events

        from snip.order import compare_results
        compare_results(valid_events, sport)


if __name__ == "__main__":
    main()