from fuzzywuzzy import process

score_cutoff = 75

choices = ["Sevilla","Arsenal","Chelsea","Zenit","Leverkusen","Dynamo Kyiv","Beşiktaş","Salzburg","Olympiacos","Villarreal","Anderlecht","Lazio","Sporting CP","Ludogorets","København","Marseille","Celtic","PAOK","AC Milan","Genk","Fenerbahçe","Krasnodar","Astana","Rapid Wien","Real Betis","BATE Borisov","Qarabağ","Dinamo Zagreb","RB Leipzig","Eintracht Frankfurt","Malmö","Spartak Moskva","Standard Liège","Zürich","Bordeaux","Rennes","Apollon","Rosenborg","Vorskla","Slavia Praha","Akhisar Belediyespor","Jablonec","AEK Larnaca","Vidi","Rangers","Dudelange","Spartak Trnava","Sarpsborg","FC Zurich"]

pnames = ["FK Karabakh","AC Milan","AEK Athens","AEK Larnaca","Akhisar Bld Spor","Apollon Limassol","Arsenal","Atalanta","Atletico Madrid","Austria Vienna","BATE Borisov","Baumit Jablonec","Bayer Leverkusen","Besiktas","Betis","Borussia Dortmund","CSKA Moscow","Celtic","Crvena Zvezda","Dinamo Zagreb","Dynamo Kiev","Eintracht Frankfurt","Everton","F91 Dudelange","FC Astana","FC Copenhagen","FC Koln","FC Zurich","FK Krasnodar","Fastav Zlin","Fenerbahce","Genk","HNK Rijeka","Hapoel Beer Sheva","Hertha Berlin","Hoffenheim","Konyaspor","Lazio","Lokomotiv Moscow","Lugano","Lyon","Maccabi Tel Aviv","Malmo","Marseille","Napoli","Nice","Olympiacos","Ostersunds","PAOK","Partizan Belgrade","RB Leipzig","Rapid Vienna","Real Sociedad","Red Bull Salzburg","Rennes","Rosenborg","Sevilla","Sheriff Tiraspol","Slavia Prague","Spartak Moscow","Spartak Trnava","Sporting Lisbon","Vardar Skopje","Villarreal","Vitesse Arnhem","Vitoria Guimaraes","Vorskla Poltava","Young Boys","Zenit St Petersburg","Zorya Lugansk"]

for pname in pnames:
    print( pname, process.extractOne(pname, choices, score_cutoff=score_cutoff) )


