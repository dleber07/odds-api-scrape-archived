import importlib
from components.Configs import Configs
import components.validate as validate
from test.mongo_connect import Mongo
import settings
import json
import scrape
from connectors.redis_connect import get_redis_client
from components.verboseprint import verboseprint

sport = 'americanfootball_nfl'
site = 'nitrogensports'

fetch_from_file = False
validate_events = False
write_events = False

sport_config_override = {}
site_config_override = {}
scrape_config_override = {}



configs = Configs()
configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

sport_config = configs.get_sports().get(sport, {}) or {}
site_config = configs.get_sites().get(site, {}) or {}
scrape_config = configs.get_scrape(sport + ':' + site) or {}


sport_config.update(sport_config_override)
site_config.update(site_config_override)
scrape_config.update(scrape_config_override)

params = {
    'sport_config': sport_config,
    'site_config': site_config,
    'scrape_config': scrape_config
}

scrape.scrape(get_redis_client(db=settings.REDIS_STATUS_DB), params, verboseprint)