import requests

#url = 'https://whatismyipaddress.com/proxy-check'
url = 'http://sports.williamhill.com/bet/en-gb/betting/t/295/English+Premier+League.html'
#url = 'http://www.xhaus.com/headers'

proxy = {
    #'https': '35.178.0.184:8888' # aws london
    #'http': '139.162.235.163:31028' # someother london from the internet
    'http': '35.176.226.194:8888'
}

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'
}

cookies = {
    'cust_prefs': 'en|DECIMAL|form|TYPE|PRICE|||0|SB|0|0||0|en|0|TIME|TYPE|0|1|A|0||0|0||TYPE||-|0'
}

r = requests.get(
    url,
    timeout=20,
    headers=headers,
    proxies=proxy,
    allow_redirects=False,
    #cookies= cookies
)

outfile = 'test/proxyresponse.txt'
with open(outfile, 'w') as text_file:
    text_file.write(r.text.encode('utf-8'))

print 'Wrote to ', outfile
