from components.Configs import Configs
from scrape import scrape
from connectors.redis_connect import get_redis_client
from connectors.mongo_connect import Mongo
import settings
from components.verboseprint import verboseprint

sport = 'soccer_england_league2'
site = 'betfair'

scrape_key = '%s:%s' % (sport, site)
redisdb = get_redis_client(db=settings.REDIS_STATUS_DB)

configs = Configs() # will only laod from json, To load from mongo, set db
configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

params = {
    'sport_config': configs.get_sports()[sport],
    'site_config': configs.get_sites()[site],
    'scrape_config': configs.get_scrape(scrape_key),
}

scrape(redisdb, params, verboseprint)
