import importlib
from components.Configs import Configs

sport = 'soccer_england_league1'
site = 'bet365'

#configs = Configs()
#sport_config = configs.get_sports()[sport]
#site_config = configs.get_sites()[site]
#scrape_config = configs.get_scrape(sport + ':' + site)


site_config = {
    "active":True,
    "key":"bet365",
    "display":"Bet365",
    "event_creator":False,
    "regions":"au,uk,us",
    "machine":"SIP1",
    "headers":{},
    "cookies":'',
    "ua":"chrome",
    "proxy":None
}

scrape_config = {
    "active": True,
    "key":"soccer_italy_serie_a:bet365",
    "sport":"soccer_italy_serie_a",
    "site":"bet365",
    "url":"https://www.bet365.com.au/dl/1",
    "custom1":"Full Time Result_United Kingdom_England Premier League",
    "fetcher":"bet365",
    "reader":"bet365",
    "reader_method":"bet365_23",
    "interval_min":40,
    "interval_max":400,
    "participantmapname":""
}


if scrape_config is None:
    print 'No scrape config for ', sport, site
    exit()

fetcher = importlib.import_module('fetchers.' + scrape_config.get('fetcher', 'default'))
print fetcher.fetch(site_config, scrape_config)
