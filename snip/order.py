import requests
import json
from env import API_KEY

def compare_results(test_events, sport):
    """
        May fail since test_events not validated (matching events from different times,)

    """

    print 
    print 'Testing order of results...'


    # Accept if within + or - this % of mean odds
    url = 'https://api.the-odds-api.com/v3/stats?sport={sport}&apiKey={apiKey}'.format(sport=sport, apiKey=API_KEY)
    r = requests.get(url, headers={'Content-type': 'application/json'})

    if r.status_code != 200:
        print 'Order test failed: Odds API returned status code', r.status_code, r.text
        return
    
    stat_events = json.loads(r.text)['data']

    print
    print
    print '***  COMPARE ODDS H2H ORDER  ***'
    compare_order(stat_events, test_events)

    print
    print
    print '***  COMPARE COMMENCE TIME  ***'
    compare_commence(stat_events, test_events)

    print
    print
    print '***  COMPARE HOME TEAM  ***'
    compare_hometeam(stat_events, test_events)

def compare_order(stat_events, test_events):
    threshold = 0.2

    results = {
        'pass': 0,
        'fail': 0,
        'missing': 0
    }

    # Create a dict like (h2h only): event_name: [odd1, odd2, oddD] 
    stats = {}
    for event in stat_events:
        try:
            stats['_'.join( event['teams']).lower() ] = [ event['stats']['h2h'][i]['mean'] for i, statdict in enumerate(event['stats']['h2h']) ]
        except Exception as e:
            print 'No stats found for ', '_'.join( event['teams']).lower()
            pass


    for event in test_events:

        if not event['odds'].get('h2h'):
            print 'No h2h found for ',  '_'.join(event['teams']) , 'in test set'
            continue

        if event['teams'][0] < event['teams'][1]:
            event_name =  '_'.join(event['teams']) 
            odds_h2h = event['odds']['h2h']
        else:
            odd2 = event['odds']['h2h'][0]
            event['odds']['h2h'][0] = event['odds']['h2h'][1]
            event['odds']['h2h'][1] = odd2
            event_name = '_'.join( reversed(event['teams']))
            odds_h2h = event['odds']['h2h']

        event_name = event_name.lower()

        if event_name not in stats:
            print '\033[95mMISSING\033[0m event in stats', event_name
            results['missing'] += 1
            continue
        
        failed = False
        for i, odd in enumerate(odds_h2h):

            if float(odd) / stats[event_name][i] - 1 > threshold:
                print '\033[93mFAIL\033[0m h2h stat test for event', event_name
                failed = True
                results['fail'] += 1
                break
            
        if not failed:
            print '\033[92mPASS\033[0m h2h stat test for event', event_name
            results['pass'] += 1

    print results


def compare_commence(stat_events, test_events):
    
    # maximum permissable commence time diff in seconds
    threshold = 360 * 60 # try to match with COMMENCE_TIME_TOLERANCE_MINS in settings.py

    print 'Threshold within ', round(threshold / 60, 1), 'mins'

    results = {
        'pass': 0,
        'fail': 0,
        'missing': 0
    }

    # Create a dict like (h2h only): event_name: [odd1, odd2, oddD] 
    stats = {}
    for event in stat_events:
        try:
            stats['_'.join( event['teams']).lower() ] = event['commence_time']
        except Exception as e:
            print 'No stats found for ', '_'.join( event['teams']).lower()
            pass


    for event in test_events:

        if not event.get('commence_time'):
            print 'No commence_time found for ',  '_'.join(event['teams']) , 'in test set'
            continue

        if event['teams'][0] < event['teams'][1]:
            event_name =  '_'.join(event['teams']) 

        else:
            event_name = '_'.join( reversed(event['teams']))

        event_name = event_name.lower()

        if event_name not in stats:
            print '\033[95mMISSING\033[0m event in stats', event_name
            results['missing'] += 1
            continue

        commence_diff_mins = abs(event['commence_time'] - stats[event_name]) / 60
        if abs(event['commence_time'] - stats[event_name]) > threshold:
            print '\033[93mFAIL\033[0m h2h stat test for event', event_name, '| Diff mins: ', commence_diff_mins
            results['fail'] += 1
            continue
            
        print '\033[92mPASS\033[0m h2h stat test for event', event_name, '| Diff mins: ', commence_diff_mins
        results['pass'] += 1

    print results




def compare_hometeam(stat_events, test_events):
    
    results = {
        'pass': 0,
        'fail': 0,
        'missing': 0
    }

    # Create a dict like (h2h only): event_name: [odd1, odd2, oddD] 
    stats = {}
    for event in stat_events:
        try:
            stats['_'.join( event['teams']).lower() ] = event['home_team']
        except Exception as e:
            print 'No stats found for ', '_'.join( event['teams']).lower()
            pass


    for event in test_events:

        if not event.get('home_team'):
            print 'No home_team found for ',  '_'.join(event['teams']) , 'in test set'
            continue

        if event['teams'][0] < event['teams'][1]:
            event_name =  '_'.join(event['teams']) 

        else:
            event_name = '_'.join( reversed(event['teams']))

        event_name = event_name.lower()

        if event_name not in stats:
            print '\033[95mMISSING\033[0m event in stats', event_name
            results['missing'] += 1
            continue

        if event['home_team'] != stats[event_name]:
            print '\033[93mFAIL\033[0m', event_name, '| ', event['home_team'], ' != ', stats[event_name], '(expected)'
            results['fail'] += 1
            continue
            
        print '\033[92mPASS\033[0m for event', event_name
        results['pass'] += 1

    print results

