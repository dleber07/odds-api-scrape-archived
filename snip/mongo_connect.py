from env import MONGO_TEST
from pymongo import MongoClient

Mongo = MongoClient('mongodb://%s:%s@%s' % (MONGO_TEST['USER'], MONGO_TEST['PASS'], MONGO_TEST['HOST']), MONGO_TEST['PORT'], serverSelectionTimeoutMS=3000)