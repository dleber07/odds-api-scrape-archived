#!/usr/bin/python
import time
from components.Configs import Configs
from components.verboseprint import verboseprint
import settings
from scrape import scrape
import multiprocessing
from connectors.redis_connect import get_redis_client
from connectors.mongo_connect import Mongo
import copy
import traceback

try:
    import sentry_sdk
    sentry_sdk.init("https://5b588b4da17e46ee9e7b6cc4c9d9c3f0@sentry.io/1393961")
except:
    print 'Failed to import sentry sdk'


# These are whitelisted on scrape3
SITES_BLACKLIST = [
    'beteasy',
    'betfair',
    'betfred',
    # 'betonlineag',
    'betvictor',
    'bookmaker',
    'bovada',
    'ladbrokes',
    'neds',
    'playup',
    'pointsbetau',
    'pointsbetus',
    'bet365',
    'pinnacle',
]


def worker(q):
    # global lock # NOTE if you acquire lock, this should be configurable (i.e. do not lock for WH)

    while True:
        params = q.get()
        verboseprint(time.time(), params['scrape_config']['key'], 'pulled from queue, starting to scrape')

        try:
            scrape(redisdb, params, verboseprint)
        except:
            print 'Time: ', round(time.time(), 0)
            print traceback.format_exc()

        q.task_done()
        time.sleep(0.1)

def scrape_due(script_start, status):

    if not status:
        # The first run ever
        return True

    queue_added = float(status.get('queue_added', '0'))
    next_scrape = float(status.get('scrape_end', '0')) + float(status.get('interval', '0'))
    
    #if script_start > queue_added:
    #    # Script was restarted after being down for a while
    #    return True

    if queue_added > next_scrape and time.time() > (queue_added + 300):
        # Was added to queue ages ago and end_time still hasn't updated.
        # Assume something unexpected happend. You may have reset the script during a scrape.
        return True

    if queue_added > next_scrape:
        # Scrape in progress
        return False

    if time.time() > next_scrape:
        # Scrape is due
        return True

    # Scrape not due yet
    return False

if __name__ == '__main__':

    configs = Configs()
    configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

    procs = []
    lock = multiprocessing.Lock()
    q = multiprocessing.JoinableQueue(maxsize=settings.MAX_QUEUE_SIZE)
    redisdb = get_redis_client(db=settings.REDIS_STATUS_DB)

    script_start = time.time()

    for _ in range(settings.NUM_PROCESSES):
        procs.append(multiprocessing.Process(target=worker, args=(q,)))
        procs[-1].daemon = True
        procs[-1].start()

    while True:

        params = {}

        for sport in configs.get_sports():

            params['sport_config'] = configs.get_sports()[sport]
            for site in configs.get_sites():

                if site in SITES_BLACKLIST:
                    continue

                scrape_key = '%s:%s' % (sport, site)

                # NOTE to force refresh all, set script_start = time.time(). Could happen in response to external flag
                if not scrape_due(script_start, redisdb.hgetall('status:' + scrape_key)):
                    continue

                scrape_config = configs.get_scrape(scrape_key)
                if not scrape_config or not scrape_config['active']:
                    verboseprint(time.time(), sport, site, 'scrape not found or inactive')
                    continue

                verboseprint(time.time(), sport, site, 'scrape due. Adding to queue')

                params.update({
                    'site_config': configs.get_sites()[site],
                    'scrape_config': scrape_config
                })

                redisdb.hset('status:' + scrape_key, 'queue_added', time.time())
                q.put(copy.deepcopy(params))

        time.sleep(settings.SCHEDULER_SLEEP_INTERVAL)

    q.join()
