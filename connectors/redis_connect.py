import redis

try:
    from env import REDIS_1
except ImportError:
    print 'env.py not found. See readme.md for more info'
    exit()

def get_redis_client(host=REDIS_1['HOST'], port=REDIS_1['PORT'], db=0):
    return redis.StrictRedis(
        host=host,
        port=port,
        db=db
    )

