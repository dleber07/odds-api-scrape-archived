from pymongo import MongoClient

try:
    from env import MONGO_1
except ImportError:
    print 'env.py not found. See readme.md for more info'
    exit()

# NOTE
#   connect=False is added to prevent server connection on creation.
#   Otherwise the same connection will be copied across processes (not thread safe)

#if MONGO_1['ENV'] == 'dev':
#    Mongo = MongoClient(serverSelectionTimeoutMS=3000, connect=False)

if MONGO_1['USER'] and MONGO_1['PASS']:
    Mongo = MongoClient('mongodb://%s:%s@%s' % (MONGO_1['USER'], MONGO_1['PASS'], MONGO_1['HOST']), MONGO_1['PORT'], serverSelectionTimeoutMS=3000)
else:
    Mongo = MongoClient('mongodb://%s' % (MONGO_1['HOST']), MONGO_1['PORT'], serverSelectionTimeoutMS=3000)
