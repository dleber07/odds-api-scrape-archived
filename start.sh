#!/usr/bin/env bash

# To be run on remote

# Load latest config into mongo
# python -m extras.update_config

# Copy logs
cp logs/main.log logs/main_prev.log
cp logs/status_to_sheets.log logs/status_to_sheets_prev.log
cp logs/stats_to_firebase.log logs/stats_to_firebase_prev.log
cp logs/results.log logs/results_prev.log

#  pkill of any webdrivers that might be open
pkill -f google/chrome
pkill -f fetchers/chromedriver

# Restart main scrape
pkill -f "python -um main"
nohup python -um main > logs/main.log &

# Restart status to sheets
# pkill -f status_to_sheets
# nohup python -um extras.status_to_sheets > logs/status_to_sheets.log &

# Restart stats to Firebase (needed for predict app)
# pkill -f stats_to_firebase
# nohup python -um extras.predict.stats_to_firebase > logs/stats_to_firebase.log &

# Restart results (& scores) scrape
pkill -f "python -um results.main"
nohup python -um results.main > logs/results.log &