import requests
import default
from settings import REDIS_CACHE_DB
from connectors.redis_connect import get_redis_client

# Performs a standard get request
def fetch(site_config, scrape_config):
    
    cache_time = 30 # TODO consider storing in settings
    redisdb = get_redis_client(db=REDIS_CACHE_DB)

    # TODO cache key should include url instead of site key!
    key = 'xhr_response:' + site_config['key']
    response = redisdb.get(key)

    if not response:
        # Cache is empty. Send a request
        response = default.fetch(site_config, scrape_config)
        redisdb.setex(key, cache_time, response)

    return response
