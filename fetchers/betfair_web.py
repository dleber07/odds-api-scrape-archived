import requests
from . import default
import json
import time
from env import BETFAIR_LOGIN_USER, BETFAIR_LOGIN_PASS

"""
def _login():
    url = 'https://identitysso.betfair.com.au/api/login'

    headers = {
        'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding':'gzip, deflate, br',
        'Accept-Language':'en-GB,en-US;q=0.9,en;q=0.8',
        'Cache-Control':'max-age=0',
        'Connection':'keep-alive',
        'Content-Type':'application/x-www-form-urlencoded',
        'Host':'identitysso.betfair.com.au',
        'Origin':'https://identitysso.betfair.com.au',
        'Referer':'https://identitysso.betfair.com.au/view/login',
        'Upgrade-Insecure-Requests':'1',
        'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
    }

    payload = {
        'username': BETFAIR_LOGIN_USER,
        'password': BETFAIR_LOGIN_PASS,
        'redirectMethod':'POST',
        'product':'home.betfair.int',
        'url':'https://www.betfair.com/',
        'submitForm':'true',
    }

    session = requests.Session()
    session.post(url, headers=headers, data=payload)
"""

def _get_mkt_ids(headers, comp_ids):
    url = 'https://www.betfair.com.au/www/sports/navigation/facet/v1/search?_ak=nzIFcwyWhrlwYMrh&alt=json'

    payload = {
        "filter":{
            "marketBettingTypes":[
                "ASIAN_HANDICAP_SINGLE_LINE",
                "ASIAN_HANDICAP_DOUBLE_LINE",
                "ODDS"
            ],
            "productTypes":["EXCHANGE"],
            "marketTypeCodes":[
                "MATCH_ODDS",
                "MATCH_ODDS_UNMANAGED",
                "MONEYLINE",
                "MONEY_LINE"
            ],
            "selectBy":"FIRST_TO_START_AZ",
            "contentGroup":{
                "language":"en",
                "regionCode":"NZAUS"
            },
            "turnInPlayEnabled":True,
            "maxResults":0,
            "competitionIds":[ int(comp_id) for comp_id in comp_ids.split(',') ]
        },
        "facets":[
            {
                "type":"EVENT_TYPE",
                "skipValues":0,"maxValues":10,
                "next":{"type":"EVENT","skipValues":0,"maxValues":50,"next":{"type":"MARKET","maxValues":1}}
            }
        ],
        "currencyCode":"AUD",
        "locale":"en"
    }

    r = requests.post(url, headers=headers, json=payload)
    j = json.loads(r.text)
    return j['attachments']['markets'].keys()


def fetch(site_config, scrape_config):
    comp_ids = scrape_config.get('custom1', '')
    if not comp_ids:
        raise UserWarning('Missing competition id - specify this in the custom1 field')

    mkt_ids = _get_mkt_ids(site_config.get('headers', ''), comp_ids)
    if not mkt_ids:
        raise UserWarning('No mkt ids found')


    results = []

    # beyond this, betfair returns Too Much Data error, so chunk results
    mkt_limit = 25
    url = scrape_config['url']
    for i in xrange(0, len(mkt_ids), mkt_limit):
        scrape_config['url'] = url.format( mkt_ids=','.join(mkt_ids[i:i+mkt_limit]) )
        results.append( default.fetch(site_config, scrape_config) )

    return '[' + ','.join(results) + ']' # stringify the list
