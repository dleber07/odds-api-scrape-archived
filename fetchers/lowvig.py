import requests
import cfscrape

# This averts an out of date error. You really need to updgrade your env
cfscrape.DEFAULT_CIPHERS += ':!SHA'


def fetch(site_config, scrape_config):

    params = scrape_config.get('custom1')
    if not params or params.find('___') == -1:
        raise UserWarning('custom1 field not set or misconfiged')
    
    lvs = params.split('___')

    payload = {
        
        'param.SortOption': 'D',
        'param.PrdNo': '0',
        'param.Type': 'H2H',
        'param.RequestType': 'Normal',
        'param.H2HParam.Lv1': lvs[0], # e.g. Basketball
        'param.H2HParam.Lv2': lvs[1], # e.g. NCA
    }
    

    """ NOTE
    on local, creating a session, visiting the home page, then using the session to post works. this doesn't work on remote.
    on remote, cfscrape works with cfscrape.DEFAULT_CIPHERS += ':!SHA'
    """
    
    # s = requests.session()

    # Hit the homepage first. 
    # This sets a cloudflare cookie
    #requests.get('https://www.lowvig.ag/',
    #    headers=site_config.get('headers', ''),
    #    #cookies=site_config.get('cookies', ''),
    #    proxies=site_config.get('proxy', '')
    #)

    scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance

    r = scraper.post(
        scrape_config['url'],
        timeout=10,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies=site_config.get('proxy', ''),
        data=payload
    )

    return r.text
