from fetchers import default
import datetime

def fetch(site_config, scrape_config):
    
    # URL:
    # https://sportsbook-platform-api.nsoft.com/prematchOffer/getMatches?dataFormat=%7B%22default%22:%22object%22,%22sports%22:%22object%22,%22categories%22:%22object%22,%22tournaments%22:%22object%22,%22matches%22:%22array%22,%22betGroups%22:%22array%22%7D&dataShrink=true&language=%7B%22default%22:%22en%22%7D&params=%7B%22start_date%22:%22__START_DATE__%22,%22end_date%22:%22__END_DATE__%22,%22bet_count%22:3,%22include_meta%22:true,%22id_tournament%22:%5B%22917fdc33-d4ea-4007-a831-9577161aaae3%22%5D,%22delivery_platform%22:%22Web%22,%22company_uuid%22:%22c91074c3-29df-4eef-aea4-70ffb8ccdb8f%22%7D


    date_format = '%Y-%m-%d+%H:%M:%S'
    d_start = datetime.datetime.now() + datetime.timedelta(days=-1)
    d_end = d_start + datetime.timedelta(days=8)
    scrape_config['url'] = scrape_config['url'].replace('__START_DATE__', d_start.strftime(date_format)).replace('__END_DATE__', d_end.strftime(date_format))

    return default.fetch(site_config, scrape_config)
