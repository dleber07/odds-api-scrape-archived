import requests
from . import default
from settings import REDIS_CACHE_DB, CACHE_DEFAULT_EXPIRY_SECONDS
from connectors.redis_connect import get_redis_client

# Performs a standard get request
def fetch(site_config, scrape_config):

    """

    TODO create external cache class
    instantiate the cache class in main and pass it into fetch as param
    """

    cache_key = 'xhr_response:' + scrape_config['url']

    redisdb = get_redis_client(db=REDIS_CACHE_DB)
    response = redisdb.get(cache_key)

    if not response:
        # Cache is empty. Send a request
        response = default.fetch(site_config, scrape_config)
        redisdb.setex(cache_key, CACHE_DEFAULT_EXPIRY_SECONDS, response)

    return response
