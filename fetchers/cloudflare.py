import cfscrape

# NOTE cfscrape lib requires regular updates
# See https://github.com/Anorov/cloudflare-scrape for details

# Check latest version:
#   pip show cfscrape
# Update:
#   pip install cfscrape -U

def fetch(site_config, scrape_config):

    scraper = cfscrape.create_scraper()  # returns a CloudflareScraper instance

    r = scraper.get(
        scrape_config['url'],
        timeout=10,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies=site_config.get('proxy', '')
    )

    return r.text
