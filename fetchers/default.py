import requests

# Performs a standard get request
def fetch(site_config, scrape_config):

    r = requests.get(
        scrape_config['url'],
        timeout=10,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies=site_config.get('proxy', '')
    )

    return r.text
