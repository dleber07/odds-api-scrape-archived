import requests
from . import default
import json

# Performs a standard get request
def fetch(site_config, scrape_config):

    s = requests.session()

    _ = s.get(
        'TODO TRY HOME PAGEs
        timeout=5,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies=site_config.get('proxy', '')
    )

    result = s.get(
        scrape_config['url'],
        timeout=5,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies=site_config.get('proxy', '')
    )

    return result.text
