from fetchers import default_uk
from bs4 import BeautifulSoup
import lxml

def fetch(site_config, scrape_config):
    """
    Loops through WillHill competition tabs, sends requests for those that are relevant (as determined by scrape_config['custom1'])
    Appends response html to list (results)

    Args:
        site_config (dict): Site configs
        scrape_config (dict): Scrape configs

    Returns:
        list: List of html pages as strings
    """

    html = default_uk.fetch(site_config, scrape_config)
    soup = BeautifulSoup(html, 'lxml')
    accepted_tab_names_str = scrape_config['custom1']
    results = []

    # TODO willhill reader to handle list of html
    accepted_tab_names = accepted_tab_names_str.split('_') + ['In-Play']
    checkBoxes = soup.select('div.block-inner.block-selector')[0].find_all('li')
    first_tab_name = checkBoxes[0].find('b').string.lower()
    acceptFirstTick = [True for tab_name in accepted_tab_names if tab_name.lower() in first_tab_name]

    if len(acceptFirstTick) > 0 or not accepted_tab_names:
        # Collect the first page
        results.append(html)

    if not accepted_tab_names:
        return results

    urlList = _makeUrlList(checkBoxes, accepted_tab_names)

    for url in urlList:
        scrape_config['url'] = url
        try:
            html = default_uk.fetch(site_config, scrape_config)
            results.append(html) #, params, isTest, isLive=(thisLoop in liveIndex)))

        except:
            pass

    return results

def _makeUrlList(checkBoxes, accepted_tab_names):
    host_url = 'https://www.williamhill.com.au'
    urlList = []

    for li in checkBoxes:
        try:
            checkClass = li['class']
            compName = li.find('b').string.lower()
            validTick = [True for i in accepted_tab_names if i.lower() in compName]

            if 'checked' in checkClass:
                continue

            if not len(validTick):
                continue

            urlList.append(host_url + li.find('a')["href"])
        except:
            pass

    return urlList
