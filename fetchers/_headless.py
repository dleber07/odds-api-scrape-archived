from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def init_headless_driver(config, has_head=False):
    # Download driver: http://chromedriver.chromium.org/downloads
    # https://stackoverflow.com/questions/46920243/how-to-configure-chromedriver-to-initiate-chrome-browser-in-headless-mode-throug

    # Test xpath:
    #   http://videlibri.sourceforge.net/cgi-bin/xidelcgi

    headers = config.get('headers', {})

    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'

    CHROMEDRIVER_PATH = 'fetchers/chromedriver'
    options = Options()
    options.accept_untrusted_certs = True
    options.assume_untrusted_cert_issuer = True
    options.add_argument("--ignore-certificate-errors")

    if not has_head:
        options.add_argument('--headless')

    options.add_argument('--disable-gpu')    
    options.add_argument("user-agent=" + headers.get('User-Agent', user_agent))

    if config.get('proxy', False):
        pkey = config['proxy'].keys()[0]
        options.add_argument('--proxy-server=%s' % config['proxy'][pkey])

    return webdriver.Chrome(CHROMEDRIVER_PATH, chrome_options=options)
  