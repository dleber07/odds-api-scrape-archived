import requests

def fetch(site_config, scrape_config):
    # protocol = 'https' if scrape_config['url'][:5] == 'https' else 'http'
    r = requests.get(
        scrape_config['url'],
        timeout=10,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies={
            # NOTE buggy requests proxy. See https://stackoverflow.com/questions/24058127/https-proxies-not-working-with-pythons-requests-module
            # test ip with http://icanhazip.com
            'http': 'http://lum-customer-hl_c3cedc8d-zone-residential_uk:2vsqbzc1b3uj@zproxy.lum-superproxy.io:22225'
        }
    )

    return r.text

