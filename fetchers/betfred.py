import requests
from . import default
import json

# Performs a standard get request
def fetch(site_config, scrape_config):

    # Get market ids
    r = default.fetch(site_config, scrape_config)
    markets = json.loads(r)
    results = []

    for mkt in markets['Bonavigationnode'].get('marketgroups'):

        if mkt['idfwmarketgrouptype'] == 'MARKETTEMPLATE':
            # Assume outrights ???
            continue

        if scrape_config.get('custom1') and scrape_config.get('custom1') not in mkt['name']:
            continue

        mkt_id = mkt['idfwmarketgroup']
        scrape_config['url'] = 'https://betfred.mobi/services/SportsBook/marketgroup?language=uk&type=marketgroup&idfwmarketgroup={mkt_id}&dataflags=12&datasize=8'.format(mkt_id=mkt_id)
        results.append( default.fetch(site_config, scrape_config) )

    return json.dumps(results)
