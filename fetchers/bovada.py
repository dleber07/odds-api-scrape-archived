from . import default
import json

# Performs a standard get request
def fetch(site_config, scrape_config):

    # Get prematch events
    r_prematch = default.fetch(site_config, scrape_config)
    
    # Construct live events url
    live_scrape_config = dict(scrape_config)
    live_scrape_config['url'].replace('preMatchOnly=true', 'liveOnly=true')
    
    # Get live events
    r_live = default.fetch(site_config, live_scrape_config)


    # Merge results
    json_prematch = json.loads(r_prematch) or [{}] # ensure a 0th index
    json_live = json.loads(r_live) or [{}] # ensure a 0th index

    json_final = [
        {
            'events': json_prematch[0].get('events', []) + json_live[0].get('events', []),
            'path': json_prematch[0].get('path', []) + json_live[0].get('path', [])
        }
    ]

    return json.dumps(json_final)
