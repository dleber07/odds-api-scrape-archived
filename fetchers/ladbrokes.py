from . import default
from bs4 import BeautifulSoup
import lxml
import json

def fetch(site_config, scrape_config):

    header = scrape_config.get('custom1')
    if not header:
        raise UserWarning('Ladbrokes fetcher needs custom1 set to h3 tag')
    
    html = default.fetch(site_config, scrape_config)
    links = _get_links(html, header)

    if not links:
        raise UserWarning('No links found in fetcher')
    
    results = []
    for link in links:
        current_scrape_config = dict(scrape_config)
        current_scrape_config.update({'url': link})
        results.append(default.fetch(site_config, current_scrape_config))
        
        # Based on mma, it seems the same events appear in each link. Therefore just use the first
        break

    return json.dumps(results)

def _get_links(html, header):
    host = 'https://www.ladbrokes.com.au'
    soup = BeautifulSoup(html, 'lxml')
    links = []
    for fullbox in soup.find_all(class_='fullbox'):

        fullbox_header = fullbox.find('h3')
        if not fullbox_header:
            continue

        if header.lower() not in fullbox_header.text.lower():
            continue
        
        for td in fullbox.select('td.market-group'):
            if not td.find('a'):
                continue
            links.append(host + td.find('a')['href'])
    return links

        