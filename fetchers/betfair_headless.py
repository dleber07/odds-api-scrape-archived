#from selenium import webdriver
#from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from fetchers._headless import init_headless_driver
import time

# More info on selenium waits
# http://selenium-python.readthedocs.io/waits.html
# https://seleniumhq.github.io/selenium/docs/api/py/webdriver/selenium.webdriver.common.by.html


def fetch(site_config, scrape_config):
    result = ''

    try:
        driver = init_headless_driver(site_config)
        driver.set_window_size(200, 400) # vary depending on ua (is a ihpone or other)
        driver.get(scrape_config['url'])
        wait = WebDriverWait(driver, 5)

        nav_name = scrape_config['custom1']

        # sport_name = '2018 FIFA World Cup'
        xpath = "//a[contains(@class, 'navigation-link') and contains(text(), '" + nav_name + "')]"
        element = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
        element.click()

        # Wait for all markets
        # Don't know what to wait for? Events for different days load separately.
        time.sleep(4)

        result = driver.find_element_by_tag_name('body').get_attribute('innerHTML')

    except Exception as e:
        pass
        # driver.save_screenshot('%s.png' % (site_config['key']) )
        # print e

    finally:
        driver.quit()

    return result
