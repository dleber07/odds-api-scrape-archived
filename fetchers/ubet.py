import requests

"""

    This is a temp solution to avoid ssl cert verification (since scrape server ssl outdated)

"""


def fetch(site_config, scrape_config):

    r = requests.get(
        scrape_config['url'],
        timeout=10,
        headers=site_config.get('headers', ''),
        cookies=site_config.get('cookies', ''),
        proxies=site_config.get('proxy', ''),
        verify=False
    )

    return r.text
