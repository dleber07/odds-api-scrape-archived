from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from fetchers._headless import init_headless_driver
import json
import time
from env import ENV

def fetch(site_config, scrape_config):
    
    test= ENV == 'dev'


    if test:
        print '**** WARNING ****'
        print 'Browser has head in bet365 fetcher'

    results = {}

    """
    example:
        header_text == Full Time Result (green bg)
        subheader_text == United Kingdom
        link_text == England Premier League

        Note: you may need to click and wait on subheader_text before link_text appears
    """
    header_text, subheader_text, link_text = scrape_config['custom1'].split('_')

    try:

        driver = init_headless_driver(site_config, has_head=test)
        driver.set_window_size(750, 1334) # iphone6 dims
        driver.get(scrape_config['url'])

        # NOTE This doesnt apply when new page loads
        driver.execute_script('Object.defineProperty(navigator, "webdriver", { get: function() { return undefined; } });')
        wait = WebDriverWait(driver, 30)
        short_wait = WebDriverWait(driver, 4)
        header_text, subheader_text, link_text = scrape_config['custom1'].split('_')
        """
        example:
            header_text == Full Time Result (green bg)
            subheader_text == United Kingdom
            link_text == England Premier League

            Note: you may need to click and wait on subheader_text before link_text appears
        """

        # Test xpath:
        #   http://videlibri.sourceforge.net/cgi-bin/xidelcgi
        # NOTE: @class is sensitive to spaces! The above link might not pick this up
        # group_xpath = "//div[@class='sm-SplashContainer ' and //em[contains(@class, 'sm-MarketGroup_GroupName') and contains(text(), '" + header_text + "')]]"
        # sport_xpath = "//div[contains(@class,'sm-CouponLink_Label') and contains(text(), '" + link_text + "')]"
        group_xpath = "//div[contains(@class,'enhancedPod') and h1/em[contains(text(), '" + header_text + "')]]"

        subheader_xpath = group_xpath +  "//h3[contains(text(), '" + subheader_text + "')]"

        if test:
            print 'Subheader xpath'
            print subheader_xpath

        subheader_element = wait.until(EC.presence_of_element_located((By.XPATH, subheader_xpath)))

        try:        
            subheader_element.click()
        except:
            pass

        # TODO look for element 
        # You might get a mobile challenge here. 
        time.sleep(3)

        sport_xpath = "//div[contains(@class,'SplashRow') and span[contains(text(), '" + link_text + "')]]"
        xpath = group_xpath + sport_xpath

        if test:
            print 'Link xpath'
            print xpath

        element = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))

        try:
            element.click()
        except:
            # you probably hid it on the group click. Click the group again to render it
            subheader_element.click()
            element = wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
            element.click()

        if test:
            print 'Waiting for element id="main"'

        wait.until(lambda driver: driver.find_element_by_id('Main'))

        markets = {
            'h2h': 'Money Line',
            'spreads': 'Spread',
            'totals': 'Total',
            'h2h': 'Money Line/Spread',
            'spreads': 'Spread/Total',
        }

        # 2 outcome sports
        # one or more markets per page
        for market_key, market_name in markets.iteritems():
            market_xpath = "//div[contains(@class,'cm-MarketGroupWithButtonBarButton') and text() = '" + market_name + "']"
            if test:
                print 'Market path', market_xpath

            try:
                element = short_wait.until(EC.presence_of_element_located((By.XPATH, market_xpath)))
            except:
                continue

            element.click()
            time.sleep(0.4)
            results[market_key] = {
                'type': '2',
                'html': driver.find_element_by_tag_name('body').get_attribute('outerHTML')
            }
        
        if not results:
            # No markets found.
            # If this continues to be a problem, try looking for the puzzle and "touching"
            # If it needs a swipe, consider injecting javascript or look into http://appium.io/docs/en/writing-running-appium/touch-actions/
            results['h2h'] = {
                'type': '3',
                'html': driver.find_element_by_tag_name('body').get_attribute('outerHTML')
            }


    except Exception as e:
        driver.save_screenshot('fetchers/errorshots/%s.png' % (site_config['key']) )
        print e
        pass

    finally:
        driver.quit()
        #if not test:
        #    driver.quit()

    return json.dumps(results)
