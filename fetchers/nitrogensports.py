from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from fetchers._headless import init_headless_driver
import time
import json

def fetch(site_config, scrape_config):

    result = {}

    sport, league = scrape_config['custom1'].split('_')

    try:
        driver = init_headless_driver(site_config, True)
        driver.set_window_size(1024,768)
        wait = WebDriverWait(driver, 60)

        script = '\
        window.der = function(sport, league, period) {\
        $.ajax({\
            type: "POST", url: "/php/query/findgames.php", dataType: "json",\
            data: ({\
                sport: sport,\
                league: league,\
                period_description: period\
            }),\
            success: function (response) {\
                window.dldata = response;\
                document.body.innerHTML += \'<div id="ready-to-go"></div>\';\
            }\
        });}'

        driver.get(scrape_config['url'])
        driver.execute_script('Object.defineProperty(navigator, "webdriver", { get: function() { return undefined; } });')
        wait.until(EC.presence_of_element_located((By.XPATH, "//ul[contains(@class, 'page-sidebar-menu')]")))

        driver.execute_script(script)
        driver.execute_script('der("' + sport + '", "' + league + '");')
        wait.until(lambda driver: driver.find_element_by_id('ready-to-go'))
        result = driver.execute_script('return dldata;')

    except Exception as e:
        print e
        driver.save_screenshot('fetchers/errorshots/%s.png' % (site_config['key']) )
        pass

    finally:
        driver.quit()

    return json.dumps(result)
