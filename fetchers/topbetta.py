import requests
import json
from . import default
from components.scrape_tools import where_dict

def fetch(site_config, scrape_config):

    if scrape_config['custom1'].find('___') == -1:
        raise UserWarning('custom1 misconfigured or missing')

    sport_group, base_competition_name = scrape_config.get('custom1').split('___')
    response = json.loads(default.fetch(site_config, scrape_config))

    group_dict = where_dict(response['data'], 'name', sport_group)[0]

    if base_competition_name == 'ALL':
        # used for mma
        competition_ids = [ base_competition['competitions'][0]['id'] for base_competition in group_dict['base_competitions'] ]
    else:
        base_competition = where_dict(group_dict['base_competitions'], 'name', base_competition_name)[0]
        competition_ids = [ competition['id'] for competition in base_competition['competitions'] ]

    event_url = 'https://services.topbetta.com.au/api/6.5/combined/sports/competition/events?competition_id={}'
    
    responses = []
    for competition_id in competition_ids:
        current_scrape_config = dict(scrape_config)
        current_scrape_config['url'] = event_url.format(competition_id)
        responses.append(json.loads(default.fetch(site_config, current_scrape_config)))
    
    return json.dumps(responses)
