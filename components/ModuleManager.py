import importlib
import time

class ModuleManager(object):
    """
    @param int refresh_interval_mins set to 0 to disable refresh
    """
    def __init__(self, refresh_interval_mins=0):
        self.modules = None
        self.refresh_interval_mins = refresh_interval_mins
        self.last_refresh = 0

    def get_modules(self, fetcher_name, reader_name):

        if self.modules is None or self._update_due():
            self._reset()
            self.last_refresh = time.time()

        fetcher_found = self._check_modules('fetchers', fetcher_name)
        if not fetcher_found:
            return False

        reader_found = self._check_modules('readers', reader_name)
        if not reader_found:
            return False

        return {
            'fetcher': self.modules['fetchers'][fetcher_name],
            'reader': self.modules['readers'][reader_name],
        }

    def get_all_modules(self):
        return self.modules

    def _reset(self):
        self.modules = {
            'fetchers': {},
            'readers': {}
        }

    def _check_modules(self, module_folder, module_name):
        # Module found in cache. All is well.
        if module_name in self.modules[module_folder]:
            return True

        # Module not in cache. Try loading it.
        module = self._load_module(module_folder, module_name)
        if type(module) is str:
            return False

        # Module loaded successfully. Add to cache are relax.
        self.modules[module_folder][module_name] = module
        return True

    def _load_module(self, folder_name, module_name):
        try:
            return importlib.import_module(folder_name + '.' + module_name)

        except ImportError as e:
            return 'Module not found %s.%s - %s' % (folder_name, module_name, str(e))

    def _update_due(self):
        if self.refresh_interval_mins == 0:
            return False

        return time.time() > (self.last_refresh + self.refresh_interval_mins * 60)
