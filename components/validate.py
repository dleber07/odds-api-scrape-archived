from operator import mul
import unicodedata
import re
from connectors.redis_connect import get_redis_client
import settings
import datetime
import os
from fuzzywuzzy import process

os.environ['TZ'] = 'UTC'
redisdb = get_redis_client(db=settings.REDIS_VALIDATOR_DB)

def normalize_team(team):
    return team.replace('\n', '').replace('\t', '').strip().lower()

def validateEvents(params, events):
    # Validate
    valid_events = []
    event_list = {}
    for event in events:

        valid_event = validateEvent(params, event)

        if not valid_event or valid_event['name'] in event_list:
            continue

        event_list[valid_event['name']] = True
        valid_events.append(valid_event)

    return valid_events

def validateEvent(params, event):
    """
    Args:
        params (dict): Contains following keys
            - sport_config (dict)
            - scrape_config (dict)
            - site_config (dict)
        event (dict): The event record including the following keys
            - teams (list)
            - home_team (str)
            - name (str)
            - other (dict)
            - odds (dict)
    Returns:
        dict
    """
    sport_config = params['sport_config']
    scrape_config = params['scrape_config']
    particpant_map_name = scrape_config['participantmapname'] or scrape_config['key']

    teams = [resolveParticipant(redisdb, team, sport_config['participant_list'], sport_config['key'], particpant_map_name) for team in event['teams']]
    event['teams'] = teams

    if None in teams:
        return False

    if '_' in teams:
        # scrape has picked up a team that we've manually mapped to _ (invalid)
        return False
    
    if teams[0] == teams[1]:
        return False

    if 'home_team' in event:
        event['home_team'] = resolveParticipant(redisdb, event['home_team'], sport_config['participant_list'], sport_config['key'], particpant_map_name)

    event['name'], is_reversed = makeEventName(*teams)

    if is_reversed:
        event['teams'] = teams[::-1]
        event['odds'] = reverse_odds(event['odds'])
        
        if 'other' in event and 'score' in event['other']:
            scores = list(event['other']['score'])
            event['other']['score'] = [scores[1], scores[0]]


    if 'h2h' in event['odds']:
        odds_h2h = [cleanOdd(odd_h2h) for odd_h2h in event['odds']['h2h']]

        # Optional - only accept event if h2h valid
        # if False in odds_h2h:
        #    return False

        if False in odds_h2h:
            event['odds'].pop('h2h', None)
        else:
            event['odds']['h2h'] = odds_h2h
    
    if 'h2h_lay' in event['odds']:
        odds_h2h_lay = [cleanOdd(odd_h2h_lay) for odd_h2h_lay in event['odds']['h2h_lay']]
        if False in odds_h2h_lay:
            event['odds'].pop('h2h_lay', None)
        else:
            event['odds']['h2h_lay'] = odds_h2h_lay

    if event['odds'].get('totals') and event['odds']['totals']['position'][0].lower() == 'under':
        event['odds']['totals'] = reverse_totals(event['odds']['totals'])
    

    if not event['odds']:
        return False

    return event


def makeEventName(team1, team2):
    """
    Concatenates teams in alphabetical order

    Args:
        team1 (str)
        team2 (str)
    Returns:
        Tuple of concatenated teams and flag indicating if they were reversed

    """

    if team1.replace(' ', '').lower() < team2.replace(' ','').lower():
        return (team1 + '_' + team2, False)

    return (team2 + '_' + team1, True)

def reverse_odds(odds):
    """
    Note: [::-1] (the martian smiley) is an efficient way to reverse a list

    Args:
        odds (dict): Dictionary of markets (key) and odds (val)
    
    Returns:
        Dictionary of odds in reverse order for relevant markets
    """
    reversed_odds = dict(odds)
    if odds.get('h2h'):
        reversed_odds['h2h'] = list(odds['h2h']) # copy all, including draw
        reversed_odds['h2h'][0] = odds['h2h'][1]
        reversed_odds['h2h'][1] = odds['h2h'][0]

    if odds.get('h2h_lay'):
        reversed_odds['h2h_lay'] = list(odds['h2h_lay'])
        reversed_odds['h2h_lay'][0] = odds['h2h_lay'][1]
        reversed_odds['h2h_lay'][1] = odds['h2h_lay'][0]

    if odds.get('spreads'):
        reversed_odds['spreads'] = {
            'odds': list(odds['spreads']['odds']),
            'points': list(odds['spreads']['points']),
        }

        reversed_odds['spreads']['odds'][0] = odds['spreads']['odds'][1]
        reversed_odds['spreads']['odds'][1] = odds['spreads']['odds'][0]

        reversed_odds['spreads']['points'][0] = odds['spreads']['points'][1]
        reversed_odds['spreads']['points'][1] = odds['spreads']['points'][0]

    return reversed_odds

def reverse_totals(totals):
    # reverse totals for consistency
    reversed_totals = {
        'position': list(totals['position']),
        'odds': list(totals['odds']),
        'points': list(totals['points'])
    }

    reversed_totals['position'][0] = totals['position'][1]
    reversed_totals['position'][1] = totals['position'][0]

    reversed_totals['odds'][0] = totals['odds'][1]
    reversed_totals['odds'][1] = totals['odds'][0]

    reversed_totals['points'][0] = totals['points'][1]
    reversed_totals['points'][1] = totals['points'][0]

    return reversed_totals

def resolveParticipant(redisdb, team, participantList, sport, participantMapName):
    """
    Tries to identify a raw participant input in a list of known participants
    First checks redis cache at site level, then sport level
    If still not found, consults fuzzywuzzy

    Args:
        redisdb (redis): redis client
        team (str): the raw participant input, straight off the bookmaker site
        participantList (list): list of known participants for the sport
        sport (str): the sport of the event
        participantMapName (str): the redis hmap name in which to search for / store result

    Returns:
        str of participant from the list of known participants, or false

    """
    
    # TODO team should be normalized at this point
    # 

    if not team:
        return None

    team = _scrub_team(sport, team)


    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # Legacy - TODO remove in future
    # This performs a lookup on non-normalized teams
    # This non-normalized lookup remains because many pmaps contain useful mappings on non-normalized teams
    # At present, any new teams are normalized
    # Before removing this code, do the following:
    # For a given pmap, replace the pmap with the existing pmap after keys have been normalized
    # Do this for all pmap:sport and pmap:sport:site. It will then safe to remove this
    # Perform an initial test on an unpopular sport
    try:
        # Try and get team from cache
        teamMatched = redisdb.hget('participantMap:' + participantMapName, team)
        if not teamMatched:
            # Nothing found in sport:site map. Check the global sport map - maybe another site resolved it
            teamMatched = redisdb.hget('participantMap:' + sport, team)
    except:
        # redis unavailable
        teamMatched = None

    if teamMatched:
        return teamMatched
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


    # Look for the normalized team in pmap
    team = normalize_team(team)
    try:
        # Try and get team from cache
        teamMatched = redisdb.hget('participantMap:' + participantMapName, team)
        if not teamMatched:
            # Nothing found in sport:site map. Check the global sport map - maybe another site resolved it
            teamMatched = redisdb.hget('participantMap:' + sport, team)
    except:
        # redis unavailable
        teamMatched = None


    if teamMatched:
        return teamMatched





    if not participantList:
        return None

    # Use fuzzywuzzy to identify the participant in the participantList
    teamMatched = process.extractOne(team, participantList, score_cutoff=settings.VALIDATION_FUZZY_PMAP_CUTOFF)
    if teamMatched:
        teamMatched = teamMatched[0]

    try:
        if teamMatched:
            redisdb.hmset('participantMap:' + sport, {team: teamMatched}) # add to sport map
        else:
            redisdb.sadd('nameMismatch:' + participantMapName, team)
    except:
        # print 'Redis unavailable'
        # NOTE unavailable redis handler is most useful in testing. A better apporach would be to pass is_test param
        pass

    return teamMatched

def cleanOdd(odd):

    if odd is str:
        odd = odd.replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')

    if not odd:
        return 0
    
    try:
        odd_float = float(odd)
    except:
        return 0

    if odd_float < 1: 
        odd = 0
 
    return round(odd_float, 2)


def formatOdd(odd):
    if not odd:
        return False

    return round( float( odd ), 3 )


def isValidOutrightOdd(odd, oddThreshold):
    if not oddThreshold: return True # (type(oddThreshold) is float)
    return float(odd) < float(oddThreshold)


def UTC_to_STR(DT, inpFormat):
    """
    @deprecated see scrape_tools.datetime_to_unix
        args:
            DT - datetime string
            inpFormat - format of DT, like "%Y-%m-%d %H:%M:%S.%f"

    """
    DT = DT.replace("T", " ").replace("Z", "")
    return datetime.datetime.strptime(DT, inpFormat).strftime("%s")


def acceptComp(thisComp, outrightEventName, sport, site):

	#	Provides an additional step of data validation for a specific site and sport, based on uniqueIdentifier
	#	Optional - not always necessary

	if not outrightEventName:
		return True # if sport not specified in uniqueIndentifier, push on

	#thisComp  = ''.join([i for i in thisComp if not i.isdigit()])
	thisCompModified= re.sub('[(){}<>/]', '', thisComp).strip()

	if outrightEventName == thisComp:
		return True
	elif outrightEventName.lower().replace(" ","") in thisCompModified.lower().replace(" ",""):
		return True

	return False

def frac_to_decimal(numerator, denominator):
    """
        Converts fractional odds to decimal odds

        Args:
            numerator (float)
            denominator (float)
        Retruns float

    """
    return round(numerator * 1.0 / denominator + 1, 3)

def american_to_decimal(am_odd):
    """
        Converts American odd to decimal odd

        Args:
            am_odd (float): american odd
        Returns float
    """
    if am_odd < 0:
        odd = 1 - 100 * 1.0 / am_odd
    else:
        odd = 1 + am_odd * 1.0 / 100
    
    return round(odd, 3)


"""
    Apply sport-specific logic to prepare team names for matching pmap or other
    Args:
        sport_key (str)
        team (str)
    Returns: str
"""
def _scrub_team(sport_key, team):

    if sport_key == 'baseball_mlb':
        # remove brackets and content within
        return re.sub(r" ?\([^)]+\)", "", team)
        
    return team
