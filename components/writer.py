from connectors.mongo_connect import Mongo
import pymongo
import time
from settings import MONGO_EVENTS_DB, MAX_NO_UPDATE_WINDOW_MINS, COMMENCE_TIME_TOLERANCE_MINS, MONGO_CONFIG_DB
from collections import OrderedDict


# Important:
#   Pymongo and ordered dicts
#   https://stackoverflow.com/questions/23001156/how-to-get-ordered-dictionaries-in-pymongo

# Find event in mongo
#   if not found, insert and return
#   if site == commence site OR commence_last_update old, update
#   Otherwise bail

# What if 2 sites with different commence, 1 due to error?
#   Both will be inserted. One will get poulated with sites, the other with 1 site.

# What if commence changes?
#   A new event will appear next to the old. The old will drop away from output when it gets too old.

# How many sites should have event_creator == true?
#   No more than 2, otherwise creates confusion and possible conflict.
#   Let event_creator be easily configurable

def _create_new_event(params, event):
    sport_config = params['sport_config']
    site_config = params['site_config']

    doc = OrderedDict([
        ('sport_key', sport_config['key']),
        ('sport_nice', sport_config['title']),
        ('sport_group_key', sport_config.get('group_key')),
        ('sport_group', sport_config['group']),
        ('name', event['name']),
        ('teams', event['teams']),
        ('commence_time', event['commence_time']),
        ('commence_site', site_config['key']),
        ('commence_last_update', int(time.time())),
        ('home_team', event.get('home_team', '')),
        ('last_update', int(time.time()))
    ])

    return pymongo.InsertOne(doc)

def _write_event_fields(queries, collection, params, event):

    sport_config = params['sport_config']
    site_config = params['site_config']

    match = OrderedDict([
        ('commence_time', {
            '$gte': (event['commence_time'] - COMMENCE_TIME_TOLERANCE_MINS * 60),
            '$lt': (event['commence_time'] + COMMENCE_TIME_TOLERANCE_MINS * 60)
        }),
        ('sport_key', sport_config['key']),
        ('name', event['name']),
    ])

    db_event = collection.find_one(
        match,
        projection=['commence_site', 'commence_last_update']
    )

    if not db_event:
        # Create a new event
        queries.append(
            _create_new_event(params, event)
        )

        return queries

    if db_event['commence_site'] == site_config['key'] or time.time() > (db_event['commence_last_update'] + MAX_NO_UPDATE_WINDOW_MINS * 60):
        # Refresh commence time
        queries.append(pymongo.UpdateOne(
            {'_id': db_event['_id']},
            {'$set': {
                'commence_time': event['commence_time'],
                'commence_site': site_config['key'],
                'commence_last_update': int(time.time()),
                'teams': event['teams'],
                'home_team': event.get('home_team', ''),
                # 'status': live, pending, etc..
            }},
            upsert=False
        ))
        return queries

    return queries

def mongo_upsert_subdoc():
    # first push where EVENT AND sites.site_key $ne {SITEKEY}
    # then update WHERE EVENT AND sites.site_key == {SITEKEY}
    #{ $set: { 'translations.$': your doc } }
    pass


def write_db(params, events):

    collection = Mongo[MONGO_EVENTS_DB].events
    collection_config_sports = Mongo[MONGO_CONFIG_DB].config_sports

    # TODO consider if each event should have a "preceding_event" key

    sport_config = params['sport_config']
    site_config = params['site_config']
    scrape_config = params['scrape_config']

    queries = []

    for event in events:

        if site_config['event_creator'] and 'commence_time' in event:
            queries = _write_event_fields(queries, collection, params, event)

        match = OrderedDict()

        if 'commence_time' in event:
            match['commence_time'] = {
                '$gte': (event['commence_time'] - COMMENCE_TIME_TOLERANCE_MINS * 60),
                '$lt': (event['commence_time'] + COMMENCE_TIME_TOLERANCE_MINS * 60)
            }

        match['sport_key'] = sport_config['key']
        match['name'] = event['name']
        
        site_fields = OrderedDict([
            ('site_key', site_config['key']),
            ('site_nice', site_config['display']),
            ('regions', site_config['regions'].split(',')),
            ('last_update', int(time.time())),
            ('odds', event['odds']),
        ])

        if 'other' in event and event['other'].get('matched'):
            site_fields['matched'] = event['other'].get('matched')


        """
        # OPTIONAL - add other fields collected at the site level. Note we add scores later (below) since we want it at the event level
        if 'other' in event:
            site_fields['other'] = event['other']
        """

        # Now we need to upsert to the site sub-document in the event document
        # As at mongo 3.6, no such query exists out of the box. We can pull off an atomic work around.
        # REF http://jthomerson.github.io/mongodb-safe-upserts-array-subdocuments/#/

        # First push the site subdoc to the event doc, only if the subdoc doesn't exist.
        push_match = match.copy()
        push_match['sites.site_key'] = {'$ne': site_config['key']}
        queries.append(pymongo.UpdateOne(
            push_match,
            {'$push': {
                'sites': site_fields
            }},
            upsert = False
        ))

        update_fields = {
            'last_update': int(time.time()),
            'sites.$': site_fields
        }

        # Add event-level fields obtained from individual sites (e.g. scores, game clock)
        if 'other' in event and event['other'].get('score'):
            update_fields['score'] = event['other']['score']

        # Now we can be sure the site subdocument exists, so go ahead and update it.
        set_match = match.copy() # has nothing todo with tennis
        set_match['sites.site_key'] = site_config['key']
        queries.append(pymongo.UpdateOne(
            set_match,
            {'$set': update_fields},
            upsert = False
        ))

    result = collection.bulk_write(queries)

    # Update sport config - this is useful for determining in-season sports
    sport_config_update_fields = {
        'last_update': int(time.time())
    }

    if site_config['event_creator']:
        sport_config_update_fields['commence_last_update'] = int(time.time())

    result_sport_config = collection_config_sports.update_one(
        {'key': sport_config['key']},
        {'$set': sport_config_update_fields},
        upsert=False
    )


    return {
        'inserted': result.inserted_count,
        'modified': result.modified_count,
        'upserted': result.upserted_count,
        # 'updated_sport_config': result_sport_config.inserted_count
    }
