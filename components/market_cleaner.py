import re

def line_clean(lineAr):
    
    lineOdd1 = str(lineAr[0]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not lineOdd1.replace('.','',1).isdigit():
        lineOdd1 = False
    
    linePoints1 = lineAr[1]
    linePoints1 = str(lineAr[1]).replace('+', '')
        
    lineOdd2 = str(lineAr[2]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not lineOdd2.replace('.','',1).isdigit():
        lineOdd2 = False
    
    linePoints2 = str(lineAr[3]).replace('+', '')

    if float(linePoints1) == -1*float(linePoints2) and lineOdd1 != False and lineOdd2 != False:
        return {
            'odds': [float(lineOdd1), float(lineOdd2)],
            'points': [linePoints1, linePoints2]
        }
    else:
        return False

def line_clean3(lineAr):
    """
        Use if 3 outcomes in lines mkt
    """
    lineOdd1 = str(lineAr[0]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not lineOdd1.replace('.','',1).isdigit():
        lineOdd1 = False
    
    linePoints1 = str(lineAr[1]).replace('+', '')
    
    lineOdd2 = str(lineAr[2]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not lineOdd2.replace('.','',1).isdigit(): 
        lineOdd2 = False
    
    linePoints2 = str(lineAr[3]).replace('+', '')

    lineOdd3 = str(lineAr[4]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not lineOdd3.replace('.','',1).isdigit(): 
        lineOdd3 = False
    
    linePoints3 = str(lineAr[5]).replace('+', '')

    if lineOdd1 != False and lineOdd2 != False and lineOdd3 != False:
        return {
            'odds': [
                round(float(lineOdd1), 2),
                round(float(lineOdd2), 2), 
                round(float(lineOdd3), 2)
            ],
            'points': [
                linePoints1,
                linePoints2,
                linePoints3
            ]
        }
    else:
        return False

def totals_clean(ouAr):
    # ouAr input = [ odd1, point1, odd2, point2] ; not ordered  
    odd1, point1, odd2, point2 = ouAr
    outcome1 = _totals_clean_one(odd1, point1)
    if not outcome1:
        return False
    
    outcome2 = _totals_clean_one(odd2, point2)
    if not outcome2:
        return False

    return {
        'points': [outcome1['point'], outcome2['point']],
        'odds': [outcome1['odd'], outcome2['odd']],
        'position': [outcome1['pos'], outcome2['pos']]
    }

    
def _totals_clean_one(odd, point):
    # Get Points
    try:
        ouPoints = float(re.findall("[-+]?\d+[\.]?\d*", str(point))[0])
    except:
        return False

    if not ouPoints:
        return False

    # Get Odds
    ouOdd = str(odd).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not ouOdd.replace('.','',1).isdigit():
        return False

    if 'under' in point.lower():
        key = 'under' 
    elif 'over' in point.lower():
        key = 'over'
    else:
        return False

    return {
        'pos': key,
        'odd': float(ouOdd),
        'point': ouPoints
    }



def margin_clean(m0, m1, d ):
    
    marginPoint1_0 = m0[0]
    marginOdd1_0 = str(m0[1]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not marginOdd1_0.replace('.','',1).isdigit() :  marginOdd1_0 = False    

    marginPoint2_0 = m0[2]
    marginOdd2_0 = str(m0[3]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not marginOdd2_0.replace('.','',1).isdigit() :  marginOdd2_0 = False    
    
    marginPoint1_1 = m1[0]
    marginOdd1_1 = str(m1[1]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not marginOdd1_1.replace('.','',1).isdigit() :  marginOdd1_1 = False    

    marginPoint2_1 = m1[2]
    marginOdd2_1 = str(m1[3]).replace('\n', '').replace('\t', '').replace('\r', '').replace(' ', '')
    if not marginOdd2_1.replace('.','',1).isdigit() :  marginOdd2_1 = False    
    
    if (marginPoint1_0 != False and marginPoint2_0 != False and
        marginOdd1_0 != False and marginOdd2_0 != False and
        marginPoint1_1 != False and marginPoint2_1 != False and
        marginOdd1_1 != False and marginOdd2_1 != False):
        
        return {'mOdds0': [marginOdd1_0, marginOdd2_0], 'mPoints0': [marginPoint1_0, marginPoint2_0],
                'mOdds1': [marginOdd1_1, marginOdd2_1], 'mPoints1': [marginPoint1_1, marginPoint2_1],
                }
    else:
        return {'mOdds0': [False, False], 'mPoints0': [False, False],
                'mOdds1': [False, False], 'mPoints1': [False, False],
                }

