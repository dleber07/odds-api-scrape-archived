import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
args = parser.parse_args()

if args.verbose:

    def verboseprint(*args):
        for arg in args:
           print arg,
        print
else:   
    verboseprint = lambda *a: None

