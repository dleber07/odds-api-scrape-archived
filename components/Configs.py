import time
import json

class Configs(object):

    """
    @param int refresh_interval_mins Refresh configs every this many minutes. 
        Note - when configs written to mongo directly, reduce this
        Set to 0 to disable
    """
    def __init__(self, refresh_interval_mins=5):

        self.refresh_interval_mins = refresh_interval_mins

        self.params = {
            'sites': {},
            'sports': {},
            'scrapes': {},
            'result_scrapes': {},
        }

        self.last_load = {
            'sites': 0,
            'sports': 0,
            'scrapes': 0,
            'result_scrapes': 0
        }

        self.db = None

    def set_db(self, db):
        self.db = db

    def get_sites(self):
        return self._get_configs('sites', self._get_sites)
 
    def get_sports(self):
        return self._get_configs('sports', self._get_sports)
 
    def get_scrape(self, scrape_key):
        return self._get_configs('scrapes', self._get_scrapes).get(scrape_key, None)

    def get_result_scrape(self, scrape_key):
        return self._get_configs('result_scrapes', self._get_result_scrapes).get(scrape_key, None)

    def _get_configs(self, key, getter):
        if not self.params[key] or self._refersh_due(key):
            self.params[key] = getter()
            self.last_load[key] = time.time()

        return self.params[key]

    def _get_sites(self):

        site_configs = self._load_config('sites')
        result = {}

        for site in site_configs:
            if not site_configs[site]['active']:
                continue

            result[site] = site_configs[site].copy()
            result[site]['headers'] = self._format_headers(result[site])
            result[site]['cookies'] = self._format_cookies(result[site])
            result[site]['proxy'] = self._format_proxy(result[site])

        return result

    def _get_sports(self):

        # NOTE: participant_type can be team or name
        sport_configs = self._load_config('sports')

        result = {}

        for sport in sport_configs:
            if not sport_configs[sport]['active']:
                continue

            result[sport] = sport_configs[sport].copy()
            result[sport]['participant_list'] = result[sport]['participant_list'].split(',')
        return result

    def _get_scrapes(self):
        # NOTE fetcher: this is a module in fetchers
        # NOTE reader_method: this is a function in module readers.{site}
        # TODO participantmapname: allow a default, something like participantlist + ':' + site

        return self._load_config('scrapes')


    def _get_result_scrapes(self):
        return self._load_config('result_scrapes')


    def _format_proxy(self, site_config):
        # Format proxy
        # proxy input should be like https_123.1.1.0:8888, but if you forget protocol, could be like 123.1.1.0:8888
        proxy = site_config.get('proxy', False)

        if not proxy:
            return {}


        if '___' in proxy:
            protocol, ip = proxy.split('___')

        elif '_' in proxy:
            # TODO remove when all scrape configs have been updated with ___
            protocol, ip = proxy.split('_')

        else:
            protocol, ip = 'https', proxy

        return {protocol: ip}


    def _format_headers(self, site_config):

        USER_AGENTS = {
            'iphone5': 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3',
            'iphone7': 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E302 [FBAN/FBIOS;FBAV/174.0.0.48.98;FBBV/110921384;FBDV/iPhone9,3;FBMD/iPhone;FBSN/iOS;FBSV/11.3.1;FBSS/2;FBCR/TelenorDK;FBID/phone;FBLC/da_DK;FBOP/5;FBRV/112241032]',
            'ipad3' : 'Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53',
            'chrome' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
            'firefox' : "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
            'galaxyS4': 'Mozilla/5.0 (Linux; Android 4.2.2; GT-I9505 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.59 Mobile Safari/537.36',
            'none'	: ''
        }


        ua_key = site_config['ua'] if site_config.get('ua') and site_config.get('ua') in USER_AGENTS else 'chrome'

        headers = {
            'User-Agent': USER_AGENTS[ua_key]
        }

        headerstr = site_config.get('headers', '')

        if not headerstr:
            return headers

        for h in headerstr.split('___'):
            kv = h.split(':', 1)
            headers[kv[0]] = kv[1]

        return headers
    
    def _format_cookies(self, site_config):

        cookiestr = site_config.get('cookies', '')
        cookies = {}

        if not cookiestr:
            return cookies

        for h in cookiestr.split('___'):
            kv = h.split(':', 1)
            cookies[kv[0]] = kv[1]

        return cookies

    def _refersh_due(self, key):
        if self.refresh_interval_mins == 0:
            return False

        return time.time() > (self.last_load[key] + self.refresh_interval_mins * 60)

    def _load_config(self, config_name):

        if self.db:
            return self._load_from_db(config_name)
        else:
            return self._load_from_json(config_name)

    def _load_from_json(self, file_name):
        with open('configs/' +  file_name+ '.json', 'r') as config_file:
            result = json.loads(config_file.read().replace('\n', ''))
    
        return result

    def _load_from_db(self, config_name):
        return {item['key']: item for item in getattr(self.db, 'config_' + config_name).find()}
