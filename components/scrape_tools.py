import pytz
import datetime

def json_loads(s):
    """
        Json parse a javascript-style dict
        Args:
            s (str) stringified javascript dict
        Returns:
            dict
    """
    import json
    return json.loads(s.replace("'", '"'))


def where_dict(list_dicts, key, val):
    """ Filter a list of dictionaries based on a key's value 

    Args:
        list_dicts (list): A list of dictionaries
            Example: [ {'name': 'fred', 'age': 124}, {'name': 'bob', 'age': 53} ]

        key (str): the key of the dictionaries in group_dicts to be matched
            Example: 'name'

        val (str): the dict value to match on
            Example: 'bob'
    Return:
        list of matching dictionaries
            Example: [ {'name': 'bob', age: 53} ]
    """
    return filter(lambda list_dicts: list_dicts.get(key) == val, list_dicts)

def datetime_to_unix(dt, dt_format, timezone=None):
    """ Convert a datetime string to a unix timestamp

    Args:
        dt (str): '2019-09-09 03:00:00'
        dt_format (str): format of dt, example '%Y-%m-%d %H:%M:%S'
        timezone (str): timezone, example "Australia/Sydney"
    
    Return:
        str: unix timestamp
    """
    d = datetime.datetime.strptime(dt, dt_format)

    if timezone:
        tz_local = pytz.timezone(timezone)
        d_local = tz_local.localize(d)
        d = d_local.astimezone(pytz.utc)

    return int(d.strftime('%s'))
