# 
SCHEDULER_SLEEP_INTERVAL = 1

#
NUM_PROCESSES = 10

MAX_QUEUE_SIZE = 10


# If no updates have occurred in this many minutes, the event is considered expired
# If commence hasn't updated in this many minutes, it will get refreshed, possible by another source
MAX_NO_UPDATE_WINDOW_MINS = 15

# Commence times from 2 different sources will match if they fall within this many minutes of each other
COMMENCE_TIME_TOLERANCE_MINS = 360

#
REDIS_STATUS_DB = 2

# Redis DB containing participant maps and mismatches
REDIS_VALIDATOR_DB = 3

# Redis DB containing short lived structures (e.g. can be used to cache http responses for several seconds)
REDIS_CACHE_DB = 2

# This many seconds before commence time, scrape faster (see interval_min, interval_max in scrape_config)
FAST_SCRAPE_PREMATCH_SECONDS = 3600


# If 0 events are found in this many consecutive scrapes, set the scrape interval to SLOW_SCRAPE_SECONDS
SLOW_SCRAPE_ZERO_EVENT_THRESHOLD = 5

VERY_SLOW_SCRAPE_ZERO_EVENT_THRESHOLD = 30

# If 0 events found SLOW_SCRAPE_ZERO_EVENT_THRESHOLD times, scrape interval will be set to this
SLOW_SCRAPE_SECONDS = 3600

VERY_SLOW_SCRAPE_SECONDS = 43200 # == 12 hrs, 86400 == 24 hrs


# The next_to_go:sport redis key (commence time) expires after this many seconds.
NEXT_TO_GO_EXPIRY = 1800 # 30 mins

#
MONGO_CONFIG_DB = 'scrape'

#
MONGO_EVENTS_DB = 'scrape'

# Frequency that sleep delegator checks to put sports to sleep / wake
SLEEP_DELEGATORS_SLEEP_INTERVAL = 3600

#
SCRAPE_INTERVAL_FOR_SLEEPY_SPORTS = 86400 # 24hrs

# Fuzzywuzzy cutoff score when matching raw participant to known list.
# (100 is perfect match)
# Below is a miss, above is added to pmap
VALIDATION_FUZZY_PMAP_CUTOFF = 75

# 
CACHE_DEFAULT_EXPIRY_SECONDS = 60
