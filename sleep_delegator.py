import time
from components.Configs import Configs
from env import ENV
import settings
from connectors.redis_connect import get_redis_client

"""
    Determines sport's sleeping flag
    Does NOT determine a change in behaviour for sleeping / awake sports
    This is handled in scrape.py
"""


def notify(msg):
    # TODO
    print 'Notify: ', msg
    pass

def post_twitter():
    # TODO do not enable this until confident in the script
    pass

def get_status(redisdb, sport, site):
    return redisdb.hgetall('status:' + '%s:%s' % (sport, site))

def ready_to_sleep(sport):
    statuses = [get_status(sport, site) for site in get_trusted_sites()]

    # Only consider a status if it has no errors
    zero_event_cnts = [status['zero_event_cnt'] for status in statuses if 'zero_event_cnt' in status and status.get('error', '') == '']

    if len(zero_event_cnts) == 0:
        # NOTE there may be NO statuses since you havent specified scrape funcs, in which case, do not sleep
        return False

    avg_zero_event_cnts = sum(zero_event_cnts) / len(zero_event_cnts)
    if avg_zero_event_cnts >= 20:
        return True

    return False


def ready_to_wake(sport):
    # NOTE - some sites can still pick up irrelevant events when sleeping
    # perhaps criteria of event_created is that it ONLY returns the competition you specify in url (unlike skybet or willhill for example)

    statuses = [get_status(sport, site) for site in get_trusted_sites()]
    total_raw_events_cnt = sum([status.get('raw_events_cnt', 0) for status in statuses])
    if total_raw_events_cnt > 0:
        return True

    return False

def set_scrape_intervals(redisdb, interval):
    # NOTE this is optional.
    # Main benefit of sleeping is simply the flag, which helps avoid returning empty event sets to users
    #   and to detect changes (to update participant maps etc)

    scrape_keys = redisdb.keys('status:' + sport + ':*')
    pipe = redisdb.pipe()
    for scrape_key in scrape_keys:
        pipe.hmset(scrape_key, 'interval', interval)

    pipe.execute()

def update_sleep_db(sport, sleeping):
    Mongo[settings.MONGO_CONFIG_DB].config_sports.update_one(
        {'key': sport},
        {'$set': {'sleeping': sleeping}},
        upsert=False
    )

def get_trusted_sites():
    return list(Mongo[settings.MONGO_CONFIG_DB].config_sites.find(
        {
            'active': True,
            'event_creator': True
        },
        projection=['key']
    ))

def wake_up(sport):
    update_sleep_db(sport, False)
    
    # Interval low enough so that it will probably be due for scrape immediately
    # high enough incase something failing & interval not being overwritten with normal values
    interval = 600
    set_scrape_intervals(redisdb, interval)

    notify(sport + ' has woken. Revise participant list & urls')

    # TODO
    post_twitter() # make sure other sites have events too?


def go_to_sleep(sport):
    update_sleep_db(sport, True)
    set_scrape_intervals(redisdb, settings.SCRAPE_INTERVAL_FOR_SLEEPY_SPORTS)

    # TODO
    # - Remove all events from mongo ? Will this happen automatically with the event removal script?
    notify(sport + ' going to sleep. Good night')


if __name__ == '__main__':

    configs = Configs()
    redisdb = get_redis_client(db=settings.REDIS_STATUS_DB)

    if ENV == 'prod':
        from connectors.mongo_connect import Mongo
        configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

    while True:

        # TODO update redis with sleep delegate timestamp (to inform still running)

        for sport in configs.get_sports():
            sport_config = configs.get_sports()[sport]
            
            if sport_config.get('sleeping', False) and ready_to_wake(sport):
                wake_up(sport)
            
            elif sport_config.get('sleeping', False) == False and ready_to_sleep(sport):
                go_to_sleep(sport)

        # Now it's the script's turn to take a nap
        time.sleep(settings.SLEEP_DELEGATORS_SLEEP_INTERVAL)


    QUESTIONS / considerations
    # TODO need a way to prevent everything running at once (in main) when starting a scrape
