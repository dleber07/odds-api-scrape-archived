
## Setup
- Create and populate env.py
- Download fetchers/chromedriver version depending on OS


**** Install Chrome headless. See
 https://stackoverflow.com/questions/46920243/how-to-configure-chromedriver-to-initiate-chrome-browser-in-headless-mode-throug


***To install gdrive
Download relevant from https://github.com/prasmussen/gdrive
Rename as gdrive
Move to /usr/bin
Call: `gdrive about` to setup


***Note on Google Sheets API:
discovery.build() shows a build up in memory usage over time. Status_to_sheets starts at 5% memory on t2.small, and grows slowly, until it drains all memory
I know it's google's discovery apiclient coz when disabled, the server runs at a STEADY 0.7% memory.
SEE https://github.com/burnash/gspread for a promising alternative!
Also https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html
REF http://gspread.readthedocs.io/en/latest/index.html
