from connectors.mongo_connect import Mongo
from collections import OrderedDict
from settings import MAX_NO_UPDATE_WINDOW_MINS, MONGO_EVENTS_DB
import time

events_collection = Mongo[MONGO_EVENTS_DB].events

match = OrderedDict([
    ('commence_last_update', {
        '$lt': time.time() - MAX_NO_UPDATE_WINDOW_MINS * 2 * 60
    })
])

result = events_collection.delete_many(match)

print time.time(), result.deleted_count
