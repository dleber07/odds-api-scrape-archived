#!/usr/bin/python
import settings
from connectors.redis_connect import get_redis_client
import datetime
import pytz
import time
from env import SCRAPE_STATUS_SPREADSHEET_ID as spreadhsheet_id

import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Update sheets every this many seconds
SLEEP_INTERVAL = 60

 
fields = [
    'queue_added',
    'fetcher_start',
    'reader_start',
    'writer_start',
    'validation_start',
    'scrape_end',
    'interval',
    'raw_events_cnt',
    'valid_events_cnt',
    'error',
    'zero_event_cnt',
]

def get_wb(spreadhsheet_id):
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('extras/credentials.json', scope)
    gc = gspread.authorize(credentials)
    return gc.open_by_key(spreadhsheet_id)

def get_statuses(redisdb):
    status_keys = redisdb.keys('status:*')
    pipe = redisdb.pipeline()
    for key in status_keys:
        pipe.hgetall(key)
    return (status_keys, pipe.execute())


def get_status_rows(redisdb, fields):
    cellvals = ['key'] + fields
    status_keys, statuses = get_statuses(redisdb)
    
    if not statuses:
        return False

    for i, status in enumerate(statuses):
        cellvals += [status_keys[i]] + [status.get(field, '') for field in fields]

    return cellvals

def write_sheets(ws, cellvals):

    first_row = 1
    first_col = 1
    last_col = len(fields) + 1
    last_row = int(len(cellvals) / last_col)

    cells = wb.worksheet('status').range(first_row, first_col, last_row, last_col)

    for i, cellval in enumerate(cellvals):
        cells[i].value = cellval

    # ws_status.resize(rows=last_row)
    wb.worksheet('status').update_cells(cells)
    wb.worksheet('sys').update_acell('B2', datetime.datetime.now(pytz.timezone('Australia/Sydney')).strftime("%Y-%m-%d %H:%M:%S"))


if __name__ == '__main__':

    redisdb = get_redis_client(db=settings.REDIS_STATUS_DB)
    
    wb = get_wb(spreadhsheet_id)
    last_sheet_write = 0

    while True:
        if float(redisdb.get('last_scrape_write') or '0') <= last_sheet_write:
            time.sleep(SLEEP_INTERVAL)
            continue

        cellvals = get_status_rows(redisdb, fields)

        if not cellvals:
            # Waiting for first time
            time.sleep(600)
            continue

        try:
            write_sheets(wb, cellvals)
            last_sheet_write = time.time()
        except:
            wb = get_wb(spreadhsheet_id)

        time.sleep(SLEEP_INTERVAL)
