"""
    Run in CRON every minute:
    * * * * * cd /home/scrape && python -m extras.stats > logs/stats.log

"""

from connectors.mongo_connect import Mongo
import time
import statistics
import pymongo

site_valid_mins = 10
batch_size = 1000

collection = Mongo['scrape'].events
ok_cnt = 0
queries = []

def get_stats(l):
    # http://cpython-test-docs.readthedocs.io/en/latest/library/statistics.html
    count = len(l)
    return {
        'mean': round(statistics.mean(l), 2),
        'median': round(statistics.median_grouped(l), 2),
        'stdev': round(statistics.stdev(l), 2) if count > 1 else 0,
        'count': len(l)
    }

def make_odds_lists_h2h(sites, site_valid_mins):
    oddslist1, oddslist2, oddslist3 = [], [], []

    for site in sites:

        if not site['odds'].get('h2h', False):
            continue

        if site['last_update'] < time.time() - site_valid_mins * 60:
            continue

        oddslist1.append(site['odds']['h2h'][0])
        oddslist2.append(site['odds']['h2h'][1])
        if len(site['odds']['h2h']) == 3:
            oddslist3.append(site['odds']['h2h'][2])
        
    return oddslist1, oddslist2, oddslist3


for event in collection.find(projection=['_id','sites']):

    if 'sites' not in event:
        continue

    oddslist1, oddslist2, oddslist3 = make_odds_lists_h2h(event['sites'], site_valid_mins)

    if not oddslist1 or not oddslist2:
        update_fields = None

    else:
        stats = [
            get_stats(oddslist1),
            get_stats(oddslist2)
        ]

        if oddslist3:
            stats.append(get_stats(oddslist3))

        update_fields = {
            'h2h': stats
        }
    
    queries.append(pymongo.UpdateOne(
        {'_id': event['_id']},
        {'$set': {
            'stats': update_fields
        }}
    ))

    if ok_cnt > 0 and ok_cnt % batch_size == 0:
        collection.bulk_write(queries)
        queries = []
        # print time.time(), ok_cnt

    ok_cnt += 1


if queries:
    collection.bulk_write(queries)
