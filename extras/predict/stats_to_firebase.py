#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import config
import requests
import json
import firebase_wrapper
import re


#    NOTE: in order to use firebase, all . have been removed from names, in keys (data['events']['name.key']) and values data['events']['blah 1._der']['name'] = ['blah 1.', 'der']
#    NOTE: the only output to log should be unexpected errors
#    NOTE: order is NOT preserved. You will need to sort dictionary by commence in Javascript



def getSportsJson(sport_url):
    r = requests.get(sport_url)
    if r.status_code == 200:
        return json.loads(r.text)['data']

    print 'No sports returned from ', sport_url
    return []

def getStatsJson(stats_url):
    r = requests.get(stats_url)
    if r.status_code == 200:
        return json.loads(r.text)['data']

    print 'No stats returned from ', stats_url
    return []

def validate_firebase_key(key):
    chars_to_remove = '.$[]/#'
    rx = '[' + re.escape(''.join(chars_to_remove)) + ']'
    return re.sub(rx, '', key)

def formatEvents(sport, sportInfo, events):
    events_dict = {}
    for event in events:
        event_key = validate_firebase_key( '_'.join(event['teams']) )

        events_dict[ event_key ] = {
            # 'commence': ,
            'commenceStr': str(event['commence_time']),
            'status': 'Live' if event['commence_time'] <= time.time() else 'Pending',
            'name': [validate_firebase_key(team) for team in event['teams']],
            'sport': event['sport_key'],
            'sportDisp': event['sport_nice'],
            'stats': []
        }

        if event.get('stats', False) and event['stats'].get('h2h', False):
            for stat in event['stats']['h2h']:
                events_dict[ event_key ]['stats'].append({
                    'avg': stat['mean'],
                    'cnt': stat['count']
                })

        # Scores
        if event.get('score', False):
            score = event.get('score')
            events_dict[ event_key ]['evInfo'] = {
                'liveInfo': {
                    'score': {
                        validate_firebase_key(event['teams'][0]): score[0],
                        validate_firebase_key(event['teams'][1]): score[1]
                    }
                }
            }

        if event.get('bf_matched', False):
            if 'evInfo' in events_dict[ event_key ]:
                events_dict[ event_key ]['evInfo']['bfMatched'] = str(event['bf_matched'][0])
            else:
                events_dict[ event_key ]['evInfo'] = {
                    'bfMatched': str(event['bf_matched'][0])
                }

        if sport == 'upcoming':
            events_dict[ event_key ]['info'] = {
                'displayName': event['sport_nice'],
                'numOutcomes': len(event['stats']['h2h']) if 'h2h' in event.get('stats', {}) else 2
            }

    results = {
        'events': events_dict,
        'last_update': sportInfo.get('last_update', time.time())
    }

    if sport != 'upcoming':
        results['info'] = {
            'displayName': sportInfo['title'],
            'numOutcomes': len(events[-1]['stats']['h2h']) if 'stats' in events[-1] and 'h2h' in events[-1]['stats'] else 2
        }
    
    return results


if __name__ == '__main__':
    runOnce = False
    lastRefresh = 0
    last_write = {}
    sport_url = config.oddsApiEndpoint + 'sports?apiKey=%s&debug=1&oldSportKey=1' % (config.oddsApiKey)
    fbase = firebase_wrapper.initFirebase()

    while True:

        # Update top limit and odds frequently, update nearby events infrequently
        if time.time() > (lastRefresh + config.refreshInt * 60):
            activeSports = getSportsJson(sport_url)
            activeSports.append({
                'key': 'upcoming',
                'active': True,
                'title': 'Upcoming'
            })
 
            lastRefresh = time.time()

        for sportInfo in activeSports:

            if not sportInfo.get('active', False):
                continue

            try:

                sport = sportInfo.get('key')

                # if sport != 'upcoming' and sport in last_write and sportInfo.get('last_update', time.time()) < last_write[sport]:
                #    # Check if sport due for update. Scrape may not have run since last check
                #    continue

                stats_url = config.oddsApiEndpoint + 'stats?apiKey=%s&sport=%s' % (config.oddsApiKey, sport)
                stats = getStatsJson(stats_url)

                if len(stats) == 0:
                    writeStatus = firebase_wrapper.writeFirebase(fbase, sport, {})
                    continue

                # TODO
                # scores
                # bfMatched
                """
                NOTE k == event name
                try:
                    sportData['events'][k]['evInfo']['bfMatched'] = sportData['events'][k]['evInfo']['bfMatched']
                except:
                    pass

                try:
                    for k2 in sportData['events'][k]['evInfo']['liveInfo']['score']:
                        cleanEvents[ev]['evInfo']['liveInfo']['score'][ k2.replace('.','') ] = sportData['events'][k]['evInfo']['liveInfo']['score'][ k2 ]
                except:
                    pass

                """

                formattedStats = formatEvents(sport, sportInfo, stats)
                writeStatus = firebase_wrapper.writeFirebase(fbase, sport, formattedStats)

                if not writeStatus:
                    fbase = firebase_wrapper.initFirebase()

                else:
                    #print 'wrote ', sportRaw
                    last_write[sport] = time.time()

            except Exception as e:
                print sportInfo, e

        if runOnce: break
        time.sleep(config.sleepSec)
