from firebase import firebase
import config

def initFirebase():
    # Initiate and authorize firebase
    firebaseAuth = firebase.FirebaseAuthentication( secret = config.firebase['secret'], email = config.firebase['email'], admin = True)
    return firebase.FirebaseApplication(config.firebase['endpoint'], firebaseAuth)

def writeFirebase(fbase, sport, cleanSportData):
    try:
        fbase.put('/oddStats/' + config.country, sport, cleanSportData, params={'print': 'silent'})
        return True
    except Exception as e:
        print
        print
        print 'Firebase failed for ', sport, e
        print
        print cleanSportData
        return False

