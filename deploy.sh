#!/usr/bin/env bash

set -e # exit on error

# Run tests
python -m unittest discover tests/

# Deploy
git push production master

# Reset scripts
ssh OASCRAPE "cd /home/scrape; bash start.sh"
