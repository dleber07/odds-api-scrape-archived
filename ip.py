# TODO
# 
        def allocateNewIP( self, region, key, secret, instanceId ):

                # Ref http://stackoverflow.com/questions/22942812/aws-dynamically-allocate-associate-new-ip-addresses-to-ec2-instance
                #       http://boto.cloudhackers.com/en/latest/ref/ec2.html#boto.ec2.connection.EC2Connection.get_all_reservations
                conn = boto.ec2.connect_to_region(region, aws_access_key_id= key, aws_secret_access_key= secret )

                if not conn:
                        print 'EC2 connection failed in changeIP()'
                        return False

                reservations = conn.get_all_reservations(filters={'instance-id' : instanceId})

                instance = reservations[0].instances[0]

                old_address = instance.ip_address
                new_address = conn.allocate_address().public_ip

                conn.disassociate_address(old_address)
                conn.associate_address(instanceId, new_address)

                # Release the old address               
                old_address_obj = conn.get_all_addresses( filters={'public-ip' : old_address })[0]
                old_address_obj.release()


~                                            