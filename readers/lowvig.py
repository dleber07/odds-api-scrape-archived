from bs4 import BeautifulSoup
from components.validate import UTC_to_STR
from components import scrape_tools
from components import market_cleaner

def lowvig23(html, params, is_test):
    results = []
    soup = BeautifulSoup(html, 'lxml')

    for event in soup.select('tr.h2hSeq.firstline'):

        odds = {}

        team1_row = event
        team2_row = event.findNext('tr')
        teamD_row = team2_row.findNext('tr')

        event_info = scrape_tools.json_loads(team1_row.find('td', class_='col_rotno')['evcfg'])

        teams = [
            event_info['ltm'],
            event_info['rtm'],
        ]

        commence_time = int(UTC_to_STR(event_info['gmdt'].replace('\\', ''), '%m/%d/%Y %H:%M:%S'))

        markets_1 = team1_row.select('td.checkboxes.bdevtt2') # for team 1
        markets_2 = team2_row.select('td.checkboxes.bdevtt2') # for team 2
        markets_D = teamD_row.select('td.checkboxes.bdevtt2') # for draw


        try:
            # Spreads
            spreads_index = 0
            mktfields1 = scrape_tools.json_loads(markets_1[spreads_index].find('input')['cfg'])
            odd1 = mktfields1.get('d')
            point1 = mktfields1.get('horg')
            
            mktfields2 = scrape_tools.json_loads(markets_2[spreads_index].find('input')['cfg'])
            odd2 = mktfields2.get('d')
            point2 = mktfields2.get('horg')

            odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])
        except:
            pass

        try:
            # Moneylines
            moneyline_index = 1
            mktfields1 = scrape_tools.json_loads(markets_1[moneyline_index].find('input')['cfg'])
            odd1 = mktfields1.get('d')
            
            mktfields2 = scrape_tools.json_loads(markets_2[moneyline_index].find('input')['cfg'])
            odd2 = mktfields2.get('d')

            odds['h2h'] = [
                odd1,
                odd2
            ]

            try:
                mktfieldsD = scrape_tools.json_loads(markets_D[moneyline_index].find('input')['cfg'])
                odds['h2h'].append(mktfieldsD.get('d'))
            except:
                pass


        except:
            pass

        try:
            # Totals
            totals_index = 2
            mktfields1 = scrape_tools.json_loads(markets_1[totals_index].find('input')['cfg'])
            odd1 = mktfields1.get('d')
            point1 = 'over ' + str(mktfields1.get('h'))
            
            mktfields2 = scrape_tools.json_loads(markets_2[totals_index].find('input')['cfg'])
            odd2 = mktfields2.get('d')
            point2 = 'under ' + str(mktfields2.get('h'))

            odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
        except:
            pass   


        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': commence_time
        })



    return results

