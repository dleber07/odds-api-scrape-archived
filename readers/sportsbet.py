from components import validate, market_cleaner
import json
import time


def _get_home_team(teams, result_type):
    if result_type == 'H':
        return teams[0]

    return teams[1]

def sportsbet23(html, params, is_test):
    results = []

    j = json.loads(html)
    for event in j['events']:


        if event.get('bettingStatus') == 'OFF':
            # These may be long expired events
            continue
        

        if 'participant1' in event:
            teams = [
                event['participant1'],
                event['participant2'],
            ]

        else:
            try:
                # mma
                teams = event['displayName'].split(' v ')
            except:
                continue

        odds = {}
        home_team = None

        for mkt in event['marketList']:

            if mkt['name'] in ['Head to Head', 'Match', 'Win-Draw-Win', 'Match betting', 'Money Line']: # look at marketSort key

                home_team = _get_home_team(teams, mkt['selections'][0]['resultType'])

                if len(mkt['selections']) == 3:
                    odds['h2h'] = [
                        mkt['selections'][0]['price']['winPrice'],
                        mkt['selections'][2]['price']['winPrice'],
                        mkt['selections'][1]['price']['winPrice'],
                    ]
                elif len(mkt['selections']) == 2:
                    odds['h2h'] = [
                        mkt['selections'][0]['price']['winPrice'],
                        mkt['selections'][1]['price']['winPrice'],
                    ]
                else:
                    continue

            elif mkt['name'] in ['Handicap Betting', 'Handicap', 'Line']:

                if len(mkt['selections']) == 3:
                    # TODO - you should accept 3 outcome lines. see market_cleaner.line_clean3
                    continue
                
                try:
                    home_team = _get_home_team(teams, mkt['selections'][0]['resultType'])
                except:
                    pass

                odd1 = mkt['selections'][0]['price']['winPrice']
                point1 = mkt['selections'][0]['displayHandicap']
                odd2 = mkt['selections'][1]['price']['winPrice']
                point2 = mkt['selections'][1]['displayHandicap']
                odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])

            elif mkt['name'] in ['Total Match Points']:
                odd1 = mkt['selections'][0]['price']['winPrice']
                point1 =  mkt['selections'][0]['name'] + ' ' +  mkt['selections'][0]['unformattedHandicap']
                odd2 = mkt['selections'][1]['price']['winPrice']
                point2 = point1 =  mkt['selections'][1]['name'] + ' ' +  mkt['selections'][1]['unformattedHandicap']
                odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
            elif is_test:
                print 'Market unknown', mkt['name']
        if not odds:
            continue
        
        results.append({
            'teams': teams,
            'commence_time': event['startTime'],
            'odds': odds,
            'home_team': home_team # NOTE won't be popualted if no h2h mkt
        })

    return results
