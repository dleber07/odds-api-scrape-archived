from components.validate import UTC_to_STR
from components import market_cleaner
import time, json


def nordicbet23(html, params, isTest=False):

    jEvents = json.loads(html)
    results = []

    for event in jEvents.get('el', []):

        startTime = int(UTC_to_STR(event['sd'], "%Y-%m-%d %H:%M:%S"))
        teams = event['en'].split(' - ')

        odds = {}
        odd1, odd2, oddD = 0, 0, 0
        hasMarket = False

        # Assume home team is the first (seems a safe assumption)
        home_team = teams[0]

        for mkt in event.get('ml', []):
            odd1 = mkt['msl'][0]['msp']

            if len(mkt['msl']) == 3:
                oddD = mkt['msl'][1]['msp']
                odd2 = mkt['msl'][2]['msp']
            else:
                odd2 = mkt['msl'][1]['msp']

            if mkt['bgn'] in ['Money Line', 'Moneyline', 'Match result', 'Match winner', 'Match Winner', 'Moneyline (inc. OT)', 'Match Winner (inc. OT)', 'Matchwinner Cricket', 'Moneyline (Incl Extra Innings)']:
                hasMarket = True
                odds['h2h'] = [odd1, odd2]
                if oddD:
                    odds['h2h'].append(oddD)
                
                if mkt['msl'][0]['msti'] != 'HOME':
                    # When given the opportunity, verify that home team is first, otherwise correct
                    home_team = teams[1]

            elif mkt['bgn'] == 'Handicap': #lines
                hasMarket = True
                points = getLinePoint(mkt['lv'])
                odds['spreads'] = market_cleaner.line_clean([odd1, points[0], odd2, points[1]])

            elif mkt['bgn'] == 'Total Match Points': #ou
                hasMarket = True
                odds['totals'] = market_cleaner.totals_clean([
                    odd1,
                    mkt['msl'][0]['mst'],
                    odd2,
                    mkt['msl'][1]['mst']
                ])
            elif isTest:
                print 'Unknown market', mkt['bgn']

        if not hasMarket:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': startTime,
            #'home_team': home_team
        })

    return results

def getLinePoint(line_str):
    point1, point2 = line_str.split(' - ')
    if point1 == '0':
        point1 = '-' + point2
    else:
        point2 = '-' + point1

    return (point1, point2)
