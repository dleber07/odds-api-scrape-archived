from components import validate
import time, json

site = 'anonibet'
host = 'https://www.anonibet.com'

def _getH2H(mkt):
    #mkt['Odds'][0]['Value']['MinStake']
    #mkt['Odds'][0]['Value']['MaxStake']
    odd1 = validate.cleanOdd(mkt['Odds'][0]['Value'])

    if len(mkt['Odds']) == 3:
        oddD = validate.cleanOdd(mkt['Odds'][1]['Value'])
        odd2 = validate.cleanOdd(mkt['Odds'][2]['Value'])
    else:
        oddD = 0
        odd2 = validate.cleanOdd(mkt['Odds'][1]['Value'])


    return [odd1, odd2, oddD]


def _getLines(mkt):
    # TODO
    """
    lineOdd1 = mkt['selections'][0]['odds']
    linePoint1 = float(mkt['selections'][0]['special_bet_value'])
    lineOdd2 = mkt['selections'][1]['odds']
    linePoint2 = float(mkt['selections'][1]['special_bet_value'])

    linePoint2 = -1 * linePoint2
    
    return validate.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])
    """
    pass


def _getOU(mkt):
    # TODO
    """
    len(mkt['Odds'])
    ouOdd1 = mkt['Odds'][0]['Value']
    ouPoint1 = mkt['Odds'][0]['Name']
    ouOdd2 = mkt['Odds'][1]['Value']
    ouPoint2 = mkt['Odds'][1]['Name']
    return validate.ou_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
    """
    pass


def anonibet23(html, params, isTest = False):

    global site, host
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    j_events = json.loads(html)

    try:

        for event_holder in j_events:

            try:
                for event in event_holder['Events']:
                    h2h, lines, ou = [], {}, {}
                    evInfo = {}
                    evInfo['startTime'] = validate.UTC_to_STR(event['PlayTime'], '%Y-%m-%d %H:%M:%S')

                    try: link = host + event['Url']
                    except: link = host

                    team1, team2 = event['Label'].split(' - ')

                    try: team1 = participantMap[team1]
                    except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
                    if not team1: continue

                    try: team2 = participantMap[team2]
                    except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
                    if not team2: continue

                    teams = [team1, team2]

                    h2h = _getH2H(event)
                    """
                    # event['AllMarkets'] not reliable, was null for NFL
                    for mkt_holder in event['AllMarkets']:
                        mkt = mkt_holder['OddsGroup'][0]

                        if mkt['MarketId'] == 1: #h2h ("standard")
                            h2h = _getH2H(mkt)
                        
                        #elif mkt['MarketId'] == 192: # lines
                        #    lines = _getLines(mkt)
                        
                        #elif mkt['MarketId'] == 594: # ou
                        #    ou = _getOU(mkt)

                        #Other market ids
                        # 5 == Double Chance
                        # 4 == 1st Half
                        # 3 == half time / full time
                        # 359 == both teams score
                    """

                    eventName = validate.makeEventName(team1, team2)
                    results[eventName] = {
                        'teams': teams,
                        'odds': {'H2H': h2h, 'lines': lines, 'ou': ou},
                        'link': link,
                        'evInfo': evInfo,
                    }

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results
