import json
from components import validate
from components import market_cleaner

def mbit23(html, params, test=False):
    results = []

    for event in json.loads(html):
        odds = {}
        teams = [
            event['HomeTeamName'],
            event['AwayTeamName']
        ]

        home_team = event['HomeTeamName']

        # NOTE odds assume same order as home team vs away team

        # Moneyline
        if len(event.get('PreviewOddsMoneyLine', [])) == 3:
            odds['h2h'] = [
                event['PreviewOddsMoneyLine'][0]['Value'],
                event['PreviewOddsMoneyLine'][2]['Value'],
                event['PreviewOddsMoneyLine'][1]['Value']
            ]
        elif len(event.get('PreviewOddsMoneyLine', [])) == 2:
            odds['h2h'] = [
                event['PreviewOddsMoneyLine'][0]['Value'],
                event['PreviewOddsMoneyLine'][1]['Value']
            ]

        # Spreads
        if event.get('PreviewOddsHandicap'):
            try:
                odd1 = event['PreviewOddsHandicap'][0]['Value']
                point1 = event['PreviewOddsHandicap'][0]['TitleSuffix']

                odd2 = event['PreviewOddsHandicap'][1]['Value']
                point2 = event['PreviewOddsHandicap'][1]['TitleSuffix']
                odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])
            except:
                pass

        # Totals
        if event.get('PreviewOddsTotal') and len(event['PreviewOddsTotal']) >= 2:
            try:
                odd1 = event['PreviewOddsTotal'][0]['Value']
                point1 = event['PreviewOddsTotal'][0]['OriginalTitle']

                odd2 = event['PreviewOddsTotal'][1]['Value']
                point2 = event['PreviewOddsTotal'][1]['OriginalTitle']
                odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
            except:
                pass

        if not odds:
            continue

        results.append({
            'teams': teams,
            'home_team': home_team,
            'odds': odds,
            'commence_time': int(validate.UTC_to_STR(event['DateOfMatch'], '%Y-%m-%d %H:%M:%S')),
            #  TODO consider event['Score']
        })


        pass

    return results