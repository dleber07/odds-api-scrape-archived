from components import validate
import time
import re
import lxml
from bs4 import BeautifulSoup

site =  'partypoker'


def fracToDec(odd):
    if str(odd).find('/') == -1:
        return odd

    v_top, v_bottom = odd.split('/')
    return (float(v_bottom) + float(v_top))/float(v_bottom)


def partypoker23(html, params, isTest=False):
    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    soup = BeautifulSoup(html, "lxml")
    events = soup.select('div.ajax-top-market') or soup.select('div.event-list')
    for event in events:

        try:
            odd1, odd2, oddD = 0, 0, 0
            odds, teams = [], []
            team1, team2 = '', ''

            #  Outrights can appear at the end of matches. Check for 'Winner' in market heading to exclude these
            #   warning: possible it excludes valid matches for some sports. Unknown.
            bettingTable = event.find(class_='betting-table')
            mkt = bettingTable.find('h2').text
            if ' Winner' in mkt:
                continue

            outcomeRows = bettingTable.select('a.result')

            # Extract event url
            urlPath = url[url.find('/sports/main'): ]
            if urlPath.count('/') == 5:
                # just add event id (league id already present in url)
                link = url + '/' + outcomeRows[0]['data-eventid']
            else:
                link = url + '/' + outcomeRows[0]['data-leagueid'] + '/' + outcomeRows[0]['data-eventid']


            team1 = outcomeRows[0].find(class_='resultname').text
            odd1 = outcomeRows[0].find(class_='odds').text

            if len(outcomeRows) == 3:
                team2 = outcomeRows[2].find(class_='resultname').text
                odd2 = outcomeRows[2].find(class_='odds').text
                oddD = outcomeRows[1].find(class_='odds').text
            else:
                team2 = outcomeRows[1].find(class_='resultname').text
                odd2 = outcomeRows[1].find(class_='odds').text


            try: team1 = participantMap[team1]
            except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
            if not team1: continue

            try: team2 = participantMap[team2]
            except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
            if not team2: continue

            # Assume odd is fraction, convert to decimal
            odd1 = fracToDec(odd1)
            odd2 = fracToDec(odd2)
            oddD = fracToDec(oddD)

            odd1 = validate.cleanOdd(odd1)
            odd2 = validate.cleanOdd(odd2)
            oddD = validate.cleanOdd(oddD)

            odds = [odd1, odd2]
            if oddD: odds.append(oddD)

            eventName = validate.makeEventName(team1, team2)

            if eventName not in results:
                results[eventName] = {
                    'teams': [team1, team2],
                    'odds': {'H2H' : odds},
                    'link': link
                }

        except Exception as e:
            if isTest: print sport, site, scrapeMethod, e


    return results

