from components import market_cleaner
import time, json


def _resolve_mkt(mkt):
    """
    Examples

    h2h
        "specialBetValue": "back,ot",
        "specialBetValue": "back,ft",
        "outcome": "1", "outcome": "2"

        "specialBetValue": "ft",
        

    spreads
        "specialBetValue": "ot,3,0:0",
        "outcome": "1", "outcome": "2",

        "specialBetValue": "ft,0.25,0:0",


    toatls
        "specialBetValue": "ot,43,",
        "outcome": "over", "outcome": "under"

        "specialBetValue": "ft,2.75,",

    """

    if mkt['selections'][0]['outcome'] in ['over', 'under']:
        return 'totals'

    if mkt['selections'][0]['outcome'] == '1' and mkt['selections'][1]['outcome'] == '2' and mkt['selections'][0].get('specialBetValue') is None:
        # afl has no specialBetValue field
        return 'h2h'

    if mkt['selections'][0]['outcome'] == '1' and mkt['selections'][1]['outcome'] == '2' and mkt['selections'][0]['specialBetValue'] == 'ft' and mkt['selections'][1]['specialBetValue'] == 'ft':
        # mma
        return 'h2h'

    if mkt['selections'][0]['outcome'] == '1' and mkt['selections'][1]['outcome'] == '2' and ',back,' in mkt['selections'][0]['specialBetValue'] and ',back,' in mkt['selections'][1]['specialBetValue']:
        return 'h2h'

    return 'spreads'

def _getH2H(mkt):
    h2h = [
        mkt['selections'][0].get('odds', 0),
        mkt['selections'][1].get('odds', 0)
    ]

    if len(mkt['selections']) == 3:
        h2h.append(mkt['selections'][2]['odds'])
 
    return h2h


def _getLines(mkt):
    lineOdd1 = mkt['selections'][0]['odds']
    linePoint1 = mkt['selections'][0]['specialBetValue']
    lineOdd2 = mkt['selections'][1]['odds']
    linePoint2 = mkt['selections'][1]['specialBetValue']

    if ',' in linePoint1:
        linePoint1 = linePoint1.split(',')[1]

    if ',' in linePoint2:
        linePoint2 = linePoint2.split(',')[1]

    linePoint1 = float(linePoint1)
    linePoint2 = -1 * float(linePoint2)
    
    return market_cleaner.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])

def _getOU(mkt):
    ouOdd1 = mkt['selections'][0]['odds']
    ouPoint1 = mkt['selections'][0]['outcome'] + ' ' + mkt['selections'][0]['specialBetValue'].replace('ot,', '')
    ouOdd2 = mkt['selections'][1]['odds']
    ouPoint2 = mkt['selections'][1]['outcome'] + ' ' + mkt['selections'][1]['specialBetValue'].replace('ot,', '')
    return market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])


def _get_stakes_h2h(mkt):
    stakesh2h = [
        [ 0, mkt['selections'][0]['maxStake'] ],
        [ 0, mkt['selections'][1]['maxStake'] ],
    ]

    if len(mkt['selections']) == 3:
        stakesh2h.append(
            [ 0, mkt['selections'][2]['maxStake'] ]
        )

    return stakesh2h

def cloudbet23(html, params, isTest = False):

    j_events = json.loads(html)
    results = []

    for event in j_events['events']:
        
        odds = {}
        stakes = {}

        teams = [
            event['competitors'][0]['name'],
            event['competitors'][1]['name']
        ]

        if 'markets' not in event:
            continue

        for mkt in event['markets']:

            try:
                mktname = _resolve_mkt(mkt)
            except:
                continue

            if mktname == 'h2h':
                odds['h2h'] = _getH2H(mkt)
                stakes['h2h'] = _get_stakes_h2h(mkt)
            
            elif mktname == 'spreads':
                try:
                    odds['spreads'] = _getLines(mkt)
                except:
                    pass
            
            elif mktname == 'totals':
                try:
                    odds['totals'] = _getOU(mkt)
                except:
                    pass

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'stakes': stakes,
            'commence_time': int(event['startsAt']),
            # 'home_team': teams[0]
        })

    return results
