import json
from components.validate import UTC_to_STR, frac_to_decimal
import components.market_cleaner as market_cleaner


def betfred23(html, params, is_test):
    accepted_markets = {
        'h2h': ['Match Result', 'Moneyline', 'Match Result (No Ot)', 'Money Line', 'Match Winner', 'Fight Winner', 'Fight'],
        'spreads': ['Handicap  - Middle Line'],
        'totals': ['Total Points U/O  - Middle Line']
    }

    results = []
    j = json.loads(html)

    for event in j['Marketgroup'].get('markets'):
        odds = {}

        if 'participantname_home' in event:

            teams = [
                event['participantname_home'],
                event['participantname_away']
            ]
        else:
            # MMA only has short names
            teams = [
                event['participantshortname_home'],
                event['participantshortname_away']               
            ]

        home_team = teams[0]
        selections = event['selections']

        if event['name'] in accepted_markets['h2h']:

            odd1, odd2, oddD = 0, 0, 0

            for selection in selections:
                if selection.get('hadvalue', '') == 'H':
                    odd1 = frac_to_decimal(float(selection['currentpriceup']), float(selection['currentpricedown']))
                elif selection.get('hadvalue', '') == 'A':
                    odd2 = frac_to_decimal(float(selection['currentpriceup']), float(selection['currentpricedown']))
                elif selection.get('hadvalue', '') == 'D':
                    oddD = frac_to_decimal(float(selection['currentpriceup']), float(selection['currentpricedown']))

            odds['h2h'] = [odd1, odd2]
            if oddD:
                odds['h2h'].append(oddD)


        elif event['name'] in accepted_markets['spreads']:
            odd1 = frac_to_decimal(float(selections[0]['currentpriceup']), float(selections[0]['currentpricedown']))
            point1 = float(selections[0]['currenthandicap'])
            odd2 = frac_to_decimal(float(selections[1]['currentpriceup']), float(selections[1]['currentpricedown']))
            point2 = point1 * -1
            odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])
            

        elif event['name'] in accepted_markets['totals']:
            odd1 = frac_to_decimal(float(selections[0]['currentpriceup']), float(selections[0]['currentpricedown']))
            odd2 = frac_to_decimal(float(selections[1]['currentpriceup']), float(selections[1]['currentpricedown']))

            point1 = float(selections[0]['currenthandicap'])
            if point1 > 0:
                point1 = 'over ' + str(point1)
                point2 = 'under ' + str(point1)
            else:
                point1 = 'under ' + str(point1)
                point2 = 'over ' + str(point1)

            odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': int(UTC_to_STR(event['tsstartiso'], '%Y-%m-%d %H:%M:%S')),
            'home_team': home_team
        })

    return results


def betfred23_multi(html, params, is_test):

    """
        Use this method with "betfred" fetcher
        html is a list of sets of events, each for a different market

    """
    results = []

    for mktset in json.loads(html):
        results += betfred23(mktset, params, is_test)
    
    results = merge_markets(results)

    return results

def merge_markets(results):
    event_indexes = {}
    results_merged = []

    for event in results:

        event_name = '_'.join(event['teams'])
        if event_name in event_indexes and results_merged[event_indexes[event_name]]['commence_time'] == event['commence_time']:
            results_merged[event_indexes[event_name]]['odds'].update(event['odds'])
        else:
            results_merged.append(event)
            event_indexes[event_name] = len(results_merged) - 1
    
    return results_merged

