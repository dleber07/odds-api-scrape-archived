from bs4 import BeautifulSoup
import lxml
# from components import validate

def marathonbet23(html, params, test=False):
    results = []
    soup = BeautifulSoup(html, 'lxml')

    for event in soup.find_all('div', attrs={'data-event-name':True}):
        try:
            link = 'https://www.marathonbet.com/en/events.htm?id=' + event['data-event-treeid']
        except:
            link = params['scrape_config']['url']

        evName = event['data-event-name']
        if evName.find(' @ ') > -1:
            teams = evName.split(' @ ')
        else:
            teams = evName.split(' vs ')

        oddD = 0

        oddTd = event.find('tr').find_all('td', class_='price')
        odd1 = oddTd[0].find('span')['data-selection-price']

        if len(oddTd) == 3:
            oddD = oddTd[1].find('span')['data-selection-price']
            odd2 = oddTd[2].find('span')['data-selection-price']

        else:
            odd2 = oddTd[1].find('span')['data-selection-price']

        odds = {
            'h2h': [odd1, odd2]
        }

        if oddD:
            odds['h2h'].append(oddD)

        results.append({
            'teams': teams,
            'odds': odds
        })

    return results
