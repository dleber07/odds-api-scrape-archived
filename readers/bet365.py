from components import validate, market_cleaner
from bs4 import BeautifulSoup
import json
import lxml


def bet365(results, params, test=False):
    market_events = {}

    for market, payload in json.loads(results).iteritems():
        
        if payload['type'] == '3':
            market_events[market] = _get_events_3(payload['html'], params, market, test)
        elif payload['type'] == '2':
            market_events[market] = _get_events_2(payload['html'], params, market, test)

    return _merge_markets(market_events)

def _merge_markets(market_events):
    event_map = {}
    
    for market, events in market_events.items():
        for event in events:

            event_key = '_'.join(event['teams'])
            if event_key in event_map:
                event_map[event_key]['odds'][market] = event['odds'][market]

            else:
                event_map[event_key] = dict(event)
        
    return [ event for event_name, event in event_map.iteritems() ]


def _get_events_2(html, params, market, test=False):
    """
        Reads body tag from mobile
    """

    results = []
    soup = BeautifulSoup(html, 'lxml')
    
    odd_elems = {}

    odds_select = 'span.cm-ParticipantOddsWithHandicapDonBest_Odds'
    points_select = 'span.cm-ParticipantOddsWithHandicapDonBest_Handicap'

    totals_odds_select = 'span.cm-ParticipantCenteredAndStackedDonBest_Odds'
    totals_points_select = 'span.cm-ParticipantCenteredAndStackedDonBest_Handicap'

    for market_col in soup.find_all(class_='sl-MarketCouponAdvancedBase'):

        if market_col.find(class_='gl-MarketColumnHeader').text == 'Money Line':
            odd_elems['h2h'] = {
                'odds': market_col.select(odds_select)
            }

        elif market_col.find(class_='gl-MarketColumnHeader').text == 'Spread':
            odd_elems['spreads'] = {
                'odds': market_col.select(odds_select),
                'points': market_col.select(points_select)
            }

        elif market == 'totals' and market_col.select(totals_odds_select):

            # Fall back on market input
            odd_elems[market] = {
                'odds': market_col.select(totals_odds_select),
                'points': market_col.select(totals_points_select)
            }

    i = 0
    for event in soup.find(class_='gl-MarketGroupContainer').select('div.cm-ParticipantWithBookClosesDonBest'):

        odds = {}
        
        team_elems = event.find_all(class_='cm-ParticipantWithBookClosesDonBest_TeamNameTruncator')
        if len(team_elems) != 2:
            continue

        team_elem1, team_elem2 = team_elems
        team1 = team_elem1.text.replace('\n', '').strip()
        team2 = team_elem2.text.replace('\n', '').strip()

        if 'h2h' in odd_elems:
            odds['h2h'] = [
                odd_elems['h2h']['odds'][i].text,
                odd_elems['h2h']['odds'][i+1].text
            ]

        if 'spreads' in odd_elems:
            odd1 = odd_elems['spreads']['odds'][i].text
            odd2 = odd_elems['spreads']['odds'][i+1].text
            point1 = odd_elems['spreads']['points'][i].text
            point2 = odd_elems['spreads']['points'][i+1].text

            if point1 and point2:
                try:
                    odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])
                except:
                    pass

        if 'totals' in odd_elems:
            odd1 = odd_elems['totals']['odds'][i].text
            odd2 = odd_elems['totals']['odds'][i+1].text
            point1 = 'over ' + odd_elems['totals']['points'][i].text.replace('O', '')
            point2 = 'under ' + odd_elems['totals']['points'][i+1].text.replace('U', '')
            
            if point1 and point2:
                try:
                    odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
                except:
                    pass

        i += 2
        results.append({
            'teams': [team1, team2],
            'odds': odds
        })

    return results


def _get_events_3(html, params, market, test=False):
    # NOTE - you can use this function for other markets
    results = []
    soup = BeautifulSoup(html, 'lxml')
    
    # cols is a list of bs4 classes
    cols_classes = soup.find('div', class_='gl-MarketGroup_Wrapper').select('div.gl-Market_General')

    # Turn each col into a list of direct children
    cols = [col.find_all(recursive=False) for col in cols_classes]

    for i, event_cell in enumerate(cols[0]):

        if 'MarketColumnHeader' in ' '.join(event_cell['class']):
            continue

        odds = {}
        
        try:
            event_names = event_cell.find_all(class_='cm-ParticipantWithBookCloses_Name')
            team1 = event_names[0].text
            team2 = event_names[1].text

            odds['h2h'] = [
                cols[1][i].find(class_='cm-ParticipantOddsWithHandicap_Odds').text,
                cols[3][i].find(class_='cm-ParticipantOddsWithHandicap_Odds').text,
                cols[2][i].find(class_='cm-ParticipantOddsWithHandicap_Odds').text
            ]

            results.append({
                'teams': [team1, team2],
                'odds': odds
            })

        except:
            continue


    return results
