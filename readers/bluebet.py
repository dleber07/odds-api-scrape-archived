from components import validate
import time, json

site = 'bluebet'
host = 'https://www.bluebet.com.au/sports/'

def bluebet23(html, params, isTest = False):

    global site, host
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    j_events = json.loads(html)

    try:
        for event in j_events["MasterCategories"][0]['Categories'][0]['MasterEvents']:

            try:

                odd1, odd2, oddD = 0, 0, 0
                lineOdds1, linePoints1, lineOdds2, linePoints2 = 0, 0, 0, 0
                lines, ou = {}, {}
                evInfo = {}

                outcomeBlocks = event['Markets']
                bbEvName = event['MasterEventName']
                bbEvId = event['MasterEventId']

                if outcomeBlocks[0]['MarketTypeCode'] != 'WIN' or outcomeBlocks[1]['MarketTypeCode'] != 'WIN':
                    continue

                team1 = outcomeBlocks[0]['OutcomeName']
                odd1 = outcomeBlocks[0]['Price']

                if len(outcomeBlocks) == 3:
                    team2 = outcomeBlocks[2]['OutcomeName']
                    odd2 = validate.cleanOdd(outcomeBlocks[2]['Price'])
                    oddD = validate.cleanOdd(outcomeBlocks[1]['Price'])

                else:
                    team2 = outcomeBlocks[1]['OutcomeName']
                    odd2 = validate.cleanOdd(outcomeBlocks[1]['Price'])

                try: team1 = participantMap[team1]
                except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
                if not team1: continue

                try: team2 = participantMap[team2]
                except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
                if not team2: continue

                teams = [team1, team2]

                try:
                    link = host + j_events['EventTypeDesc'] + '/' + j_events["MasterCategories"][0]['MasterCategory'].replace(' ', '-') + '/' + j_events["MasterCategories"][0]['Categories'][0]['CategoryName'].replace(' ', '-') + '/' + bbEvName.replace(' ', '-') + '/' + str(bbEvId) + '/'
                except:
                    link = host

                odds = {'H2H' : [odd1, odd2, oddD]}
                eventName = validate.makeEventName(team1, team2)

                results[eventName] = {
                    'teams': teams,
                    'odds': odds,
                    'link': link
                }

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results
