import json
from components import market_cleaner

"""
    NOTE
    URL has param: preMatchOnly=true
    if you can't return preMatch and live in same request, 
        create a fetcher that first sends the prematch url as normal, and then replaces preMatchOnly with liveOnly in the url and sends again
    The website sends both requests
"""

def bovada23(html, params, isTest=False):

    results = []
    j = json.loads(html)

    for event in j[0]['events']:

        odds = {}

        teams = [
            event['competitors'][0]['name'],
            event['competitors'][1]['name']
        ]

        home_team = event['competitors'][0]['name'] if event['competitors'][0]['home'] else event['competitors'][1]['name']

        for mkt in event['displayGroups'][0]['markets']:

            if not mkt['outcomes']:
                continue

            if mkt['description'] in ['Point Spread', 'Goal Spread']:

                if mkt['outcomes'][1]['description'] == home_team:
                    home_index, away_index = 1, 0
                else:
                    home_index, away_index = 0, 1

                odds['spreads'] = market_cleaner.line_clean([
                    mkt['outcomes'][home_index]['price']['decimal'],
                    mkt['outcomes'][home_index]['price']['handicap'],
                    mkt['outcomes'][away_index]['price']['decimal'],
                    mkt['outcomes'][away_index]['price']['handicap'],
                ])

            if mkt['description'] in ['Moneyline', '2-Way Moneyline', '3-Way Moneyline']:

                if len(mkt['outcomes']) < 2:
                    # Invalid data (seen in french open). Go to next market
                    continue

                if mkt['outcomes'][1]['description'] == home_team:
                    home_index, away_index = 1, 0
                else:
                    home_index, away_index = 0, 1

                odds['h2h'] = [
                    mkt['outcomes'][home_index]['price']['decimal'],
                    mkt['outcomes'][away_index]['price']['decimal'],
                ]
                
                if len(mkt['outcomes']) == 3 and mkt['outcomes'][2]['description'] == 'Draw':
                    odds['h2h'].append(mkt['outcomes'][2]['price']['decimal'])


            if mkt['description'] == 'Total':
                odds['totals'] = market_cleaner.totals_clean([
                    mkt['outcomes'][0]['price']['decimal'],
                    mkt['outcomes'][0]['description'] + mkt['outcomes'][0]['price']['handicap'],
                    mkt['outcomes'][1]['price']['decimal'],
                    mkt['outcomes'][1]['description'] + mkt['outcomes'][1]['price']['handicap'],
                ])
            
        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': event['startTime'] / 1000,
            'home_team': home_team
        })
    
    return results
