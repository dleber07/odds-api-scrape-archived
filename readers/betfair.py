import json
from components import validate, market_cleaner
import re

# RETURN_FACTOR = 0.95

def betfair23(html, params, isTest=False):

    j = json.loads(html)
    results = []

    for eventset in j:

        for event in eventset['eventTypes'][0]['eventNodes']:

            startTime = int(validate.UTC_to_STR(event['event']['openDate'], '%Y-%m-%d %H:%M:%S.%f'))
            mkt = event['marketNodes'][0]

            if mkt['description']['marketType'] not in ['MATCH_ODDS', 'MONEY_LINE']:
                continue

            hasDraw = (mkt['state']['numberOfRunners'] == 3)

            try: 
                totalMatched = round(mkt['state']['totalMatched'], 2)
            except:
                totalMatched = 0

            #mkt['state']['totalAvailable']
            try:
                team1 = mkt['runners'][0]['description']['runnerName']
                odd1 = mkt['runners'][0]['exchange']['availableToBack'][0]['price']
                layOdd1 = mkt['runners'][0]['exchange']['availableToLay'][0]['price']

                team2 = mkt['runners'][1]['description']['runnerName']
                odd2 = mkt['runners'][1]['exchange']['availableToBack'][0]['price']
                layOdd2 = mkt['runners'][1]['exchange']['availableToLay'][0]['price']

                h2h = [odd1, odd2]
                h2hLay = [layOdd1, layOdd2]

                if hasDraw:            
                    oddD = mkt['runners'][2]['exchange']['availableToBack'][0]['price']
                    layOddD = mkt['runners'][2]['exchange']['availableToLay'][0]['price']
                    h2h.append(oddD)
                    h2hLay.append(layOddD)
            except Exception as e:
                # common errors include missing keys: availableToBack, availableToLay
                # for events with no money on them yet
                continue

            odds = {
                'h2h' : h2h,
                'h2h_lay': h2hLay, # See note below
            }

            results.append({
                'teams': [team1, team2],
                'odds': odds,
                'commence_time': startTime,
                'other': {
                    'matched': totalMatched
                }
            })

    return results

def betfair_headless(html, params, test=False):
    from bs4 import BeautifulSoup

    results = []
    soup = BeautifulSoup(html, 'lxml')

    for event in soup.select('table.coupon-table tbody tr'):
        team1 = False
        team2 = False

        teams = event.select('ul.runners > li')
        if not teams:
            continue

        team1 = teams[0].string
        team2 = teams[1].string

        if not team1 or not team2:
            continue

        outcomes = event.select('div.coupon-runner')
        backs = event.select('div.coupon-runner button.back-button')
        lays = event.select('div.coupon-runner button.lay-button')
    
        if len(outcomes) == 2:
            odds = {
                'h2h': [
                    backs[0]['price'],
                    backs[1]['price']
                ],
                'h2h_lay': [
                    lays[0]['price'],
                    lays[1]['price'],
                ],
                'h2h_size': [
                    _clean_size(backs[0]['size']),
                    _clean_size(backs[1]['size'])
                ],
                'h2h_lay_size': [
                    _clean_size(lays[0]['size']),
                    _clean_size(lays[1]['size'])
                ]
            }

        elif len(outcomes) == 3:
             odds = {
                'h2h': [
                    backs[0]['price'],
                    backs[2]['price'],
                    backs[1]['price'],
                ],
                'h2h_lay': [
                    lays[0]['price'],
                    lays[2]['price'],
                    lays[1]['price']
                ],
                'h2h_size': [
                    _clean_size(backs[0]['size']),
                    _clean_size(backs[2]['size']),
                    _clean_size(backs[1]['size']),
                ],
                'h2h_lay_size': [
                    _clean_size(lays[0]['size']),
                    _clean_size(lays[2]['size']),
                    _clean_size(lays[1]['size'])
                ]
            }
        else:
            continue

        results.append({
            'teams': [team1, team2],
            'odds': odds
        })

    return results

def _clean_size(size):
    """ 
        Remove non-numerics (such as $) from html matched amount
    """
    try: 
        return float(re.sub('[^0-9]', '', size))
    except:
        return 0
