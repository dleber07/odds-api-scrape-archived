#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import lxml
from components import market_cleaner
import json


def palmerbet23(html, params, isTest=False):
    # 2 Outcome Sports

    results = []
    soup = BeautifulSoup(html, 'lxml')

    header_tds = soup.find('table', class_ = 'odds-grid').find('tr').find_all('td')
    headers = [ td.text for td in header_tds ]

    for mb in soup.find("table", class_="odds-grid").find("tbody").find_all("tr"):

        odds = {}
        tds = mb.find_all("td")

        try:
            teams = tds[1].find("a").decode_contents(formatter="html")
        except:
            teams =tds[1].decode_contents(formatter="html")

        teams = teams.split('<br/>')
        team1 = teams[0].replace(' vs','')
        team2 = teams[1]
        teams = [team1, team2]

        odd1, odd2, oddDraw = 0, 0, 0
        oddsHtml = tds[2].find_all("a")

        #    H2H
        if 'AWin' in oddsHtml[0]['data-product']:
            odd1 = oddsHtml[0]['data-div']

        if 'BWin' in oddsHtml[1]['data-product']:
            odd2 = oddsHtml[1]['data-div']

        try:
            if tds[3].find("a")["data-product"] == "Draw":
                oddDraw = tds[3].find("a")["data-div"]
        except:
            pass

        odds = {
            'h2h': [odd1, odd2]
        }

        if oddDraw:
            odds['h2h'].append(oddDraw)

        # Spreads
        if 'Line' in headers:
            i = headers.index('Line')
            lineTD = tds[i].find_all("a")

            if 'ALine' in lineTD[0]['data-product']:
                odd1 = lineTD[0]['data-div']
                point1 = lineTD[0]['data-low']

            if 'BLine' in lineTD[1]['data-product']:
                odd2 = lineTD[1]['data-div']
                point2 = lineTD[1]['data-low']

            odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])


        # Totals
        if 'O/U' in headers:
            i = headers.index('O/U')
            ouTD = tds[i].find_all("a")

            if 'Total' in ouTD[0]['data-product']:
                odd1 = ouTD[0]['data-div']
                point1 = ouTD[0]['data-product'] + ' ' + ouTD[0]['data-low']

            if 'Total' in ouTD[1]['data-product']:
                odd2 = ouTD[1]['data-div']
                point2 = ouTD[1]['data-product'] + ' ' + ouTD[1]['data-high']

            odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
    
        if odds:
            results.append({
                'teams': [team1, team2],
                'odds': odds
            })

    return results


def palmer0(html, params, isTest=False):

    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    sportName, eventName = sport.split('___')
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    outrightEventName = params['scrapeInfo']['outrightEventName']
    oddThreshold = params['sportInfo']['oddThreshold']

    url = params['url']
    results = {}

    soup = BeautifulSoup(html)
    link = url
    hashData = {}

    try:
        try: oddThreshold = float(oddThreshold)
        except: oddThreshold = False

        for mb in soup.find("table", class_="odds-grid").find("tbody").find_all("tr"):

            try:
                tds = mb.find_all("td")
                team = tds[0].string
                try: team = participantMap[team]
                except: team = validate.resolveParticipant(outcomeType, team, listName, sport, site)
                if not team: continue

                odd = tds[1].find(text=True)
                odd = validate.cleanOdd(odd)
                if not odd: continue

                if team not in hashData and isValidOutrightOdd(odd, oddThreshold):
                    hashData[team] = odd

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    results[eventName] = {
        'odds': hashData,
        'link': link
    }

    return results
