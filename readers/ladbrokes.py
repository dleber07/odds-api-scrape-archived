from components import validate, market_cleaner
from bs4 import BeautifulSoup
import json
import lxml
import json


MARKETS_H2H = ['Match Betting', 'Match Result', 'Match Betting (Inc ET)', 'Head To Head', 'Fight Betting']
MARKETS_SPREAD = []
MARKETS_TOTALS = []
    

def ladbrokes23(html, params, test):
    results = []
    j = json.loads(html)
    prices = _construct_prices(j)

    for event_id, event in j['events'].iteritems():

        if event['match_status'] != 'BettingOpen':
            continue

        commence_time = event['advertised_start']['seconds']

        teams = []
        odds = {}
        
        # TODO
        # consider alternative way of extracting teams
        # teams = event['name'].split(' vs ')
    
        for market_id in event['main_markets']:
            market = j['markets'][market_id]
            
            if market['name'] in MARKETS_H2H:

                home_team = _get_home_team(j, market['entrant_ids'])

                if len(market['entrant_ids']) == 3:
                    teams = [
                        _get_team_name(j, market['entrant_ids'][0]),
                        _get_team_name(j, market['entrant_ids'][2])
                    ]

                    odds['h2h'] = [
                        _get_odd(prices, market['entrant_ids'][0]),
                        _get_odd(prices, market['entrant_ids'][2]),
                        _get_odd(prices, market['entrant_ids'][1]),
                    ]

                else:
                    teams = [
                        _get_team_name(j, market['entrant_ids'][0]),
                        _get_team_name(j, market['entrant_ids'][1])

                    ]

                    odds['h2h'] = [
                        _get_odd(prices, market['entrant_ids'][0]),
                        _get_odd(prices, market['entrant_ids'][1])
                    ]
                
        if not teams:
            continue

        results.append({
            'home_team': home_team,
            'commence_time': commence_time,
            'teams': teams,
            'odds': odds,
        })
    
    return results
        
def _get_team_name(j, entrant_id):
    return j['entrants'][entrant_id]['name']

def _get_home_team(j, entrant_ids):
    for entrant_id in entrant_ids:
        if j['entrants'][entrant_id]['home_away'] == 'HOME':
            return j['entrants'][entrant_id]['name']

    return None


def _construct_prices(j):
    """ Reconstruct the prices key to use id of the entrant only
    
    """
    return {price['entrant_id']: price for id, price in j['prices'].iteritems()}

def _get_odd(prices, entrant_id):
    return prices[entrant_id]['odds']['decimal']








# Old methods

def ladbrokes23_multi(html, params, test=False):
        results = []
        for page in json.loads(html):
                results += ladbrokes23(page, params, test)
        return results


def ladbrokes23_old(html, params, test=False):
        j = _extract_page_json(html)
        # events = j['payload'][0]["rows"].itervalues().next().itervalues().next()["Matches"]

        events = []
        comps = j['payload'][0]["rows"].itervalues().next().itervalues()
        for comp in list(comps):
                events += comp['Matches']

        return _read_matches_23(params, events)

def _extract_page_json(html):
        java_start_string = '<script type="text/javascript">Delegator.init('
        start_pos = html.find(java_start_string)
        if start_pos == -1:
                # No JSON found
                return {}

        start_pos += len(java_start_string)
        end_pos = html.find(");", start_pos)
        json_str = html[start_pos : end_pos]
        return json.loads(json_str)


"""
        Args:
                events (dict)
        Returns:
                list
"""
def _read_matches_23(params, events):
        results = []
        for event in events:
                if not event.get("HasWinOdds"):
                        continue

                try:
                        link = 'https://m.ladbrokes.com.au/sports/' + event['Sport'].replace(' ','-').lower() + '/' + str(event["EventID"])
                except:
                        link = params['scrape_config']['url']

                odds = {}

                teams = [
                        event["Competitors"][0]["Name"],
                        event["Competitors"][1]["Name"]
                ]

                odds['h2h'] = [
                        event["Competitors"][0]["Win"],
                        event["Competitors"][1]["Win"]
                ]

                if event["HasDrawOdds"] == True:
                        odds['h2h'].append(event["Draw"])

                if event["HasLineOdds"] == True:
                        lineOdd1 = event["Competitors"][0]["LineDiv"]
                        lineOdd2 = event["Competitors"][1]["LineDiv"]
                        linePoint1 = event["Competitors"][0]["Line"]
                        linePoint2 = event["Competitors"][1]["Line"]
                        spreads = market_cleaner.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])
                        if spreads:
                                odds['spreads'] = spreads


                if event["HasTotalPointsOdds"] == True or event["HasUnderOverOdds"] == True:
                        ouOdd1 = event["Competitors"][0]["TotalPoints"]
                        ouOdd2 = event["Competitors"][1]["TotalPoints"]
                        ouPoint1 = event["Competitors"][0]["UnderOverID"] + ' ' + str(event["Competitors"][0]["TotalPointsHandicap"])
                        ouPoint2 = event["Competitors"][1]["UnderOverID"] + ' ' + str(event["Competitors"][1]["TotalPointsHandicap"])
                        totals = market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
                        if totals:
                                odds['totals'] = totals
        

                results.append({
                        'teams': teams,
                        'odds': odds,
                        'commence_time': event['OutcomeDateTimeInt']
                })

        return results

def ladbrokes2_old(html, params, test=False):
        # SOME 2 and 3 Outcome Sports (e.g. AFL, EPL)
        # (does not use JSON)
        results = []
        soup = BeautifulSoup(html, 'lxml')

        for event in soup.select('div.sports-market.allow-bets'):

                teamBlock = event.find(class_='sports-market-listing').select('span.team-odd.win')
                if teamBlock is None:
                        continue

                odds = {}
                link = _construct_link(teamBlock) or params['scrape_config']['url']

                team1 = teamBlock[0]["data-teamname"]
                team2 = teamBlock[1]["data-teamname"]

                odds['h2h'] = [
                        teamBlock[0]["data-odds"],
                        teamBlock[1]["data-odds"]
                ]

                try:
                        drawBlock = event.find(class_='sports-market-listing').select('span.team-odd.draw')
                        odds['h2h'].append(drawBlock[0]["data-odds"])
                except:
                        pass

                lines = {}
                lineBlock = event.find(class_='sports-market-listing').select('span.team-odd.line')

                if lineBlock:
                        spreads = _get_lines(lineBlock)
                        if spreads:
                                odds['spreads'] = spreads

                results.append({
                        'teams': [team1, team2],
                        'odds': odds,
                        # 'link': link
                })
    
        return results

def _construct_link(teamBlock):
        try:
                # for desktop use: link = mb.find('a')['href']
                return 'https://m.ladbrokes.com.au/sports/'+ teamBlock[0]["data-sport"].replace(' ','-').lower() + '/' + teamBlock[0]["data-eventid"]
        except:
                return False
                
def _get_lines(mkt):

        lineOdds1 = mkt[0]["data-odds"]
        lineOdds2 = mkt[1]["data-odds"]
        linePoints1 = mkt[0]["data-handicap"]
        linePoints2 = mkt[1]["data-handicap"]
        return market_cleaner.line_clean([lineOdds1, linePoints1, lineOdds2, linePoints2])
