from components import validate, market_cleaner
import time
import json
import requests
import re

site = 'tab'

def tab23(html, params, test=False):

    # Possibly not needed
    # sport_info = params['sport_config']

    # Might need url in links
    # scrape_info = params['scrape_config']

    # site_info = params['site_config']

    results = []

    response = json.loads(html)
    if 'matches' not in response:
        return results

    for match in response['matches']:

        odds = {}

        # link = _make_link(scrape_info, match)

        """
        if 'tab.com' not in match.get('broadcastChannel', '').lower():
            evInfo['channel'] = match['broadcastChannel']
        """
        team1 = False
        team2 = False

        for mkt in match["markets"]:

            if mkt["bettingStatus"] != 'Open':
                continue

            if mkt["betOption"] in ["Head To Head", "Result", "Next Round Hd to Hd"]:

                if len(mkt["propositions"]) == 2:
                    team1 = mkt["propositions"][0]["name"]
                    team2 = mkt["propositions"][1]["name"]

                    odd1 = mkt["propositions"][0]["returnWin"]
                    odd2 = mkt["propositions"][1]["returnWin"]
                    oddDraw = 0

                else:
                    team1 = mkt["propositions"][0]["name"]
                    team2 = mkt["propositions"][2]["name"]

                    odd1 = mkt["propositions"][0]["returnWin"]
                    oddDraw = mkt["propositions"][1]["returnWin"]
                    odd2 = mkt["propositions"][2]["returnWin"]

                # Remove (Rd XX) from teams
                team1 = re.sub("[\(\[].*?[\)\]]", "", team1).strip()
                team2 = re.sub("[\(\[].*?[\)\]]", "", team2).strip()

                odds['h2h'] = [odd1, odd2]

                if oddDraw:
                    odds['h2h'].append(oddDraw)

                continue

            if mkt["betOption"] in ['Line', 'Live Lines'] and len(mkt["propositions"]) == 2:
                if not team1 or not team2:
                    team1, team2 = match['name'].split(' v ')

                spreads = _get_lines(mkt)
                if spreads:
                    odds['spreads'] = spreads

                continue

            if mkt["betOption"] in ['Total Points Over/Under', 'Total Score Over/Under']:
                totals = _get_ou(mkt)
                if totals:
                    odds['totals'] = totals

                continue
        
        if not team1 or not team2:
            continue

        results.append({
            'teams': [team1, team2],
            'odds': odds,
            'commence_time': int(validate.UTC_to_STR(match['startTime'], '%Y-%m-%d %H:%M:%S.%f')), # like "2017-05-18T16:35:00.000Z",
            'home_team': _get_home_team(match, team1, team2)
            # 'link': link
        })

    return results


def _get_home_team(match, team1, team2):
    if team1 == match['contestants'][0]['name']:

        if match['contestants'][0]['isHome']:
            return team1

        return team2

    if team2 == match['contestants'][0]['name']:
        if match['contestants'][0]['isHome']:
            return team2

        return team1


def _get_lines(mkt):
    lineOdds1 = mkt["propositions"][0]["returnWin"]
    linePoints1 = mkt["propositions"][0]["line"]

    lineOdds2 = mkt["propositions"][1]["returnWin"]
    linePoints2 = mkt["propositions"][1]["line"]

    return market_cleaner.line_clean([lineOdds1, linePoints1, lineOdds2, linePoints2])

def _get_ou(mkt):
    ouOdd1 = mkt["propositions"][0]["returnWin"]
    ouPoint1 = str(mkt["propositions"][0]["name"])
    ouOdd2 = mkt["propositions"][1]["returnWin"]
    ouPoint2 = str(mkt["propositions"][1]["name"])
    return market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])

def _make_link(scrape_info, match):
    try:
        link = match['_links']['self'].replace("https://webapi.tab.com.au/v1/tab-info-service/", "").replace("https://api.beta.tab.com.au/v1/tab-info-service/", "")
        if link.find('sports/betting/') == -1:
            link = link.replace('sports/', 'sports/betting/')
        link = link[ : link.find("?") ]
    except:
        link = scrape_info['url'].replace('v1/tab-info-service/', '')
        link = link[ : link.find("?") ]

    return link


def tab0(html, params, test = False):
    #    -    -    -    -    -    -    -    -
    #            Outrights
    #    -    -    -    -    -    -    -    -

    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    sportName, eventName = sport.split('___')
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    outrightEventName = params['scrapeInfo']['outrightEventName']
    oddThreshold = params['sportInfo']['oddThreshold']
    url = params['url']

    link = ''
    j_events = json.loads(html)
    results = {}
    hashData = {}

    try:
        try: oddThreshold = float(oddThreshold)
        except: oddThreshold = False

        outrightEventName_outrightMarketName = outrightEventName.split('_')
        outrightEventName = outrightEventName_outrightMarketName[0]

        if len(outrightEventName_outrightMarketName) == 2:
            outrightMarketName = outrightEventName_outrightMarketName[1]
        else:
            outrightMarketName = False

        for m in j_events["matches"]:

            if match["name"] != outrightEventName:
                continue

            link = url.replace("https://webapi.tab.com.au/v1/tab-info-service/", "")
            link = link.replace("v1/tab-info-service/", "")
            link = link.replace("sports/", "sports/betting/")
            link = link[ : link.find("/markets?")]

            for mkt in match["markets"]:

                try:
                    if outrightMarketName != False and mkt['shortName'] != outrightMarketName:
                        continue

                    if mkt['bettingStatus'] == 'Suspended': # or mkt["betOption"] not in ["Winner", "Medal Winner"]:
                        continue

                    for m3 in mkt["propositions"]:

                        team = re.sub(r'\([^)]*\)', '', m3["name"])
                        try: team = participantMap[team]
                        except: team = validate.resolveParticipant(outcomeType, team, listName, sport, site)
                        if not team: continue

                        odd = m3["returnWin"]
                        odd = validate.cleanOdd(odd)
                        if not odd: continue

                        if team not in hashData and validate.isValidOutrightOdd(odd, oddThreshold):
                            hashData[team] = odd

                except Exception as e:
                    if test: print sport, site, scrapeMethod, e
                    pass

    except Exception as e:
        if test: print sport, site, scrapeMethod, e
        pass

    results[eventName] = {
        'odds': hashData,
        'link': link
    }

    return results
