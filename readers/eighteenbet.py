#!/usr/bin/env python
# -*- coding: utf-8 -*-

from components import validate
from bs4 import BeautifulSoup
import requests

site = 'eighteenbet'
host = 'https://18bet.com'

sessions = {}

def createSession(leagueId, headers):
    global host
    s = requests.Session()
    s.get(host, headers=headers)
    # post league id (this may require sport url)
    #s.post(host, headers=headers)
    return s

def request(url, headers):
    #  for soccer EPL, url like https://www.18bet.com/index.php?dir=site&page=vic_odds&op=lines&ShowMode=all___2671
    url, leagueId = url.split('___')

    if leagueId not in sessions:
        sessions[leageId] = createSession(leageId, headers)
        
    sessions[leageId].get(url, headers=headers)


def eighteenbet23(html, params, isTest=False):

    global site, host
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    soup = BeautifulSoup(html)

    #soup.find(id='sbtEvents')
    for event in soup.find(class_='league_body').find_all(class_='line'):
        teamBlock = event.find(class_='game_text').find_all('p')
        team1 = teamBlock[0].text
        try: team1 = participantMap[team1]
        except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
        if not team1: continue

        team2 = teamBlock[1].text
        try: team2 = participantMap[team2]
        except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
        if not team2: continue

        odds, h2h, lines, ou = {}, [], {}, {}


        fullTimeOddsBlock = event.find(class_='fulltime')
        # h2h
        odd1, odd2, oddD = 0, 0, 0
        h2hBlock = fullTimeOddsBlock.find(class_='money_line').find_all('p')
        odd1 = validate.cleanOdd(h2hBlock[0].find(class_='suff').text)
        odd2 = validate.cleanOdd(h2hBlock[1].find(class_='suff').text)
        if len(h2hBlock) == 3:
            oddD = h2hBlock[2].find(class_='suff').text

        odds['H2H'] = [odd1, odd2, oddD]

        # lines
        try:
            linesBlock = fullTimeOddsBlock.find(class_='hdp').find_all('p')
            lineOdd1 = linesBlock[0].find(class_='suff').text
            linePoint1 = linesBlock[0].find(class_='preff').text

            lineOdd2 = linesBlock[1].find(class_='suff').text
            linePoint2 = linesBlock[1].find(class_='preff').text

            lines = validate.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])
            if lines: odds['lines'] = lines
        except:
            pass

        # ou
        try:
            ouBlock = fullTimeOddsBlock.find(class_='ou').find_all('p')
            ouOdd1 = linesBlock[0].find(class_='suff').text
            ouPoint1 = linesBlock[0].find(class_='preff').text

            ouOdd2 = linesBlock[1].find(class_='suff').text
            ouPoint2 = linesBlock[1].find(class_='preff').text

            try: ouPoint1 = 'over ' + ouPoint1 if float(ouPoint1) > 0 else 'under ' + ouPoint1
            except: ouPoint1 = ''

            try: ouPoint2 = 'over ' + ouPoint2 if float(ouPoint2) > 0 else 'under ' + ouPoint2
            except: ouPoint2 = ''

            ou = validate.ou_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
            if ou: odds['ou'] = ou

        except:
            pass

        eventName = validate.makeEventName(team1, team2)

        if eventName not in results:
            results[eventName] = {
                'teams': [team1, team2],
                'odds': odds,
                'link': link
            }

    return results
