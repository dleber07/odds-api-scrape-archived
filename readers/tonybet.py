#!/usr/bin/env python
# -*- coding: utf-8 -*-

from components import validate
import json
from collections import OrderedDict
import datetime
import time

site = 'tonybet'
host = 'https://tonybet.com'


def isoToUnix(d):
    #convert iso date string to unix timestamp
    dFormat = '%Y-%m-%dT%H:%M:%S.%fZ'
    d = datetime.datetime.strptime(d, dFormat)
    return int(time.mktime(d.timetuple()))

def _groupOdds(odds, isTest):
    outcomes = {}
    for oddId in odds:
        odd = odds[oddId]

        try:
            evId = odd['event_id']
            oddCode = odd['odd_code']

            if evId not in outcomes:
                outcomes[evId] = {}

            outcomes[evId][oddCode] = {
                'value': odd['odd_value'],
                'points': odd['additional_value_raw'], # e.g. number for lines points
                'title': odd['team_name']['en'], # e.g. Arsenal or Total OVER
            }

        except Exception as e:
            if isTest: print e
            pass

    return outcomes


def tonybet23(html, params, isTest=False):
    global site, host
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    j = json.loads(html, object_pairs_hook=OrderedDict)

    # Step 1: Group odds into events
    outcomes = _groupOdds(j['odds'], isTest)

    for event in j['events']:
        try:
            evId = event['id']

            if event['outright']:
                continue

            if evId not in outcomes:
                if isTest:
                    print 'evId has no outcomes'
                continue

            link = host + '/event/' + str(evId)
            startTime = isoToUnix(event['date_start'])

            team1 = event['teams']['home']
            try: team1 = participantMap[team1]
            except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
            if not team1: continue

            team2 = event['teams']['away']
            try: team2 = participantMap[team2]
            except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
            if not team2: continue

            odds, h2h, lines, ou = {}, [], {}, {}
            odd1, odd2, oddD = 0, 0, 0

            # H2H
            odd1 = validate.cleanOdd(outcomes[evId]['ODD_S1']['value'])
            odd2 = validate.cleanOdd(outcomes[evId]['ODD_S2']['value'])
            if 'ODD_SX' in outcomes[evId]:
                oddD = validate.cleanOdd(outcomes[evId]['ODD_SX']['value'])

            odds['H2H'] = [odd1, odd2, oddD]

            # Lines
            try:
                lines = validate.line_clean([
                    outcomes[evId]['ODD_HND_1_1']['value'],
                    outcomes[evId]['ODD_HND_1_1']['points'],
                    outcomes[evId]['ODD_HND_1_2']['value'],
                    outcomes[evId]['ODD_HND_1_2']['points'],
                ])
                if lines: odds['lines'] = lines

            except:
                if isTest:
                    print sport, site, 'unable to get lines'

            try:
                ou = validate.ou_clean([
                    outcomes[evId]['ODD_TTL_1_OVR']['value'],
                    'over' + str(outcomes[evId]['ODD_TTL_1_OVR']['points']),
                    outcomes[evId]['ODD_TTL_1_UND']['value'],
                    'under ' + str(outcomes[evId]['ODD_TTL_1_UND']['points'])
                ])
                if ou: odds['ou'] = ou

            except:
                if isTest:
                    print sport, site, 'unable to get ou'


            eventName = validate.makeEventName(team1, team2)

            if eventName not in results:
                results[eventName] = {
                    'teams': [team1, team2],
                    'odds': odds,
                    'link': link,
                    'evInfo': {
                        'startTime': startTime,
                    }
                }

        except Exception as e:
            if isTest: print sport, site, scrapeMethod, e
            pass

    return results
