import json
from components.validate import UTC_to_STR
from components import market_cleaner

def _get_runner_indexes(runners, team1_id, team2_id):
    """
    Determine if market outcomes are listing in a different order to teams
    """

    if runners[0]['event-participant-id'] == team1_id and runners[1]['event-participant-id'] == team2_id:
        return (0, 1)

    elif runners[0]['event-participant-id'] == team2_id and runners[1]['event-participant-id'] == team1_id:
        return (1, 0)
    else:
        
        return None

def _get_h2h(runners, team1_id, team2_id):
    runner_indexes = _get_runner_indexes(runners, team1_id, team2_id)
    if not runner_indexes:
        # Unknown order
        return {}

    team1_index, team2_index = runner_indexes

    if not runners[0]['prices'] or not runners[1]['prices']:
        return {}

    h2h = [
        runners[team1_index]['prices'][0]['odds'],
        runners[team2_index]['prices'][0]['odds']
    ]

    h2h_lay = [
        runners[team1_index]['prices'][1]['odds'],
        runners[team2_index]['prices'][1]['odds']
    ]

    if len(runners) == 3:
        h2h.append(runners[2]['prices'][0]['odds'])
        h2h_lay.append(runners[2]['prices'][1]['odds'])

    return {
        'h2h': h2h,
        'h2h_lay': h2h_lay
    }

def _get_spreads(runners, team1_id, team2_id):

    runner_indexes = _get_runner_indexes(runners, team1_id, team2_id)
    if not runner_indexes:
        # Unknown order
        return {}

    team1_index, team2_index = runner_indexes

    try:
        lineOdd1 = runners[team1_index]['prices'][0]['odds']
        linePoint1 = runners[team1_index]['handicap']
        lineOdd2 = runners[team2_index]['prices'][0]['odds']
        linePoint2 = runners[team2_index]['handicap']
        return {
            'spreads': market_cleaner.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])
        }
    except:
        return {}

def _get_totals(runners):
    try:
        ouOdd1 = runners[0]['prices'][0]['odds']
        ouPoint1 = runners[0]['name']
        ouOdd2 = runners[1]['prices'][0]['odds']
        ouPoint2 = runners[1]['name']
        return {
            'totals': market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
        }
    except:
        return {}

def matchbook23(html, params, test=False):

    mkt_names = {
        'h2h': ['Moneyline', 'Match Odds'],
        'spreads': ['Handicap'],
        'totals': ['Total']
    }

    j = json.loads(html)
    results = []

    for event in j.get('events'):

        if len(event['event-participants']) > 2:
            # Outrights
            continue

        odds = {}
        teams = [
            event['event-participants'][0]['participant-name'],
            event['event-participants'][1]['participant-name']
        ]

        team1_id = event['event-participants'][0]['id']
        team2_id = event['event-participants'][1]['id']

        for mkt in event.get('markets'):

            if mkt['name'] in mkt_names['h2h'] and 'h2h' not in odds:
                try:
                    odds.update(_get_h2h(mkt['runners'], team1_id, team2_id))
                except:
                    pass

            if mkt['name'] in mkt_names['spreads'] and 'spreads' not in odds:
                try:
                    odds.update(_get_spreads(mkt['runners'], team1_id, team2_id))
                except:
                    pass

            if mkt['name'] in mkt_names['totals'] and 'totals' not in odds:
                try:
                    odds.update(_get_totals(mkt['runners']))
                except:
                    pass

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': int(UTC_to_STR(event['start'], "%Y-%m-%d %H:%M:%S.%f")),
            'other': {
                # TODO TODO TODO TODO TODO
                'matched': int(event.get('volume', 0))
            }
        })

    return results
