import json
from components.scrape_tools import where_dict, datetime_to_unix

def topbetta23(html, params, is_test = False):
    result = []
    for response_set in json.loads(html):
        result += get_events(response_set, params, is_test)
    return result

def get_events(html, params, is_test = False):
    results = []

    sport_group, base_competition_name = params['scrape_config'].get('custom1').split('___')
    group_dict = where_dict(html['data'], 'name', sport_group)[0]

    if base_competition_name == 'ALL':
        events = []
        for base_competition in group_dict['base_competitions']:
            events += base_competition['competitions'][0].get('events', [])
    else:
        base_competition = where_dict(group_dict['base_competitions'], 'name', base_competition_name)[0]
        events = base_competition['competitions'][0].get('events', [])

    for event in events:
        
        if 'teams' in event:
            teams = [
                event['teams'][0]['name'],
                event['teams'][1]['name'],
            ]
            home_team = teams[0] if event['teams'][0]['position'] == 'home' else teams[1]
        else:
            if event['name'].find(' v '):
                teams = event['name'].split(' v ')
                home_team = ''
            else:
                raise UserWarning('Unable to extract teams from: ' + event['name'])

        odds, mkt_teams = get_odds(event['markets'])

        if mkt_teams:
            # Preference teams collected at the market leven rather than top level (top level aren't necessarily in order)
            teams = mkt_teams

        if not odds:
            continue
            

        results.append({
            'teams': teams,
            'odds': odds,
            'home_team': home_team,
            'commence_time': datetime_to_unix(event['start_date'], '%Y-%m-%d %H:%M:%S', 'Australia/Sydney')
        })

    return results

def topbetta23b(html, params, is_test=False):
    results = []

    for event in json.loads(html)['data'][0]['events']:
        
        if 'teams' in event:
            teams = [
                event['teams'][0]['name'],
                event['teams'][1]['name'],
            ]
            home_team = teams[0] if event['teams'][0]['position'] == 'home' else teams[1]
        else:
            if event['name'].find(' v '):
                teams = event['name'].split(' v ')
                home_team = ''
            else:
                raise UserWarning('Unable to extract teams from: ' + event['name'])

        try:
            odds, mkt_teams = get_odds(event['markets'])
        except:
            continue

        if mkt_teams:
            # Preference teams collected at the market leven rather than top level (top level aren't necessarily in order)
            teams = mkt_teams

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'home_team': home_team,
            'commence_time': datetime_to_unix(event['start_date'], '%Y-%m-%d %H:%M:%S', 'Australia/Sydney')
        })

    return results

def get_odds(markets):
    odds = {}
    teams = []

    for market in markets:
        if market['name'] in ['Head to Head (Inc OT)', 'Head to Head (Inc ET)', 'Head to Head', 'Match Winner']:
            if len(market['selections']) == 3:
                odds['h2h'] = [
                    market['selections'][0]['price'],
                    market['selections'][2]['price'],
                    market['selections'][1]['price'],
                ]

                teams = [
                    market['selections'][0]['name'],
                    market['selections'][2]['name'],
                ]

            else:
                odds['h2h'] = [
                    market['selections'][0]['price'],
                    market['selections'][1]['price'],
                ]

                teams = [
                    market['selections'][0]['name'],
                    market['selections'][1]['name'],
                ]




    return odds, teams

