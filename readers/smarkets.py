import json
import time

def smarkets23(html, params, is_test=False):
    j = json.loads(html)

    if time.time() - j.get('timestamp', 0) > 600:
        # Ensure s3 data is recent
        return []

    return j.get('data', [])
