from bs4 import BeautifulSoup
import lxml
from components.scrape_tools import datetime_to_unix
from components import market_cleaner

def playup23(html, params, is_test=False):
    soup = BeautifulSoup(html, 'lxml')
    events = [make_event(event) for event in soup.find_all('div', class_='MarketGroup')]
    return [event for event in events if event]

def make_event(event):
    odds = {}
    start_time = event.find('span', class_='matchTime').text

    market_table = event.find('table', class_='MarketTable')
    team1_fields = market_table.select('tbody > tr')[0].select('td')
    team2_fields = market_table.select('tbody > tr')[1].select('td')

    teams = [
        team1_fields[0].text.strip(),
        team2_fields[0].text.strip(),
    ]

    try:
        odds_h2h = get_h2h(event)
        if odds_h2h:
            odds['h2h'] = odds_h2h
    except:
        pass

    try:
        odds_spreads = get_spreads(event)
        if odds_spreads:
            odds['spreads'] = odds_spreads
    except:
        pass

    try:
        odds_totals = get_totals(event)
        if odds_totals:
            odds['totals'] = odds_totals
    except:
        pass

    if not odds:
        return False

    return {
        'home_team': teams[0], # no idea if this is correct?
        'commence_time': format_date(start_time),
        'teams': teams,
        'odds': odds,
    }


def format_date(date_raw):
    """
    Args:
        date_raw (str)
    Returns:
        int
    """
    # remove the day of week from date
    # this will turn:
    #  Wednesday 04 December 2019 @ 11:10
    # into 04 December 2019 @ 11:10
    date_str = ' '.join(date_raw.split(' ')[ 1: ])
    return datetime_to_unix(date_str, '%d %B %Y @ %H:%M', 'Australia/Sydney')



def get_h2h(event):
    odd_draw = event.find('td', {'data-bettype': 'Draw'})
    h2h = [
        event.find('td', {'data-bettype': 'AWin'})['data-price'],
        event.find('td', {'data-bettype': 'BWin'})['data-price'],
    ]

    if odd_draw is not None:
        h2h.append(odd_draw['data-price'])

    return h2h

def get_spreads(event):
    odd1 = event.find('td', {'data-bettype': 'ALine'})['data-price']
    odd2 = event.find('td', {'data-bettype': 'BLine'})['data-price']
    point1 = event.find('td', {'data-bettype': 'ALine'})['data-mod1']
    point2 = event.find('td', {'data-bettype': 'BLine'})['data-mod1']

    return market_cleaner.line_clean([odd1, point1, odd2, point2])

def get_totals(event):
    odd1 = event.find('td', {'data-bettype': 'TotalOver'})['data-price']
    odd2 = event.find('td', {'data-bettype': 'TotalUnder'})['data-price']
    point1 = 'over ' + event.find('td', {'data-bettype': 'TotalOver'})['data-mod1']
    # note this is set to above, since their TotalUnder value is invalid
    point2 = 'under ' + event.find('td', {'data-bettype': 'TotalOver'})['data-mod1']

    return market_cleaner.totals_clean([odd1, point1, odd2, point2])
    