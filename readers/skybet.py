

from components import validate
from bs4 import BeautifulSoup
import lxml
import re
import json

# TODO
# Need to scrape live games, from separate html block
# Need to scrape lines when h2h not available (e.g. see rugby). Use mkt_type to detect lines.


def fracToDec(v_top, v_bottom):
    return round((float(v_bottom) + float(v_top))/float(v_bottom), 2)


def skybet23(html, params, test=False):

    html = json.loads(html)['body']
    results = []
    compitition_name = params['scrape_config']['custom1']
    soup = BeautifulSoup(html, "lxml")

    for competition in soup.select('#competitions > ul > li'):
        competition_title_elem = competition.find(class_='split__title')

        if not competition_title_elem:
            continue
        
        if competition_title_elem.string != compitition_name:
            continue

        for event in competition.select('table.market-table tr'):

            tds = event.find_all('td')

            if len(tds) < 3 or len(tds) > 4:
                continue

            event_name = tds[0].find(class_='competitions-team-name')
            if not event_name:
                continue

            odds = {}
            teams = event_name.string.split(' v ')

            if len(tds) == 4:
                # has draw
                odds['h2h'] = [
                    fracToDec(tds[1]['data-oc-num'], tds[1]['data-oc-den']),
                    fracToDec(tds[3]['data-oc-num'], tds[3]['data-oc-den']),
                    fracToDec(tds[2]['data-oc-num'], tds[2]['data-oc-den'])
                ]

            else:
                # assume 2 outcome
                odds['h2h'] = [
                    fracToDec(tds[1]['data-oc-num'], tds[1]['data-oc-den']),
                    fracToDec(tds[2]['data-oc-num'], tds[2]['data-oc-den'])
                ]

            results.append({
                'teams': teams,
                'odds': odds
            })


    return results
