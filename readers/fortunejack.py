import json
from components.scrape_tools import where_dict
from components.market_cleaner import totals_clean, line_clean


def fortunejack23(html, params, is_test=False):
    results = []
    for event in json.loads(html)[0]['events']:
        odds = {}

        for market in event['markets']:
            if market.get('name', '') in ['Fulltime Result', 'Money Line', 'To Win']:
                odds['h2h'] = _get_h2h(market)

            elif market.get('defaultMarket') and market['defaultMarket']['outcomes'][0]['marketName'].strip() in ['Goals', 'Points', 'Total']:
                odds['totals'] = _get_totals(market)
            
            elif market.get('defaultMarket') and market['defaultMarket']['outcomes'][0]['marketName'].strip() in ['Spread', 'Handicap']:
                odds['spreads'] = _get_spreads(market)

            # TODO spreads

        results.append({
            'teams': [
                event['homeTeamName'],
                event['awayTeamName'],
            ],
            'odds': odds,
            'commence_time': _get_unix_time(event['startDate']),
            'home_team': event['homeTeamName'],
        })

    return results


def _get_unix_time(weird_timestamp):
    # For whatever reason, their dates are formatted strangely
    # but always resolve to const seconds diff from unix
    return weird_timestamp / 1e7 - 62135596800

def _get_h2h(market):
    if len(market['outcomes']) == 3:
        return [
            market['outcomes'][0]['odd'],
            market['outcomes'][2]['odd'],
            market['outcomes'][1]['odd'],
        ]

    return [
        market['outcomes'][0]['odd'],
        market['outcomes'][1]['odd'],
    ]

def _get_totals(market):
    under = where_dict(market['defaultMarket']['outcomes'], 'name', 'Under')[0]
    over = where_dict(market['defaultMarket']['outcomes'], 'name', 'Over')[0]
    return totals_clean([under['odd'], 'under ' + under['argumentString'], over['odd'], 'over ' + over['argumentString']])

def _get_spreads(market):
    first = where_dict(market['defaultMarket']['outcomes'], 'name', '1')[0]
    second = where_dict(market['defaultMarket']['outcomes'], 'name', '2')[0]
    return line_clean([first['odd'], first['argumentString'], second['odd'],  second['argumentString']])
