import json
from components.validate import UTC_to_STR, american_to_decimal
from components import market_cleaner

def gtbets23(html, params, is_test):
    results = []
    j = json.loads(html)

    event_indexes = {}

    for event in j.get('events_games'):


        teams = [
            event['htnm'], # assume home team name
            event['vtnm'], # assume vistor team name
        ]

        home_team = teams[0]
        odds = {}
        mkt_name = event.get('lt')

        if mkt_name in ['moneyline']:
            odds['h2h'] = [
                american_to_decimal(event['hml']), # home moneyline
                american_to_decimal(event['vml']), # visitor moneyline
            ]

            if event.get('dml'):
                odds['h2h'].append(american_to_decimal(event['dml'])) # draw moneyline

        elif mkt_name in ['spread']:
            odd1 = american_to_decimal(event['hml'])
            point1 = event['hps'] # home point spread
            odd2 = american_to_decimal(event['vml'])
            point2 = event['vps']
            odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])


        elif mkt_name in ['total']:
            odd1 = american_to_decimal(event['oml']) # over money line
            point1 = 'over ' + str(event['opts'])
            odd2 = american_to_decimal(event['uml']) # under money line
            point2 = 'under ' + str(event['opts'])
            odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

        if not odds:
            continue

        event_dict = {
            'teams': teams,
            'home_team': home_team,
            'odds': odds,
            'commence_time': int(UTC_to_STR(event['dt'], '%Y-%m-%d %H:%M:%S'))
        }

        event_name = '_'.join(teams)
        if event_name in event_indexes and results[event_indexes[event_name]]['commence_time'] == event_dict['commence_time']:
            results[event_indexes[event_name]]['odds'].update(event_dict['odds'])
        else:
            results.append(event_dict)
            event_indexes[event_name] = len(results) - 1

    return results
