from components import validate
from bs4 import BeautifulSoup


# NOTE:

# Untested. Could not get successful request.
# Careful with headers.
# Uses blocking library.



site = 'tempobet'

def tempobet23(html, params, isTest=False):
    # 2 Outcome Sports

    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}
    soup = BeautifulSoup(html)

    try:
        for event in soup.find("table", class_="table-a").find_all("tr"):
            odds, lines, ou = {}, {}, {}
            try:
                odds = {}

                tds = event.find_all("td")
                tdsOdds = event.find("table", class_="odd")

                try:
                    link = tds[1].find("a")['href']
                except:
                    link = url


                teams = tds[1].find("a").text#.decode_contents(formatter="html")

                teams = teams.split(' - ')
                team1, team2 = teams

                try: team1 = participantMap[team1]
                except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
                if not team1: continue

                try: team2 = participantMap[team2]
                except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
                if not team2: continue

                teams = [team1, team2]

                odd1, odd2 = 0, 0


                odd1 = tdsOdds[0]['data-oddval']
                if len(tdsOdds) == 3:
                    oddDraw = tdsOdds[1]['data-oddval']
                    odd2 = tdsOdds[2]['data-oddval']
                else:
                    odd2 = tdsOdds[1]['data-oddval']

                odd1 = validate.cleanOdd(odd1)
                odd2 = validate.cleanOdd(odd2)

                oddsH2H = [odd1, odd2]
                if oddDraw: oddsH2H.append(validate.cleanOdd(oddDraw))

                odds['H2H'] = oddsH2H

                eventName = validate.makeEventName(team1, team2)

                if eventName not in results:
                    results[eventName] = {
                        'teams': teams,
                        'odds': odds,
                        'link': link
                    }

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass
    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results