from components import validate
import time, json

"""


TESTING:
https://www.jetwin.com/api/v1/sports/american-football/usa/nfl
AND
https://www.jetwin.com/api/v1/sports/soccer/england/english-premier-league
"""



site = 'jetwin'
host = 'https://www.jetwin.com/en-us/sports/'

"""
    Returns a map of competitorId => teamName
"""
def _get_team_map(teams):
    team_map = {}
    for team in teams:
        team_map[team['competitor']] = team['translation']
    return team_map


"""
    Returns a map of matchId => Competitor Ids in the order of Home, 
"""
def _get_match_competitor_map(blocks):
    matchCompetitorMap = {}
    for block in blocks:
        haKey = 'Home' if block['homeTeam'] else 'Away'
        matchCompetitorMap[block['match']] = {
            haKey: block['id']
        }
    return matchCompetitorMap




def _get_match_odds(oddBlocks):

    oddsMap = {}
    
    for oddBlock in oddBlocks:
        matchId = oddBlock['match']
        mktId = oddBlock['oddType']

        if matchId not in oddsMap:
            oddsMap[matchId] = {}
        
        if mktId not in oddsMap[matchId]
            oddsMap[matchId][mktId] = []
        
        oddsMap[matchId][mktId].append({
            'outcome': oddBlock['outcome'], # e.g. home, away, draw, first to 2 goals..
            'odd': oddBlock['odd']
        })
        
    return oddsMap


def _getH2H(mkt):
    #mkt['selections'][0]['odds']['min_stake']
    #mkt['selections'][0]['odds']['max_stake']
    odd1 = validate.cleanOdd(mkt['selections'][0]['odds'])
    odd2 = validate.cleanOdd(mkt['selections'][1]['odds'])
    oddD = 0

    if len(mkt['selections']) == 3:
        oddD = validate.cleanOdd(mkt['selections'][2]['odds'])
 
    return [odd1, odd2, oddD]




def jetwin23(html, params, isTest = False):

    global site, host
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    j = json.loads(html)['data']

    competitorNameMap = _get_team_map(j['matchCompetitorTranslations'])
    matchCompetitorMap = _get_match_competitor_map(event['matchCompetitors'])
    matchOddsMap = _get_match_odds(j['matchBets'])


    try:

        for event in j['matches']:
            
            
            try:
                h2h, lines, ou = [], {}, {}
                evInfo = {}

                if not event['enabled']: continue
    
                evInfo['startTime'] = validate.UTC_to_STR(event['startTime'], '%Y-%m-%d %H:%M:%S.%f')

                # https://www.jetwin.com/en-us/sports/soccer/england/english-premier-league/manchester-city-fc-vs-liverpool-fc-20170909-2129192
                try: link = host + j['sport']['slug'] + '/' + j['category']['slug'] + '/'  + j['tournament']['slug'] + '/' + event['slug']
                except: link = host

                team1, team2 = _resolve_teams(event['id'])

                try: team1 = participantMap[team1]
                except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
                if not team1: continue

                try: team2 = participantMap[team2]
                except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
                if not team2: continue

                teams = [team1, team2]


                for mkt in event['markets']:
                    
                    if mkt['id'] == 1: #h2h
                        h2h = _getH2H(mkt)
                    
                    elif mkt['id'] == 192: # lines
                        lines = _getLines(mkt)
                    
                    elif mkt['id'] == 193: # ou
                        ou = _getOU(mkt)


                eventName = validate.makeEventName(team1, team2)
                results[eventName] = {
                    'teams': teams,
                    'odds': {'H2H': h2h, 'lines': lines, 'ou': ou},
                    'link': link,
                    'evInfo': evInfo,
                }

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results
