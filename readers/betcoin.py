from components.validate import UTC_to_STR
from components import market_cleaner
import time, json

def _get_teams(teams_block):
    """
    Teams may need reordering
    """
    first_team_pos = teams_block[0]['e']
    if first_team_pos == '2':
        return [
            teams_block[1]['b'],
            teams_block[0]['b']
        ]
    else:
        return [
            teams_block[0]['b'],
            teams_block[1]['b']
        ]

def _get_h2h(mkt):
    # 'h' is the position of the outcome. 1 == odd1, 2 == oddD, 3 == odd2.
    #   They can apepar in any order

    if len(mkt['i']) == 3:
        # Draw outcomes

        raw_odds = {
            int(mkt['i'][0]['h']): mkt['i'][0]['g'],
            int(mkt['i'][1]['h']): mkt['i'][1]['g'],
            int(mkt['i'][2]['h']): mkt['i'][2]['g'],
        }

        return [
            raw_odds[1],
            raw_odds[3],
            raw_odds[2] # draw
        ]

    else:
        first_outcome_pos = mkt['i'][0]['h']

        if first_outcome_pos == '2':
            return [
                mkt['i'][1]['g'],
                mkt['i'][0]['g']
            ]
        else:
            return [
                mkt['i'][0]['g'],
                mkt['i'][1]['g']
            ]

def _get_spreads(mkt):
    point1 = float(mkt['e'])
    point2 = point1 * -1

    if mkt['i'][0]['h'] == '2':
        odd1 = mkt['i'][1]['g']
        odd2 = mkt['i'][0]['g']
    else:
        odd1 = mkt['i'][0]['g']
        odd2 = mkt['i'][1]['g']
    
    return market_cleaner.line_clean([odd1, point1, odd2, point2])


def betcoin23(html, params, isTest=False):

    jEvents = json.loads(html)
    results = []

    for event in jEvents.get('m', []):
        odds = {}
        teams = _get_teams(event['k'])

        for mkt in event.get('l', []):

            mktname = mkt.get('n', '')

            if mktname in ['Three  Way', 'Two Way (including overtime)']:
                odds['h2h'] = _get_h2h(mkt)

            elif mktname in ['Total', 'Total (including overtime)']:
                odds['totals'] = market_cleaner.totals_clean([
                    mkt['i'][0]['g'],
                    mkt['i'][0]['j'],
                    mkt['i'][1]['g'],
                    mkt['i'][1]['j']
                ])
            
            elif mktname in ['Points Spreads (including overtime)']:
                odds['spreads'] = _get_spreads(mkt)


        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': event['r'] / 1000,
            #'home_team': home_team
        })

    return results
