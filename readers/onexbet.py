#!/usr/bin/env python
# -*- coding: utf-8 -*-

from components import market_cleaner
import json

mktcode_map = {
    'h2h': 1,
    'spreads': 2, # at 0??? missing P
    'totals': 17,
    'totals_individual_team1': 15,
    'totals_individual_team2': 62,
    'double_chance': 8,
    'both_teams_to_score': 19
}

"""
    Args
        mkts: list of dicitionaries, each for an individual outcome
    
    Returns:
        dictionary of mkt_code (key) and 2D list, each containing a list of outcome position, odd and point. Top list is sorted by outcome position.
"""
def _make_mkts_normal(mkts):

    results = {}

    for mktitem in mkts:

        mkt_code = mktitem['G'] 

        if mkt_code not in results:
            results[mkt_code] = []

        odd = mktitem.get('C')
        pos = mktitem.get('T') # draw == 2 if 3 outcomes
        points = mktitem.get('P', 0)

        results[mkt_code].append([pos, odd, points])

    for mkt_code in results.keys():
        results[mkt_code] = sorted(results[mkt_code], key=lambda l:l[0], reverse=False)

    return results


def onexbet23(html, params, isTest=False):

    results = []
    j = json.loads(html)

    for event in j['Value']:

        team1 = event.get('O1')
        team2 = event.get('O2')

        if not team1 or not team2:
            continue

        odds = {}
        mkts = _make_mkts_normal(event.get('E', []))

        if mktcode_map['h2h'] in mkts:
            h2hs = mkts[mktcode_map['h2h']]
            if len(h2hs) == 3:
                odds['h2h'] = [h2hs[0][1], h2hs[2][1], h2hs[1][1]]
            else:
                odds['h2h'] = [h2hs[0][1], h2hs[1][1]]
            
        if mktcode_map['spreads'] in mkts:
            spreads = mkts[mktcode_map['spreads']]
            odd1 = spreads[0][1]
            point1 = spreads[0][2]
            odd2 = spreads[1][1]
            point2 = spreads[1][2]
            odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])

        if mktcode_map['totals'] in mkts:
            totals = mkts[mktcode_map['totals']]
            odd1 = totals[0][1]
            point1 = 'over ' + str(totals[0][2])
            odd2 = totals[1][1]
            point2 = 'under ' + str(totals[1][2])
            odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

        results.append({
            'teams': [team1, team2],
            'odds': odds,
            'commence_time': event.get('S'),
        })

    return results
