import json
from components import validate
from components import market_cleaner


def _get_h2h(runners):
    team1 = runners[0]['runnerName']
    odd1 = runners[0]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']

    if len(runners) == 3:
        team2 = runners[2]['runnerName']
        odd2 = runners[2]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']
        oddD = runners[1]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']

    else:
        team2 = runners[1]['runnerName']
        odd2 = runners[1]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']
        oddD = 0


    home_team = team1 if runners[0]['result'].get('type') == 'HOME' else team2
        
    odds_h2h = [odd1, odd2]
    if oddD:
        odds_h2h.append(oddD)

    return {
        'teams': [team1, team2],
        'h2h': odds_h2h,
        'home_team': home_team
    }

def _get_spreads(runners):

    team1 = runners[0]['runnerName']
    odd1 = runners[0]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']
    point1 = runners[0]['handicap']

    if len(runners) == 3:
        team2 = runners[2]['runnerName']
        odd2 = runners[2]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']
        point2 = runners[2]['handicap']

        oddD = runners[1]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']
        pointD = runners[1]['handicap']

    else:
        team2 = runners[1]['runnerName']
        odd2 = runners[1]['winRunnerOdds']['trueOdds']['decimalOdds']['decimalOdds']
        point2 = runners[1]['handicap']

        oddD = 0
        pointD = 0

    home_team = team1 if runners[0]['result']['type'] == 'HOME' else team2

    if oddD:
        spreads = market_cleaner.line_clean3([odd1, point1, odd2, point2, oddD, pointD])
    else:
        spreads = market_cleaner.line_clean([odd1, point1, odd2, point2])
    
    return {
        'teams': [team1, team2],
        'spreads': spreads,
        'home_team': home_team
    }




def paddypower23(html, params, isTest=False):

    results = []
    j = json.loads(html)
    h2h_mkt_types = ['MATCH_ODDS', 'MONEY_LINE', 'MATCH_BETTING'] # what's the type for Win-Draw-Win?
    spread_mkt_types = ['MATCH_HANDICAP', 'MATCH_HANDICAP_(2-WAY)', 'MATCH_HANDICAP_(3-WAY)']
    valid_market_types = h2h_mkt_types + spread_mkt_types

    events = {}

    for mkt_id, mkt in j['attachments']['markets'].iteritems():
        mkt_type = mkt['marketType']
        event_id = mkt['eventId']
        event = {}

        if mkt_type not in valid_market_types or mkt['marketStatus'] != 'OPEN':
            continue

        if event_id not in events:
            events[event_id] = {
                'odds': {}
            }
    
        if mkt_type in h2h_mkt_types:
            event = _get_h2h(mkt['runners'])
            events[event_id]['odds']['h2h'] = event['h2h']

        elif mkt_type in spread_mkt_types:
            event = _get_spreads(mkt['runners'])
            events[event_id]['odds']['spreads'] = event['spreads']


        # TODO more markets here


        if event:
            events[event_id]['teams'] = event['teams']
            events[event_id]['home_team'] = event['home_team']


    for event_id, event in events.iteritems():
        if 'teams' not in event:
            continue

        event['commence_time'] = int(validate.UTC_to_STR(j['attachments']['events'][
            str(event_id)]['openDate'],
            '%Y-%m-%d %H:%M:%S.%f'
        ))
        results.append(event)

    return results
