from bs4 import BeautifulSoup
import lxml
from components.validate import american_to_decimal
from components.scrape_tools import datetime_to_unix
from components import market_cleaner

def _get_teams(buttons):
    return [
        buttons[0]['data-team'],
        buttons[1]['data-team']
    ]

def _get_h2h(buttons):
    return [american_to_decimal(float(button['data-odds'])) for button in buttons]

def _get_spreads(team_rows):

    outcome1 = team_rows[0]
    outcome2 = team_rows[1]

    if not outcome1 or not outcome1['data-odds']:
        return None

    odd1 = american_to_decimal(float(outcome1['data-odds']))
    point1 = outcome1['data-spread']

    odd2 = american_to_decimal(float(outcome2['data-odds']))
    point2 = outcome2['data-spread']

    return market_cleaner.line_clean([odd1, point1, odd2, point2])


def _get_totals(team_rows):

    outcome1 = team_rows[0]
    outcome2 = team_rows[1]

    if not outcome1 or not outcome1['data-odds']:
        return None

    odd1 = american_to_decimal(float(outcome1['data-odds']))
    point1 = outcome1['data-spread']

    odd2 = american_to_decimal(float(outcome2['data-odds']))
    point2 = outcome2['data-spread']
    
    if float(point1) > 0:
        point1 = 'under ' + point1
        point2 = 'over ' + point1
    else:
        point1 = 'over ' + point2
        point2 = 'under ' + point2

    return market_cleaner.totals_clean([odd1, point1, odd2, point2])


def mybookieag23(html, params, is_test):
    results = []
    soup = BeautifulSoup(html, 'lxml')

    for event in soup.select('div.sportsbook-lines'):        
        odds = {}
        teams = []

        # time is US EST
        # formatted like 2019-08-13 19:05-04:00
        try:
            start_date_string = event.find(class_='event-wrapper').find('meta', attrs={"itemprop" : "startDate"})['content']
            naive_time_str, offset_str = start_date_string[:-6], start_date_string[-6:]
            commence_time = datetime_to_unix(naive_time_str, '%Y-%m-%d %H:%M', 'US/Eastern')
        except:
            try:
                datetime_elem = event.find('p', class_='sportsbook__line-date')
                if not datetime_elem:
                    continue
                commence_time = datetime_to_unix(event.find('p', class_='sportsbook__line-date')['data-time'],'%Y-%m-%d %H:%M:%S', 'US/Eastern')
            except:
                continue

        if event.find(class_='money-lines'):
            try:
                buttons = event.find(class_='money-lines').find_all('button')
                teams = _get_teams(buttons)
                odds['h2h'] = _get_h2h(buttons)
            except:
                pass

        if event.find(class_='spread-lines'):
            if not teams:
                teams = _get_teams(buttons)

            spreads = _get_spreads(event.find(class_='spread-lines').find_all('button'))
            if spreads:
                odds['spreads'] = spreads

        if event.find(class_='total-lines'):
            if not teams:
                teams = _get_teams(buttons)

            totals = _get_totals(event.find(class_='total-lines').find_all('button'))
            if totals:
                odds['totals'] = totals
        

        if not teams:
            continue

        if '1H ' in ' '.join(teams):
            # 1st half market
            continue

        results.append({
            'teams': teams,
            'commence_time': commence_time,
            'odds': odds
        })
    
    return results