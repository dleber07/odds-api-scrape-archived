import json
from components import market_cleaner
from components.validate import UTC_to_STR

def ubet23(html, params, test=False):

    results = []
    j_events = json.loads(html)

    # Market indexes
    subEventIndexH2H = 0
    subEventIndexLine = 1
    subEventIndexOU = 2

    for ev in j_events["mainEvents"]:

        odds = {}

        if not ev["subEvents"]:
            continue


        if ev["subEvents"][subEventIndexH2H]["betTypeName"].lower() not in ["head to head", "result"]:
            # Must have h2h to collect results
            # TODO - consider changing. Extract teams from other market
            continue


        link = 'https://ubet.com' + ev['MainEventUrl'] if 'MainEventUrl' in ev else 'https://ubet.com/sports/'

        mkt_h2h = ev["subEvents"][subEventIndexH2H]["offers"]
        try:
            if len(mkt_h2h) == 2:

                teams = [
                    mkt_h2h[0]["offerName"],
                    mkt_h2h[1]["offerName"]
                ]

                odds['h2h'] = [
                    mkt_h2h[0]["winReturn"],
                    mkt_h2h[1]["winReturn"]
                ]

            else:
                teams = [
                    mkt_h2h[0]["offerName"],
                    mkt_h2h[2]["offerName"]
                ]

                odds['h2h'] = [
                    mkt_h2h[0]["winReturn"],
                    mkt_h2h[2]["winReturn"],
                    mkt_h2h[1]["winReturn"]
                ]
        except:
            continue

        try:
            # Spreads
            if ev["subEvents"][subEventIndexLine]["betTypeName"].lower() == 'line':
                mkt_lines = ev["subEvents"][subEventIndexLine]["offers"]
                lineOdds1 = mkt_lines[0]["winReturn"]
                lineOdds2 = mkt_lines[1]["winReturn"]
                linePoints1 = mkt_lines[0]["offerName"].replace(teams[0], '').strip()
                linePoints2 = mkt_lines[1]["offerName"].replace(teams[1], '').strip()
                odds['spreads'] = market_cleaner.line_clean([lineOdds1, linePoints1, lineOdds2, linePoints2])
        except:
            pass

        try:
            # Totals
            if ev["subEvents"][subEventIndexOU]["betTypeName"].lower() == 'total score over/under':
                mkt_totals = ev["subEvents"][subEventIndexOU]["offers"]
                ouOdd1 = mkt_totals[0]["winReturn"]
                ouOdd2 = mkt_totals[1]["winReturn"]
                ouPoint1 = mkt_totals[0]["offerName"]
                ouPoint2 = mkt_totals[1]["offerName"]
                odds['totals'] = market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
        except:
            pass
        
        try:
            commence_time = int(UTC_to_STR(ev['eventStartTime'], "%Y-%m-%d %H:%M:%S.%f"))
        except:
            try:
                commence_time = int(UTC_to_STR(ev['eventStartTime'], "%Y-%m-%d %H:%M:%S"))
            except:
                continue


        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': commence_time,
            'home_team': teams[0] # TODO verify if correct
            # 'link': link
        })

    return results



def ubet0(html, params, isTest = False):
    # Outrights
    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    sportName, eventName = sport.split('___')
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    outrightEventName = params['scrapeInfo']['outrightEventName']
    oddThreshold = params['sportInfo']['oddThreshold']
    url = params['url']

    matchedCnt, mismatchCnt = 0, 0
    j_events = json.loads(html)
    results = {}
    hashData = {}

    try:
        try: oddThreshold = float(oddThreshold)
        except: oddThreshold = False

        EventName_BetTypeName = outrightEventName.split('_')
        EventName = EventName_BetTypeName[0]
        BetTypeName = EventName_BetTypeName[1]

        for ev in j_events["MainEvents"]:
            if ev["EventName"] == EventName:
                try: link = 'https://ubet.com/sports' + ev['MainEventUrl']
                except: link = 'https://ubet.com/sports/'
                try:
                    for m3 in ev["SubEvents"]:

                        if m3["BetTypeName"] != BetTypeName: continue

                        for m4 in m3["Offers"]:

                            if m4["Status"] not in ['o', 'i']: continue

                            team, odd = '', 0
                            team = m4["OfferName"]
                            odd = m4["WinReturn"]

                            try: team = participantMap[team]
                            except: team = validate.resolveParticipant(outcomeType, team, listName, sport, site)
                            if not team: continue

                            odd = validate.cleanOdd(odd)
                            if not odd: continue

                            if team not in hashData and validate.isValidOutrightOdd(odd, oddThreshold):
                                hashData[team] = odd
                                matchedCnt += 1
                            else: mismatchCnt += 1
                except Exception as e:
                    if isTest: print sport, site, scrapeMethod, e
                    pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass


    results[eventName] = {
        'odds': hashData,
        'link': link
    }

    return results
