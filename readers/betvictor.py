#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import lxml
from components import market_cleaner
from components.validate import UTC_to_STR
import json

def betvictor2(html, params, isTest=False):
    results = []


    h2hAttr = {'data-market_description': 'Money Line - Match'}
    lineAttr = {'data-market_description': 'Point Spread - Match'}
    ouAttr = {'data-market_description':  'Total Points - Match'}

    soup = BeautifulSoup(html, "lxml")

    for competition in soup.select('div.sports-coupon'):

        target_compitition = params['scrape_config'].get('custom1', False)
        competition_title_elem = competition.find('span', class_='coupon-title')
        if not competition_title_elem:
            continue

        competition_title = competition_title_elem.text

        if target_compitition and target_compitition not in competition_title:
            continue

        events = competition.select('tr.body.top')
        if not events:
            events = competition.select('tr.body')

        for teamBlock1 in events:
            teamBlock2 = teamBlock1.findNext('tr')

            odds = {}

            # NOTE: could also gather from line or ou odds field
            try:
                team1 = teamBlock1.find('span', h2hAttr)['data-outcome_description'] 
                team2 = teamBlock2.find('span', h2hAttr)['data-outcome_description']
            except:
                continue

            odd1 = teamBlock1.find('span', h2hAttr)['data-price']
            odd2 = teamBlock2.find('span', h2hAttr)['data-price']
            odds['h2h'] = [odd1, odd2]

            try:
                linePoint1 = teamBlock1.find(class_='outcome_handicap').text
                linePoint2 = teamBlock2.find(class_='outcome_handicap').text

                lineOdd1 = teamBlock1.find('span', lineAttr)['data-price']
                lineOdd2 = teamBlock2.find('span', lineAttr)['data-price']
                odds['spreads'] = market_cleaner.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])
            except:
                pass

            try:
                ouPoint1 = teamBlock1.find(class_='outcome_ou').text
                ouPoint2 = teamBlock2.find(class_='outcome_ou').text
                ouOdd1 = teamBlock1.find('span', ouAttr)['data-price']
                ouOdd2 = teamBlock2.find('span', ouAttr)['data-price']
                odds['totals'] = market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
            except:
                pass


            results.append({
                'teams': [team1, team2],
                'odds': odds
            })


    return results


def betvictor2b(html, params, isTest=False):

    results = []

    h2hMarkets = ['To Win - 2 way - Match', 'Match Betting - Match', 'Money Line - Match', 'To Win the Bout - Match']
    lineAttr = {'data-market_description': 'Handicap - Match'}
    ouAttr = {'data-market_description':  'Total Points U/O - Match'}

    soup = BeautifulSoup(html, 'lxml')

    for tr in soup.find(id='center_content').select('tr.body'):

        tds = tr.find_all('td', class_='outcome_td')
        if not tds:
            continue
    
        if len(tds) < 2:
            continue
        
        teamBlock1 = tds[0]
        teamBlock2 = tds[1]
        odds = {}

        # NOTE: could also gather from line or ou odds field
        for mktName in h2hMarkets:
            team1 = teamBlock1.find('span', {'data-market_description': mktName})
            if team1:
                team1 = teamBlock1.find('span', {'data-market_description': mktName})['data-outcome_description']
                team2 = teamBlock2.find('span', {'data-market_description': mktName})['data-outcome_description']
                break

        if not team1:
            continue # market not found!

        odds['h2h'] = [
            teamBlock1.find('span', {'data-market_description': mktName})['data-price'],
            teamBlock2.find('span', {'data-market_description': mktName})['data-price']
        ]

        try:
            linePoint1 = teamBlock1.find(class_='outcome_handicap').text
            linePoint2 = teamBlock2.find(class_='outcome_handicap').text

            lineOdd1 = teamBlock1.find('span', lineAttr)['data-price']
            lineOdd2 = teamBlock2.find('span', lineAttr)['data-price']
            odds['spreads'] = market_cleaner.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])
        except:
            pass

        try:
            ouPoint1 = teamBlock1.find(class_='outcome_ou').text
            ouPoint2 = teamBlock2.find(class_='outcome_ou').text
            ouOdd1 = teamBlock1.find('span', ouAttr)['data-price']
            ouOdd2 = teamBlock2.find('span', ouAttr)['data-price']
            odds['totals'] = market_cleaner.totals_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])
        except:
            pass


        results.append({
            'teams': [team1, team2],
            'odds': odds
        })


    return results


def betvictor3(html, params, isTest=False):

    results = []
    soup = BeautifulSoup(html, 'lxml')

    for event in soup.find(id='three_way_outright_coupon-markets').find_all('tr', class_ = 'body'):

        outcomes = event.find_all(class_='outcome_td')
        if not outcomes:
            continue

        hasDraw = True if len(outcomes) == 3 else False
        oddD = 0
        odds = {}

        team1 = outcomes[0].find('span')['data-outcome_description']
        odd1 = outcomes[0].find('span')['data-price']

        if hasDraw:
            team2 = outcomes[2].find('span')['data-outcome_description']
            odd2 = outcomes[2].find('span')['data-price']
            oddD = outcomes[1].find('span')['data-price']

        else:
            team2 = outcomes[1].find('span')['data-outcome_description']
            odd2 = outcomes[1].find('span')['data-price']

        teams = [team1, team2]
        odds['h2h'] = [odd1, odd2]

        if hasDraw:
            odds['h2h'].append(oddD)

        results.append({
            'teams': teams,
            'odds': odds
        })

    return results


def betvictor_mob23(html, params, isTest=False):

    h2h_market_names = ['Money Line', 'Match Betting', 'To Win - 2 way', 'To Win - 3 way', 'To Win the Bout', 'To Win', '2-Way Match Betting']
    spreads_market_names = ['Handicap', 'Point Spread']
    totals_market_names = ['Total Points U/O', 'Total Points', 'Total Goals'] # Total Goals (soccer) is technically a different market since O/U points is fixed. Consider removing

    results = []
    j = json.loads(html)

    for event in j.get('events'):

        odds = {}

        if '@' in event['description']:
            teams = event['description'].split(' @ ')
            teams.reverse()
        elif ' v ' in event['description']:
            teams = event['description'].split(' v ')
        else:
            raise UserWarning('Unsure how to split teams (neither @ nor v)')


        for market in event.get('markets'):        
            market_name = market['description'].strip()
            
            if market_name in h2h_market_names:

                if len(market['outcomes']) == 3:
                    odds['h2h'] = [
                        market['outcomes'][0]['decimal_price'],
                        market['outcomes'][2]['decimal_price'],
                        market['outcomes'][1]['decimal_price'],
                    ]

                else:
                    odds['h2h'] = [
                        market['outcomes'][0]['decimal_price'],
                        market['outcomes'][1]['decimal_price'],
                    ]

            elif market_name in spreads_market_names:
                if 'markets' in market:
                    spreads = market['markets'][0]['outcomes']
                else:
                    spreads = market['outcomes']

                odd1 = spreads[0]['decimal_price']
                point1 = spreads[0]['description'].replace(teams[0], '').strip()

                odd2 = spreads[1]['decimal_price']
                point2 = spreads[1]['description'].replace(teams[1], '').strip()
                odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])

            elif market_name in totals_market_names:
                if 'markets' in market:
                    totals = market['markets'][0]['outcomes']
                else:
                    totals = market['outcomes']

                odd1 = totals[0]['decimal_price']
                point1 = totals[0]['description']

                odd2 = totals[1]['decimal_price']
                point2 = totals[1]['description']
                odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

             
        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': event['epoch_date'],
            'home_team': teams[0]
        })


    return results
    