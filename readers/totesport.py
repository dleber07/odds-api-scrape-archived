#!/usr/bin/env python
# -*- coding: utf-8 -*-

from modules import Validator
from xml.dom import minidom

site =  'totesport'

# NOTE:
# Some tags contain mixed casing. Chrome inspector shows everything in lower.
# Use View Source!

def fracToDecimal(numerator, demoninator):
    return round(float(numerator)/float(demoninator) + 1, 3)

def getOdd(eventXml, selectionIndex):
    numer = eventXml.getElementsByTagName("b:marketSelection")[selectionIndex].getElementsByTagName("b:currentpriceup")[0].firstChild.nodeValue
    denom = eventXml.getElementsByTagName("b:marketSelection")[selectionIndex].getElementsByTagName("b:currentpricedown")[0].firstChild.nodeValue
    if not denom or not numer: return False
    odd = Validator.cleanOdd(fracToDecimal(numer, denom))
    return odd

def totesport23(html, params, isTest=False):

    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    try:
        xmldoc = minidom.parseString(html)

        for event in xmldoc.getElementsByTagName("b:market"):

            # marketType reliability untested. Also consider: b:markettypename, b:eachwayterms -> b:name
            marketType = event.getElementsByTagName('b:externaldescription')[0].firstChild.nodeValue
            if marketType.lower() not in ['money line', 'match result']: continue

            try:
                teams, odds = [], []
                lines, ou = {}, {}
                lineOdds, linePoints = [], []
                ouOdds, ouPoints = [], []

                link = 'https://totesport.mobi/sports/'
                try:
                    eventId = event.getElementsByTagName('b:idfoevent')[0].firstChild.nodeValue
                    link = link + 'event/' + eventId
                    # Note: could not find event-specific url on site
                except: pass

                hasDraw = False
                team1, team2 = '', ''
                odd1, odd2, oddDraw = 0, 0, 0

                team1 = event.getElementsByTagName("b:marketSelection")[0].getElementsByTagName("b:name")[0].firstChild.nodeValue.encode('utf8') # home team
                team2 = event.getElementsByTagName("b:marketSelection")[1].getElementsByTagName("b:name")[0].firstChild.nodeValue.encode('utf8') # away team

                if team2 == 'Draw':
                    hasDraw = True
                    team2 = event.getElementsByTagName("b:marketSelection")[2].getElementsByTagName("b:name")[0].firstChild.nodeValue.encode('utf8') # away team

                try: team1 = participantMap[team1]
                except: team1 = Validator.cleanPointer(outcomeType, team1, listName, sport, site)
                if not team1: continue

                try: team2 = participantMap[team2]
                except: team2 = Validator.cleanPointer(outcomeType, team2, listName, sport, site)
                if not team2: continue

                teams = [team1, team2]

                odd1 = getOdd(event, 0)
                if not odd1: continue

                if hasDraw:
                    odd2 = getOdd(event, 2)
                    if not odd2: continue
                    oddDraw = getOdd(event, 1)
                    oddsH2H = [odd1, odd2, oddDraw]

                else:
                    odd2 = getOdd(event, 1)
                    oddsH2H = [odd1, odd2]

                odds = {
                    'H2H': oddsH2H
                }

                eventName = Validator.makeEventName(team1, team2)
                if eventName not in results:
                    results[eventName] = {
                        'teams': [team1, team2],
                        'odds': odds,
                        'link': link
                    }

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results