from components import validate
from bs4 import BeautifulSoup
import time
import re

site = 'madbookie'
host = 'https://m.madbookie.com.au/'


def _make_mkt_dict(mkt_block):
    mkts = {}
    for a in mkt_block:
        click_params = re.findall(re.escape('(')+"(.*)"+re.escape(')'), a['onclick'])[0].replace("'", '').replace('"', '').split(',')
        # click_params like ['this', ' M', '7417087', '', 'AWin', '', '1.9700', '0', '0']
        mkt_key = click_params[4].lower()
        mkts[mkt_key] = click_params[6: 9]
        ev_id = click_params[2]

    return [ev_id, mkts]


def _make_link(url, ev_id):
    global host
    start_str = '.com.au/'
    start_pos = url.find(start_str) + len(start_str)
    return host + url[start_pos:].replace('Matches', 'Match') + '/' + ev_id


def madbookie23(html, params, isTest=False):
    #    2 Outcome Sports
    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']

    results = {}

    soup = BeautifulSoup(html, 'lxml')

    try:

        for event in soup.select('table.MarketTable.MatchMarket'):

            try:

                team_block1 = event.find_all('tr')[1]
                team_block2 = event.find_all('tr')[2]

                odd1, odd2, oddD = 0, 0, 0
                h2h, lines, ou, margins = {}, {}, {}, {}

                team1 = team_block1.find('th').text
                try: team1 = participantMap[team1]
                except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
                if not team1: continue

                team2 = team_block2.find('th').text
                try: team2 = participantMap[team2]
                except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
                if not team2: continue


                ev_id, mkts = _make_mkt_dict(team_block1.find_all('a'))
                mkts.update(_make_mkt_dict(team_block2.find_all('a'))[1])

                try:
                    link = _make_link(url, ev_id)
                except:
                    link = url

                # h2h
                mkt_key = 'awin'
                if mkt_key in mkts:
                    odd1 = validate.cleanOdd(mkts[mkt_key][0])

                if not odd1: continue

                mkt_key = 'bwin'
                if mkt_key in mkts:
                    odd2 = validate.cleanOdd(mkts[mkt_key][0])

                if not odd2: continue


                mkt_key = 'draw'
                if mkt_key in mkts:
                    oddD = validate.cleanOdd(mkts[mkt_key][0])

                h2h = [odd1, odd2, oddD]


                # Lines

                if 'aline' in mkts and 'bline' in mkts:

                    lineOdd1, linePoint1, _ = mkts['aline']
                    lineOdd2, linePoint2, _ = mkts['bline']
                    lines = validate.line_clean([lineOdd1, linePoint1, lineOdd2, linePoint2])

                # OU
                if 'totalover' in mkts and 'totalunder' in mkts:
                    ouOdd1, ouPoint1, _ = mkts['totalover']
                    ouPoint1 = 'over ' + ouPoint1

                    ouOdd2, _, ouPoint2 = mkts['totalunder'] #for some reason, under points is index 2
                    ouPoint2 = 'under ' + ouPoint2

                    ou = validate.ou_clean([ouOdd1, ouPoint1, ouOdd2, ouPoint2])


                # Margins
                #   TODO


                odds = {
                    'H2H': h2h,
                    'lines': lines,
                    'ou': ou,
                    'margins': margins
                }

                eventName = validate.makeEventName(team1, team2)

                if eventName not in results:
                    results[eventName] = {
                        'teams': [team1, team2],
                        'odds': odds,
                        'link': link
                    }


            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results

