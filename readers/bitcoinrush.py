from components.validate import UTC_to_STR
import components.market_cleaner as market_cleaner
from bs4 import BeautifulSoup
import lxml

def _extract_vals(element):
    return element.encode_contents().strip().split('<br/>')

def _get_layout(event):
    """
        Infer layout based on date element
    """
    date_element = event.find(class_='match-date-5col')
    if date_element:
        return '5col'

    date_element = event.find(class_='match-date-4col')
    if date_element:
        return '4col'

    return None

    
def bitcoinrush23(html, params, test=False):
    soup = BeautifulSoup(html, 'lxml')
    results = []

    for event in soup.find_all('div', class_='row match-one-game'):
        odds = {}

        layout = _get_layout(event) #  TODO this should ideally be called once outside of loop

        date_element = event.find(class_='match-date-{}'.format(layout))
        date_parts = _extract_vals(date_element)
        teams = _extract_vals(event.find(class_='match-names-{}'.format(layout)))

        try:
            h2h_parts = _extract_vals(event.find(class_='match-moneyline-{}'.format(layout)))
            odds['h2h'] = [h2h_part.split(' / ')[1] for h2h_part in h2h_parts]
        except:
            pass

        try:
            spread_parts = _extract_vals(event.find(class_='match-spread'))
            spreads = [spread_part.split(' / ') for spread_part in spread_parts]
            point1 = spreads[0][0].split(' ')[0]
            odd1 = spreads[0][1]
            point2 = spreads[1][0].split(' ')[0]
            odd2 = spreads[1][1]
            odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])

        except:
            pass
        
        try:
            total_parts = event.find(class_='match-totalpoints').find_all('select')
            totals1 = total_parts[0].find('option').text.split(' / ')
            totals2 = total_parts[1].find('option').text.split(' / ')

            odd1 = totals1[1]
            odd2 = totals2[1]

            point1_parts = totals1[0].split(' ')
            point1 = point1_parts[0] + ' ' + point1_parts[1]

            point2_parts = totals2[0].split(' ')
            point2 = point2_parts[0] + ' ' + point2_parts[1]

            odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

        except:
            pass

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': int(UTC_to_STR(''.join(date_parts), '%Y-%m-%d %H:%M'))
        })

    return results
