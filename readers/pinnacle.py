import json
from components.scrape_tools import where_dict, datetime_to_unix
from components import market_cleaner

def pinnacle23(html, params, is_test=False):
    
    H2H_MARKETS = [
        'moneyline',
    ]

    SPREADS_MARKETS = [
        'spread',
    ]

    TOTALS_MARKETS = [
        'team_total',
    ]
    
    j = json.loads(html)
    
    events = j['matchups']
    markets = make_market_dict(j['markets'])
    result = []

    for event in events:

        if event['type'] != 'matchup':
            continue

        odds = {}
        for market in markets.get(event['id'], []):
            if market['type'] in H2H_MARKETS:
                odds['h2h'] = get_odds_h2h(market)

            elif market['type'] in SPREADS_MARKETS:
                odds['spreads'] = get_odds_spreads(market)

            elif market['type'] in TOTALS_MARKETS:
                odds['totals'] = get_odds_totals(market)

        if not odds:
            continue

        result.append({
            'teams': [
                where_dict(event['participants'], 'alignment', 'home')['name'],
                where_dict(event['participants'], 'alignment', 'away')['name'],
            ],
            'odds': odds,
            'commence_time': datetime_to_unix(event['startTime'], '%Y-%m-%dT%H:%M:%SZ'), # TODO like 2019-09-14T14:00:00Z
            'home_team': where_dict(event['participants'], 'alignment', 'home')['name'],
        })

    return result

def make_market_dict(markets):
    """ Reconstruct markets to have event id key

    """
    result = {}
    for market in markets:
        if market['status'] != 'open':
            continue
    
        if market['isAlternate']:
            # ignore non-popular spreads and totals markets
            continue

        if market['matchupId'] not in result:
            result[market['matchupId']] = []

        result[market['matchupId']].append(market)
    
    return result

def get_odds_h2h(market):
    odds = [
        where_dict(market['prices'], 'designation', 'home')['price'],
        where_dict(market['prices'], 'designation', 'away')['price']
    ]

    if len(market['prices']) == 3:
        odds.append(where_dict(market['prices'], 'designation', 'draw')['price'])
    return odds

def get_odds_totals(market):
    over = where_dict(market['prices'], 'designation', 'over')
    under = where_dict(market['prices'], 'designation', 'under')    
    return market_cleaner.totals_clean([over['price'], 'over ' + str(over['points']), under['price'], 'under ' + str(under['points'])])

def get_odds_spreads(market):
    team1 = where_dict(market['prices'], 'designation', 'home')
    team2 = where_dict(market['prices'], 'designation', 'away')    
    return market_cleaner.line_clean([team1['price'], team1['points'], team2['price'], team2['points']])
