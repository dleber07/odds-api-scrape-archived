import json
from components.validate import UTC_to_STR
"""
    NOTE
    To access all markets for an event: https://api.pointsbet.com/api/v2/events/{event['key']} 
"""

def pointsbetau23(html, params, is_test=False):

    valid_markets = {
        'h2h': [
            'head to head',
            'match result'
        ] # market['groupName].lower()
    }

    results = []
    j = json.loads(html)

    for event in j['events']:

        teams = [
            event.get('homeTeam'),
            event.get('awayTeam')
        ]

        odds = {}

        for market in event['fixedOddsMarkets']:
            if market['groupName'].lower() in valid_markets['h2h']:
                
                # TODO ensure teams in same order as outcomes
                if len(market['outcomes']) == 3:
                    odds['h2h'] = [
                        market['outcomes'][0]['price'],
                        market['outcomes'][2]['price'],
                        market['outcomes'][1]['price'],
                    ]
                else:
                    odds['h2h'] = [
                        market['outcomes'][0]['price'],
                        market['outcomes'][1]['price'],
                    ]

        if not odds:
            continue

        results.append({
            'teams': teams,
            'home_team': event.get('homeTeam'),
            'commence_time': int(UTC_to_STR(event['startsAt'], '%Y-%m-%d %H:%M:%S')),
            'odds': odds
        })

    return results
