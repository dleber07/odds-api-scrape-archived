from bs4 import BeautifulSoup
from components.validate import american_to_decimal
from components import market_cleaner


def _clean_odd(odd_raw):
    try:
        return american_to_decimal(float(odd_raw.replace('&nbsp;', '').strip()))
    except:
        return 0

def _split_point_odds(html):
    point_sign = html[0]
    point_odd = html[ 1 : ]
    if point_odd.find('+') > -1:
        point, odd = point_odd.split('+', 1)
        odd = '+' + odd
    else:
        point, odd = point_odd.split('-', 1)
        odd = '-' + odd
    
    point = point_sign + point
    # replace fractions with decimals
    point = point.replace(u'\xbd', '.5')
    point = point.replace(u'\xbe', '.75')
    point = point.replace(u'\xbc', '.25')

    # if totals
    point = point.replace('o', 'over ')
    point = point.replace('u', 'under ')

    odd = american_to_decimal(float(odd))

    return (point, odd)


def bookmaker23(html, params, is_test):

    results = []
    soup = BeautifulSoup(html, 'lxml')

    league = None

    if not params['scrape_config'].get('custom1'):
        raise UserWarning('Forgot to specify custom1 field (league name)')

    # Extract the league of interest
    for league_elem in soup.find_all('div', class_='league'):

        league_header = league_elem.find('h2')

        if not league_header:
            continue

        if league_header.text.strip() == params['scrape_config']['custom1']:
            league = league_elem
            break
    
    if not league:
        raise UserWarning('League not found')

    for event_container in league.find_all('div', class_='matchup'):
        event = event_container.find('li', class_='odds')

        team1_info = event.find('div', class_='hTeam') # home
        team2_info = event.find('div', class_='vTeam') # vistor
        teamD_info = event.find('div', class_='dTeam') # draw

        teams = [
            team1_info.find(class_='team').text.strip(),
            team2_info.find(class_='team').text.strip(),
        ]

        odds = {}

        if team1_info.find('div', class_='money'):
            odds['h2h'] = [
                _clean_odd(team1_info.find('div', class_='money').text),
                _clean_odd(team2_info.find('div', class_='money').text)
            ]
            if teamD_info:
                odds['h2h'].append(
                    _clean_odd(teamD_info.find('div', class_='money').text)
                )

        if team1_info.find('div', class_='spread'):
            try:
                point1, odd1 = _split_point_odds(team1_info.find('div', class_='spread').text)
                point2, odd2 = _split_point_odds(team2_info.find('div', class_='spread').text)
                odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])
            except Exception as e:
                if is_test:
                    print e


        if team1_info.find('div', class_='total'):
            try:
                point1, odd1 = _split_point_odds(team1_info.find('div', class_='total').text)
                point2, odd2 = _split_point_odds(team2_info.find('div', class_='total').text)
                odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
            except Exception as e:
                if is_test:
                    print e

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds
        })

    return results
