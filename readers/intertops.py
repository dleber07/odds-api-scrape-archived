from bs4 import BeautifulSoup
from components.validate import american_to_decimal, UTC_to_STR
from components import market_cleaner

def _get_commence_time(s):
    return int(UTC_to_STR(s.replace('<br/>', ' '), '%m/%d/%Y %I:%M %p'))


def intertops2(html, params, is_test):
    results = []
    soup = BeautifulSoup(html, 'lxml')

    mkt_names = []
    mkt_name_elems = soup.find('div', class_='markettable').select('div.res2.th')
    resclass = 'res2'
    
    if not mkt_name_elems:
        mkt_name_elems = soup.find('div', class_='markettable').select('div.res3.th')
        resclass = 'res3'
    
    if not mkt_name_elems:
        mkt_name_elems = soup.find('div', class_='markettable').select('div.res4.th')
        resclass = 'res4'
    
    if not mkt_name_elems:
        mkt_name_elems = soup.find('div', class_='markettable').select('div.res5.th')
        resclass = 'res5'

    if not mkt_name_elems:
        raise UserWarning('Unknown res class - don\'t know what markets to collect')


    for th in mkt_name_elems:
        # get market names from table headers
        mkt_names.append(th.text)


    for event in soup.find('div', class_='panel-primary').select('div.onemarket.tr'):

        # TODO
        # if find('h4', class_='panel-title').text != 'Game Lines':
        #   continue
        try:
            teams = [
                event.find('div', class_='ustop').text.strip(),
                event.find('div', class_='usbot').text.strip()
            ]
        except:
            raise UserWarning('No teams found. If 1 mkt, try intertops2b reader instead')

        if not teams:
            continue

        commence_time = _get_commence_time(event.find('span', class_='eventdatetime')['title'])

        odds = {}

        for i, mkt in enumerate(event.select('div.' + resclass + '.td')):

            if mkt_names[i] == 'Money Line':
                outcomes = mkt.find_all('a')
                if not outcomes:
                    continue

                odds['h2h'] = [
                    outcomes[0]['data-o-inv'],
                    outcomes[1]['data-o-inv']
                ]
            
            elif mkt_names[i] == 'Spread':
                outcomes = mkt.find_all('a')
                if not outcomes:
                    continue

                try:
                    odd1 = outcomes[0]['data-o-inv']
                    point1 = outcomes[0]['data-o-pts']
                    odd2 = outcomes[1]['data-o-inv']
                    point2 = outcomes[1]['data-o-pts']
                    odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])
                except:
                    pass

            elif mkt_names[i] == 'Total':
                outcomes = mkt.find_all('a')
                if not outcomes:
                    continue

                try:
                    odd1 = outcomes[0]['data-o-inv']
                    point1 = outcomes[0]['data-o-pts']
                    odd2 = outcomes[1]['data-o-inv']
                    point2 = outcomes[1]['data-o-pts']
    
                    if 'OVER' in mkt.find('div', class_='tablebutton').get('title', ''):
                        point1 = 'over ' + point1
                        point2 = 'under ' + point2
                    elif  'UDNER' in mkt.find('div', class_='tablebutton').get('title', ''):
                        point1 = 'under ' + point1
                        point2 = 'over ' + point2
                    else:
                        continue

                    odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])
                except:
                    pass

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': commence_time
        })

    return results

def intertops2b(html, params, is_test):
    """

        For single market (h2h), 2-outcome sports, such as tennis

    """
    results = []
    soup = BeautifulSoup(html, 'lxml')
    
    for event in soup.select('div.odds.markettable')[0].select('div.onemarket.tr'):
        commence_time = _get_commence_time(event.find('span', class_='eventdatetime')['title'])
        teams = event.find('b', class_='cl-ttl').text.split(' v ')
        odds_elems = event.find_all('span', class_='odds')

        odds = {
            'h2h': [
                odds_elems[0]['data-o-dec'],
                odds_elems[1]['data-o-dec'],
            ]
        }

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': commence_time
        })

    return results

def intertops3(html, params, is_test):
    results = []
    soup = BeautifulSoup(html, 'lxml')

    for event in soup.find('div', class_='panel-primary').select('div.onemarket.tr'):

        # TODO
        # if find('h4', class_='panel-title').text != 'Match Winner':
        #   continue

        teams = event.find('a', class_='seeall').find('b').text.split(' v ')
        commence_time = int(UTC_to_STR(event.find('span', class_='eventdatetime')['title'].replace('<br/>', ' '), '%m/%d/%Y %I:%M %p'))
        odds = {
            'h2h': []
        }

        for i, mkt in enumerate(event.find_all('div', class_='res2')):
            odds['h2h'].append(mkt.find('a')['data-o-inv'])
        oddD = odds['h2h'][1]
        odds['h2h'][1] = odds['h2h'][2]
        odds['h2h'][2] = oddD

        if not odds:
            continue

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': commence_time
        })

    return results
