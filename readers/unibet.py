from components import validate, market_cleaner
import time, json

site = 'UniBet'
host = 'https://www.unibet.com.au/'

def unibet23(html, params, test=False):
    #        2 or 3 Outcome Sports
    #            scrapes unibet API v3
    #            Implemented 7-Jan-16

    # Note: received ssl handshake error when making request
    # fixed with pip install requests[security]

    results = []
    matches = json.loads(html)

    for match in matches["events"]:

        # eid = match['event']['id']
        """
        if e['event']['state'] == 'STARTED': 
            linkBase = 'betting/bettingclient#event/live/'
        else:
            linkBase = 'betting/bettingclient#event/'
        try:
            link = host + linkBase + str(eid)
        except:
            link = host # do NOT send them to the API
        """

        odds = {}
        evInfo = {}
        
        # capture the start time
        try:
            team1 = match['event']['homeName'].encode('utf8')
            team2 = match['event']['awayName'].encode('utf8')
        except:
            continue

        teams = [team1, team2]
        home_team = team1

        # Collect odds
        for o in match['betOffers']:

            if not o['outcomes'][0].get('odds'):
                continue

            offerType = o['betOfferType']['name'].lower()
            
            if 'odds' not in o['outcomes'][0] or 'odds' not in o['outcomes'][1]:
                continue

            odd1 = float(o['outcomes'][0]['odds']) / 1000
            odd2 = float(o['outcomes'][1]['odds']) / 1000

            if offerType in ['head to head', 'match']:
                if len(o['outcomes']) == 3 and o['outcomes'][1]['label'] == 'X':
                    oddD = odd2
                    odd2 = float(o['outcomes'][2]['odds']) / 1000
                    odds['h2h'] = [odd1, odd2, oddD]
                else:
                    odds['h2h'] = [odd1, odd2]

            elif offerType == 'line':
                point1 = float(o['outcomes'][0]['line']) / 1000
                point2 = float(o['outcomes'][1]['line']) / 1000
                odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])

            elif offerType == 'totals':
                point1 = o["outcomes"][0]["label"] + ' ' + str( float(o["outcomes"][0]["line"]) / 1000)
                point2 =o["outcomes"][1]["label"] + ' ' + str(float(o["outcomes"][1]["line"]) / 1000)
                odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

        try:
            # LIVE SCORES - set scores regardless of available odds
            if match['event']['state'] == 'STARTED':
                scores = match['liveData']['score'] # home, away, info, who
                scoreList = [scores.pop('home', None), scores.pop('away', None)]
                """
                clock = match['liveData']['matchClock'] # minute, second, period, running
                clock.pop('running', None) # remove unwanted fields. what is 'running'?
                stats = {}
                try:
                    stats = match['liveData']['statistics']['setBasedStats']
                    stats[team1] = stats.pop('home', None)
                    stats[team2] = stats.pop('away', None)
                except:
                    pass
                """

                evInfo['score'] = scoreList
                # evInfo['clock'] = clock
                # evInfo['stats'] = stats

        except Exception as e:
            pass

        if type(match['event']['start']) is str or type(match['event']['start']) is unicode:
            try:
                commence_time = int(validate.UTC_to_STR(match['event']['start'], '%Y-%m-%d %H:%M:%S'))
            except:
                try:
                    commence_time = int(validate.UTC_to_STR(match['event']['start'], '%Y-%m-%d %H:%M:%S.%f'))
                except Exception as e:
                    raise UserWarning('Unknown time format')
        else:
            commence_time = match['event']['start'] / 1000            


        if odds:
            results.append({
                'teams': teams,
                'odds': odds,
                'commence_time': commence_time,
                'home_team': home_team,
                # 'link': link,
                'other': evInfo
            })

    return results


def unibet0(html, params, isTest = False):

    global site, host
    scrapeMethod = 'WEB'

    sport = params['sport']
    sportName, eventName = sport.split('___')
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    outrightEventName = params['scrapeInfo']['outrightEventName']
    oddThreshold = params['sportInfo']['oddThreshold']
    url = params['url']
    results = {}
    hashData = {}

    matches = json.loads(html)
    try:
        unibet_event_id = ''
        unibet_sport_id = ''
        matchCriteria = outrightEventName
        if matchCriteria.find('_') != -1:
            matchMethod_matchName = matchCriteria.split('_')
            matchMethod = matchMethod_matchName[0]
            outrightEventName = matchMethod_matchName[1]
        else:
            matchMethod = 'events'
            outrightEventName = matchCriteria

        try: oddThreshold = float(oddThreshold)
        except: oddThreshold = False

        if matchMethod == 'events':

            for e in matches["events"]:
                if acceptComp(e["name"], outrightEventName, sport, site) == True:
                    competition = e["name"]    #    not used... yet
                    unibet_event_id = e["id"]
                    try: unibet_sport_id = e["sportId"]
                    except: unibet_sport_id = e["groupId"]
                    break
        else:
            sportIds = re.findall('\/(\d+)\.json', url)
            unibet_sport_id = sportIds[0].strip('/').strip('.json')

        link = host + 'betting#/'
        if unibet_sport_id:
            link += "group/" + str(unibet_sport_id) + "/category/0"


        for m in matches["betoffers"]:
            try:
                eid = m["eventId"]
                if (matchMethod == 'events' and unibet_event_id == eid and m["betOfferType"]["name"] in ["Winner"]) or (matchMethod == 'betoffers' and m['criterion']['label'] == outrightEventName):
                    for o in m["outcomes"]:

                        team = o["label"]
                        try: team = participantMap[team]
                        except: team = validate.resolveParticipant(outcomeType, team, listName, sport, site)
                        if not team: continue

                        odd = float(o["odds"])/1000
                        odd = validate.cleanOdd(odd)
                        if not odd: continue

                        if team not in hashData and validate.isValidOutrightOdd(odd, oddThreshold):
                            hashData[team] = odd

            except Exception as e:
                if isTest: print sport, site, scrapeMethod, e
                pass

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    results[eventName] = {
        'odds': hashData,
        'link': link
    }

    return results

"""
def unibet23(html, params, test=False):


    results = {}

    matches = json.loads(html)

    try:
        # Map eventIds to team names
        eventMap = {}
        for match in matches["events"]:
            try:
                team1, team2 = False, False
                if ('homeName' in e) and ('awayName' in e):
                    team1 = e["homeName"]
                    try: team1 = participantMap[team1]
                    except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
                    if not team1: continue

                    team2 = e["awayName"]
                    try: team2 = participantMap[team2]
                    except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
                    if not team2: continue

                    if team1 != team2:
                        eventMap[e["id"]] = {'teams':[team1, team2], 'odds':{}}
            except Exception as e:
                if test:
                    print sport, site, scrapeMethod, e
                pass

        for m in matches["betoffers"]:
            odd1, odd2 = False, False
            lines, ou = {}, {}
            try:
                eid = m["eventId"]
                if eid in eventMap:
                    try: link = host + 'betting/bettingclient#event/' + str(eid)
                    except: link = host

                    if m["betOfferType"]["name"].lower() in ['head to head', 'match']:
                        oddDraw = ''
                        if len(m["outcomes"]) == 3:

                            odd1 = float(m["outcomes"][0]["odds"])/1000
                            odd2 = float(m["outcomes"][2]["odds"])/1000
                            oddDraw = float(m["outcomes"][1]["odds"])/1000

                        else:
                            odd1 = float(m["outcomes"][0]["odds"])/1000
                            odd2 = float(m["outcomes"][1]["odds"])/1000

                        odd1 = validate.cleanOdd(odd1)
                        odd2 = validate.cleanOdd(odd2)
                        oddDraw = validate.cleanOdd(oddDraw)
                        odds = [odd1, odd2]
                        if oddDraw: odds.append(oddDraw)

                        eventMap[eid]['odds']['H2H'] = odds

                    elif m["betOfferType"]["name"] == 'Line':
                        try:
                            odd1 = float(m["outcomes"][0]["odds"])/1000
                            odd2 = float(m["outcomes"][1]["odds"])/1000
                            point1 = float(m["outcomes"][0]["line"])/1000
                            point2 = float(m["outcomes"][1]["line"])/1000
                            lines = validate.line_clean([odd1,point1,odd2,point2])
                            eventMap[eid]['odds']['lines'] = lines
                        except: pass


                    elif (m["betOfferType"]["name"]=='Totals' and 'Total Points' in m["criterion"]["label"]):
                        try:
                            odd1 = float(m["outcomes"][0]["odds"])/1000
                            odd2 = float(m["outcomes"][1]["odds"])/1000
                            point1 = m["outcomes"][0]["label"] +' '+ str( float(m["outcomes"][0]["line"])/1000 )
                            point2 = m["outcomes"][1]["label"] +' '+ str(float(m["outcomes"][1]["line"])/1000 )
                            ou = validate.ou_clean([odd1,point1,odd2,point2])
                            eventMap[eid]['odds']['ou'] = ou
                        except: pass
            except:
                pass

        for e in eventMap:
            teams = eventMap[e]['teams']
            odds = eventMap[e]['odds'] #{'H2H' : odds, 'lines':lines,'ou':ou }

            if False not in odds['H2H']:
                eventName = validate.makeEventName(teams[0], teams[1])

                if eventName not in results:
                    results[eventName] = {
                        'teams': teams,
                        'odds': odds,
                        'link': link
                    }

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass


    return results
 """