from components import validate
import time, json

site = 'vonbets'

def vonbets23(html, params, isTest=False):
    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    url = params['url']
    results = {}

    jEvents = json.loads(html)

    try:
        for event in jEvents['events']:

            team1 = event['competitors'][0]['name']

            try: team1 = participantMap[team1]
            except: team1 = validate.resolveParticipant(outcomeType, team1, listName, sport, site)
            if not team1: continue

            team2 = event['competitors'][1]['name']
            try: team2 = participantMap[team2]
            except: team2 = validate.resolveParticipant(outcomeType, team2, listName, sport, site)
            if not team2: continue

            try:
                link = 'https://m.vonbets.com/event-details/?eventid=' + str(event['id'])
            except:
                link = 'https://m.vonbets.com/'


            odd1, odd2, oddD = 0, 0, 0
            oddsBlock = event['children'][0]['children']

            odd1 = validate.cleanOdd(oddsBlock[0]['odds'])

            if len(oddsBlock) == 3:
                oddD = validate.cleanOdd(oddsBlock[1]['odds'])
                odd2 = validate.cleanOdd(oddsBlock[2]['odds'])
            else:
                odd2 = validate.cleanOdd(oddsBlock[1]['odds'])

            h2h = [odd1, odd2, oddD]

            eventName = validate.makeEventName(team1, team2)

            if eventName not in results:
                results[eventName] = {
                    'teams': [team1, team2],
                    'odds': {
                        'H2H': h2h
                    },
                    'link': link
                }

    except Exception as e:
        if isTest: print sport, site, scrapeMethod, e
        pass

    return results

