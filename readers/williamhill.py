#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import lxml
from components import market_cleaner
import re
import json
from components.validate import UTC_to_STR, frac_to_decimal

def williamhill2(html, params, isTest=False):

    """
        TODO
        use this url format: https://w.sports.williamhill.com/fragments/competitionEventList/en-gb/basketball/OB_TY266
        find competition id (e.g. OB_TY266) from li['data-competition-id']
        Beward of "Show More". If present, increment url with like https://w.sports.williamhill.com/fragments/competitionEventList/en-gb/basketball/OB_TY266/1

        add spreads and totals urls too. special fetcher should agreggate

    """

    results = []
    soup = BeautifulSoup(html, 'lxml')

    parent_container = soup.find('div', {"data-marketgroupname" : "Money Line"})
    if not parent_container:
        valid_markets = ['Money Line', 'Bout Betting', 'Home']

        market_name = soup.find('p', class_='btmarket__name').text
        if market_name not in valid_markets:
            raise UserWarning('Unable to read market ' + market_name)
        
        parent_container = soup.find('section', class_='event-container')
 

    for event in parent_container.find_all('div', class_='event'):

        try:
            event_name = event.find('div', class_='btmarket__link-name').text
        except:
            continue

        commence_time = _format_commence_time(event.find('time', class_='eventStartTime')['datetime'])
        
        teams_html = event.find('div', class_='btmarket__link-name--2-rows').find_all('span')
        teams = [
            teams_html[0].text,
            teams_html[1].text,
        ]

        # TODO - from old method. Remove if not needed
        # teams = team_str.replace(u'\xa0', '').split(split_str)


        link = 'https://w.sports.williamhill.com' + event.find('a', class_='btmarket__name').text
        # TODO consider using link to gather other markets
        


        market_block = event.find('div', class_='btmarket__actions')
        if market_block['data-displayed'] == 'false':
            continue

        odds_buttons = market_block.find_all('button')

        odds = {}
        odds['h2h'] = [
            frac_to_decimal(float(odds_buttons[0]['data-num']), float(odds_buttons[0]['data-denom'])),
            frac_to_decimal(float(odds_buttons[1]['data-num']), float(odds_buttons[1]['data-denom'])),
        ]

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': commence_time,
        })

    return results

def williamhill2b(html, params, isTest=False):
    """
        For 2 outcome h2h only (used for tennis)
    """
    results = []
    soup = BeautifulSoup(html, 'lxml')
    comp_name = params['scrape_config'].get('custom1')

    for comp in soup.select('article.comp-container'):

        if comp_name and comp.find('h3').text != comp_name:
            continue

        for event in comp.select('div.event'):
            try:
                commence_time = _format_commence_time(event.find('time', class_='eventStartTime')['datetime'])
            except:
                continue

            odds_buttons = event.find_all('button', class_='oddsbutton')

            if len(odds_buttons) < 2:
                continue

            teams = [
                odds_buttons[0]['data-name'],
                odds_buttons[1]['data-name'],
            ]

            odds = {
                'h2h': [
                    frac_to_decimal(float(odds_buttons[0]['data-num']), float(odds_buttons[0]['data-denom'])),
                    frac_to_decimal(float(odds_buttons[1]['data-num']), float(odds_buttons[1]['data-denom'])),
                ]
            }

            results.append({
                'teams': teams,
                'odds': odds,
                'commence_time': commence_time,
            })

    return results

def williamhill23(html, params, isTest=False):

    market_map = {
        # map willhill markets (marketGroupName) to OA markets
        '90 Minutes': 'h2h',
        'Total Match Goals Over/Under 2.5 Goals': 'totals',
        # TODO spreads, over totals for 2 outcomes
    }

    results = []
    j = json.loads(html)
    
    for event_key, event in j['data']['events'].iteritems():
        if not event['displayed']:
            continue

        teams = _get_teams(event['name'])
        if not teams:
            continue

        odds = {}
        for market_group_key, market_group_list in event['marketGroups'].iteritems():
            market_group = market_group_list[0]
            
            if market_group['marketGroupName'] not in market_map:
                continue

            selection_ids = j['data']['markets'][market_group['marketId']]['selectionIds']
            selection_names = j['data']['markets'][market_group['marketId']]['selectionNames']
            if market_map[market_group['marketGroupName']] == 'h2h':
                
                # selections keys will be 'H', 'D' and 'A' for home draw away
                selections = {}
                for selection_id, selection_name in zip(selection_ids, selection_names):
                    selections[selection_name] = j['data']['selections'][selection_id]

                home_team = selections['H']['name']

                if teams.index(home_team) == 1:
                    # first odd is the second team. Need to reverse teams
                    teams = teams[::-1]
                
                odds['h2h'] = [
                    frac_to_decimal(float(selections['H']['currentPriceNum']), selections['H']['currentPriceDen']),
                    frac_to_decimal(float(selections['A']['currentPriceNum']), selections['A']['currentPriceDen']),
                ]

                if len(selections) == 3:   
                    odds['h2h'].append(
                        frac_to_decimal(selections['D']['currentPriceNum'], selections['D']['currentPriceDen'])
                    )

            elif market_map[market_group['marketGroupName']] == 'totals':
                selection_ids = j['data']['markets'][market_group['marketId']]['selectionIds']
                selection1 = j['data']['selections'][selection_ids[0]]
                selection2 = j['data']['selections'][selection_ids[1]]

                odd1 = frac_to_decimal(float(selection1['currentPriceNum']), selection1['currentPriceDen'])
                point1 = selection1['name']
                odd2 = frac_to_decimal(float(selection2['currentPriceNum']), selection2['currentPriceDen'])
                point2 = selection2['name']
                odds['totals'] = market_cleaner.totals_clean([odd1, point1, odd2, point2])

        if not odds:
            continue
        
        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': _format_commence_time(event['startDateTime']),
            'home_team': home_team,
        })

    return results

"""
    Args:
        dt (str) -  like "2019-03-30 12:30:00.000+0000" (datetime struggles with offset (%z), needs other libs)

    Returns:
        int
"""
def _format_commence_time(dt):
    dt = dt[:dt.find('+')] # like 2019-03-30 12:30:00.000
    time_format = '%Y-%m-%d %H:%M:%S.%f' if '.' in dt else '%Y-%m-%d %H:%M:%S'
    return int(UTC_to_STR(dt, time_format))

"""
    Args:
        event_name (str) -  like "team1 @ team2", or "team1 v team2"

    Returns:
        list
"""
def _get_teams(event_name):
    teams = []
    if ' @ ' in event_name:
        teams = event_name.split(' @ ')
    elif ' v ' in event_name:
        teams = event_name.split(' v ')

    return teams

"""
deprecated
"""
def williamhill23_old(html, params, isTest=False):
    results = []
    soup = BeautifulSoup(html, 'lxml')
    events_html = None

    for mkt_block in soup.select('#newBetLiveHolder'):
        if not mkt_block.find(id='upcomingHeader'):
            continue

        events_html = mkt_block
        break

    if events_html:
        event_trs = events_html.find(class_='marketHolderExpanded').find_all('tr', class_='rowOdd')
    else:
        event_trs = soup.find(class_='marketHolderExpanded').find_all('tr', class_='rowOdd')

    for event_tr in event_trs:

        if not event_tr.find('td'):
            # assume it's a header row
            continue

        split_str = ' @ '
        team_str = event_tr.find(text=re.compile(split_str))
        if not team_str:
            split_str = ' v '
            team_str = event_tr.find(text=re.compile(split_str))
    
        if not team_str:
            team1 = event_tr.find_all('td')

        if not team_str:
            raise UserWarning('Dont know how to separate teams. @ and v not valid')


        teams = team_str.replace(u'\xa0', '').split(split_str)
        if not teams:
            continue

        odds_raw = event_tr.find_all(class_='eventprice')
        oddD = 0
        odd1 = odds_raw[0].text.replace('\t', '').replace('\n', '')

        if len(odds_raw) == 3:
            odd2 = odds_raw[2].text.replace('\t', '').replace('\n', '')
            oddD = odds_raw[1].text.replace('\t', '').replace('\n', '')
        else:
            odd2 = odds_raw[1].text.replace('\t', '').replace('\n', '')
        
        odds = {
            'h2h': [odd1, odd2]
        }

        if oddD:
            odds['h2h'].append(oddD)

        results.append({
            'teams': teams,
            'odds': odds
        })

    return results

"""
deprecated
"""
def williamhill_mkts2_old(html, params, isTest=False):
    results = []
    soup = BeautifulSoup(html, 'lxml')

    for tr_1 in soup.find(id='tup_type_1_mkt_grps').select('tr.rowOddUS'):
        tds_1 = tr_1.find_all('td')
        team1 = tds_1[2].find('a').text

        tr_2 = tr_1.findNext('tr')
        tds_2 = tr_2.find_all('td')
        team2 = tds_2[2].find('a').text

        odds = {}

        moneyline_index = 3
        odds['h2h'] = [
            tds_1[moneyline_index].text.strip(),
            tds_2[moneyline_index].text.strip()
        ]
        
        try:
            spread_point_index = 4
            spread_odd_index = 5
            odds['spreads'] = market_cleaner.line_clean([
                tds_1[spread_odd_index].text.strip(),
                tds_1[spread_point_index].text.strip(),
                tds_2[spread_odd_index].text.strip(),
                tds_2[spread_point_index].text.strip(),
            ])
        except:
            pass

        try:
            totals_point_index = 6
            totals_odd_index = 7
            odds['totals'] = market_cleaner.totals_clean([
                tds_1[totals_odd_index].text.strip(),
                tds_1[totals_point_index].text.strip().replace('O', 'over').replace('U', 'under'),
                tds_2[totals_odd_index].text.strip(),
                tds_2[totals_point_index].text.strip().replace('U', 'under'),
            ])
        except:
            pass

        try:
            # try extract commence time - not always available
            commence_str = tds_1[0].find('span')['id']
            commence_time = int(re.search(r'br:(.*?):br', commence_str).group(1))
        except:
            commence_time = None
            pass

        event = {
            'teams': [team1, team2],
            'odds': odds
        }
        if commence_time:
            event['commence_time'] = commence_time
        results.append(event)

    return results
