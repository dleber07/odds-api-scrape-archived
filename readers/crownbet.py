from components import validate, market_cleaner
import json
import re

def crownbet23(html, params, test=False):
    results = []

    valid_markets = ["match winner", "h2h", "link", "head to head", "win-draw-win","match result", "fight result"]

    java_start_string =  'app.module.sports, ['
    start_pos = html.find(java_start_string)
    
    if start_pos == -1:
        # 'CrownBet: JSON NOT FOUND'
        return []

    start_pos += len(java_start_string)
    end_pos = html.find("]);", start_pos)
    json_str = html[start_pos : end_pos]

    j = json.loads(json_str)
    #print json.dumps(j, indent=4, sort_keys=True)
    matches = j['sports']["events"]

    for m in matches:

        try:
            odds = {}

            if m["BettingType"][0]["EventName"].lower() not in valid_markets:
                continue

            mkt_h2h = m["BettingType"][0]["Outcomes"]
            if len(mkt_h2h) == 3:

                team1 = mkt_h2h[0]["OutcomeName"]
                team2 = mkt_h2h[2]["OutcomeName"]

                odds['h2h'] = [
                    mkt_h2h[0]["BetTypes"][0]["Price"],
                    mkt_h2h[2]["BetTypes"][0]["Price"],
                    mkt_h2h[1]["BetTypes"][0]["Price"]
                ]

            else:
                team1 = mkt_h2h[0]["OutcomeName"]
                team2 = mkt_h2h[1]["OutcomeName"]

                odds['h2h'] = [
                    mkt_h2h[0]["BetTypes"][0]["Price"],
                    mkt_h2h[1]["BetTypes"][0]["Price"]
                ]

            team1Raw, team2Raw = team1, team2


            try:
                # Spreads
                if m["BettingType"][1]["EventName"].lower()  == 'line':
                    for bType in m["BettingType"][1]["Outcomes"][0]["BetTypes"]:
                        if bType["MarketTypeDesc"] == "Standard Handicap":
                            lineOdds1 = bType["Price"]
                            linePoints1 = bType["Points"]
                            lineTeam1 = m["BettingType"][1]["Outcomes"][0]["OutcomeName"]
                            break

                    for bType in m["BettingType"][1]["Outcomes"][1]["BetTypes"]:
                        if bType["MarketTypeDesc"] == "Standard Handicap":
                            lineOdds2 = bType["Price"]
                            linePoints2 = bType["Points"]
                            lineTeam2 = m["BettingType"][1]["Outcomes"][1]["OutcomeName"]
                            break

                    if team1Raw == lineTeam1:
                        odds['spreads'] = market_cleaner.line_clean([lineOdds1, linePoints1, lineOdds2, linePoints2])
                    else:
                        odds['spreads'] = market_cleaner.line_clean([lineOdds2, linePoints2, lineOdds1, linePoints1])
            except:
                pass
        
            results.append({
                'teams': [team1, team2],
                'odds': odds
                # 'link': link
            })

        except Exception:
            pass

    return results

def crownbet0(html, params, isTest=False):
    #   Futures + Multi outcomes

    global site
    scrapeMethod = 'WEB'

    sport = params['sport']
    sportName, eventName = sport.split('___')
    listName = params['sportInfo']['listName']
    outcomeType = params['sportInfo']['outcomeType']
    participantMap = params['participants']
    outrightEventName = params['scrapeInfo']['outrightEventName']
    oddThreshold = params['sportInfo']['oddThreshold']

    url = params['url']
    results = {}
    hashData = {}

    java_start_string = 'app.module.sports_single, ['
    start_pos = html.find(java_start_string)


    if start_pos == -1:
        if isTest: print sport, 'CrownBet: JSON NOT FOUND'
        return {}

    else:

        try:
            start_pos += len(java_start_string)
            end_pos = html.find("]);", start_pos)
            json_str = html[start_pos : end_pos]
            j = json.loads(json_str)
            #print json.dumps(j, indent=4, sort_keys=True)

            matches = j["event"]["EventGroups"]
            link = url

            try: oddThreshold = float(oddThreshold)
            except: oddThreshold = False

            for m in matches:
                try:

                    for bt in m['BettingType']:

                        try:

                            if bt['EventName'] != outrightEventName: continue

                            for outcome in bt["Outcomes"]:

                                team, odd = '', 0
                                team = outcome["OutcomeName"]
                                team = re.sub(r'\([^)]*\)', '', team)

                                try: team = participantMap[team]
                                except: team = validate.resolveParticipant(outcomeType, team, listName, sport, site)
                                if not team: continue

                                odd = outcome["BetTypes"][0]["Price"]
                                odd = validate.cleanOdd(odd)

                                if validate.isValidOutrightOdd(odd, oddThreshold):
                                    hashData[team] = odd

                        except Exception as e:
                            if isTest: print sport, site, scrapeMethod, e
                            pass

                except Exception as e:
                    if isTest: print sport, site, scrapeMethod, e
                    pass

        except Exception as e:
            if isTest: print sport, site, scrapeMethod, e
            pass

    results[eventName] = {
        'odds': hashData,
        'link': link
    }

    return results
