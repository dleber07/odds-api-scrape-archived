from components import validate, market_cleaner
import json
import re


def beteasy23(html, params, test=False):
    results = []
    j = json.loads(html)

    market_names = {
        # event['BettingType'][0]['EventName']
        'h2h': [
            'H2H',
            'Win-Draw-Win',
            'Head to Head',
            'Match Result',
            'Fight Result',
            'Match Winner',
        ],
        'spreads': [
            'Line',
        ],
        'totals': [

        ]
    }

    for event in j['result']['sports'].get('events', []):

        odds = {}
        event_name = event['MasterEventName']
        if event_name.find(' v ') != -1:
            teams = event['MasterEventName'].split(' v ')
        elif event_name.find(' @ ') != -1:
            teams = event['MasterEventName'].split(' @ ')
            teams = teams[::-1]
        else:
            raise UserWarning('Dont know how to separate teams. @ and v not valid')

        first_market = event['BettingType'][0] 
        commence_time = int(validate.UTC_to_STR(first_market['AdvertisedStartTime'], '%Y-%m-%d %H:%M:%S'))        

        for market in event['BettingType']:

            if not market['Outcomes']:
                continue
            
            if market['EventName'] in market_names['h2h']:

                if len(market['Outcomes']) == 3:
                    # TODO  ensure market['Outcomes'][0]['OutcomeName'] == teams[0]
                    odds['h2h'] = [
                        market['Outcomes'][0]['BetTypes'][0]['Price'],
                        market['Outcomes'][2]['BetTypes'][0]['Price'],
                        market['Outcomes'][1]['BetTypes'][0]['Price']
                    ]
                elif len(market['Outcomes']) == 2:
                    # TODO  ensure market['Outcomes'][0]['OutcomeName'] == teams[0]
                    odds['h2h'] = [
                        market['Outcomes'][0]['BetTypes'][0]['Price'],
                        market['Outcomes'][1]['BetTypes'][0]['Price']
                    ]

            elif market['EventName'] in market_names['spreads']:
                price_point = _get_spreads(market['Outcomes'][0]['BetTypes'])
                if not price_point:
                    continue

                odd1, point1 = price_point

                price_point = _get_spreads(market['Outcomes'][1]['BetTypes'])
                if not price_point:
                    continue
                odd2, point2 = price_point

                odds['spreads'] = market_cleaner.line_clean([odd1, point1, odd2, point2])

        if not odds:
            continue

        results.append({
            'teams': teams,
            'commence_time': commence_time,
            'odds': odds,
            'home_team': teams[0]
        })

    return results


def _get_spreads(bet_types):
    for bet_type in bet_types:
        if bet_type['MarketTypeDesc'] == 'Standard Handicap':
            return (bet_type['Price'], bet_type['Points'])
    return None


"""
Read from html

deprecated
"""
def beteasy_web23(html, params, test=False):
    results = []

    valid_markets = ["match winner", "h2h", "link", "head to head", "win-draw-win","match result", "fight result"]

    java_start_string =  'app.module.sports, ['
    start_pos = html.find(java_start_string)
    
    if start_pos == -1:
        # 'CrownBet: JSON NOT FOUND'
        return []

    start_pos += len(java_start_string)
    end_pos = html.find("]);", start_pos)
    json_str = html[start_pos : end_pos]

    j = json.loads(json_str)
    #print json.dumps(j, indent=4, sort_keys=True)
    matches = j['sports']["events"]

    for m in matches:

        try:
            odds = {}

            if m["BettingType"][0]["EventName"].lower() not in valid_markets:
                continue

            mkt_h2h = m["BettingType"][0]["Outcomes"]
            if len(mkt_h2h) == 3:

                team1 = mkt_h2h[0]["OutcomeName"]
                team2 = mkt_h2h[2]["OutcomeName"]

                odds['h2h'] = [
                    mkt_h2h[0]["BetTypes"][0]["Price"],
                    mkt_h2h[2]["BetTypes"][0]["Price"],
                    mkt_h2h[1]["BetTypes"][0]["Price"]
                ]

            else:
                team1 = mkt_h2h[0]["OutcomeName"]
                team2 = mkt_h2h[1]["OutcomeName"]

                odds['h2h'] = [
                    mkt_h2h[0]["BetTypes"][0]["Price"],
                    mkt_h2h[1]["BetTypes"][0]["Price"]
                ]

            team1Raw, team2Raw = team1, team2


            try:
                # Spreads
                if m["BettingType"][1]["EventName"].lower()  == 'line':
                    for bType in m["BettingType"][1]["Outcomes"][0]["BetTypes"]:
                        if bType["MarketTypeDesc"] == "Standard Handicap":
                            lineOdds1 = bType["Price"]
                            linePoints1 = bType["Points"]
                            lineTeam1 = m["BettingType"][1]["Outcomes"][0]["OutcomeName"]
                            break

                    for bType in m["BettingType"][1]["Outcomes"][1]["BetTypes"]:
                        if bType["MarketTypeDesc"] == "Standard Handicap":
                            lineOdds2 = bType["Price"]
                            linePoints2 = bType["Points"]
                            lineTeam2 = m["BettingType"][1]["Outcomes"][1]["OutcomeName"]
                            break

                    if team1Raw == lineTeam1:
                        odds['spreads'] = market_cleaner.line_clean([lineOdds1, linePoints1, lineOdds2, linePoints2])
                    else:
                        odds['spreads'] = market_cleaner.line_clean([lineOdds2, linePoints2, lineOdds1, linePoints1])
            except:
                pass
        
            results.append({
                'teams': [team1, team2],
                'odds': odds
                # 'link': link
            })

        except Exception:
            pass

    return results
