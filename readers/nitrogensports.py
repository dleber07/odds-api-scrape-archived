from components import market_cleaner
import json

def nitrogensports23(html, params, isTest=False):

    events = json.loads(html)['data']
    results = []
    
    for event in events:

        odds = {}
        stakes = {}

        teams = [ event['homeTeam_name'], event['awayTeam_name'] ]
        mkts = event['period'][0]

        if not mkts:
            continue

        if mkts.get('moneyLine'):

            odds['h2h'] = [
                mkts['moneyLine'][0]['homePrice'],
                mkts['moneyLine'][0]['awayPrice']
            ]

            if mkts['moneyLine'][0].get('drawPrice'):
                odds['h2h'].append(mkts['moneyLine'][0]['drawPrice'])

            stakes['h2h'] = [
                [0, mkts['maxBetAmount_moneyLine']],
                [0, mkts['maxBetAmount_moneyLine']]
            ]
        
        if mkts.get('spread'):
            # There may be multiple spreads. Choose the middle
            spread_index = int(len(mkts['spread']) / 2)
            spreads = mkts['spread'][spread_index]
            
            odds['spreads'] = market_cleaner.line_clean([
                spreads['homePrice'],
                spreads['homeSpread'],
                spreads['awayPrice'],
                spreads['awaySpread']
            ])

            stakes['spreads'] = [
                [0, mkts['maxBetAmount_spread']],
                [0, mkts['maxBetAmount_spread']],
            ]

        if mkts.get('total'):
            # There may be multiple totals. Choose the middle
            total_index = int(len(mkts['total']) / 2)
            totals = mkts['total'][total_index]
            odds['totals'] = market_cleaner.totals_clean([
                totals['underPrice'],
                'under ' + totals['points'],
                totals['overPrice'],
                'over ' + totals['points'],
            ])

            stakes['totals'] = [
                [0, mkts['maxBetAmount_totalPoints']],
                [0, mkts['maxBetAmount_totalPoints']],
            ]

        results.append({
            'teams': teams,
            'odds': odds,
            'stakes': stakes,
            'commence_time': event['startDateTime'],
            'home_team': event['homeTeam_name']
        })

    return results
