import json

mkt_map = {
    'Match Betting (Inc ET)': 'h2h',
    'Winner': 'h2h',
    'Match Betting': 'h2h',
    'Match Result': 'h2h',
    'Match Betting (With Overtime)': 'h2h',
    'Head To Head': 'h2h',
    'Head to Head': 'h2h',
    'Three Way Money Line': 'h2h',
    'Match Winner': 'h2h'
}


def neds23(html, params, isTest=False):

    jdata = json.loads(html)
    results = []

    jdata['prices'] = restructure_prices(jdata['prices'])

    for event_id in jdata['events']:
        event = jdata['events'][event_id]
        start_time = event['actual_start']['seconds']
        # event_name = event['name']
        # event_location = event['location']

        home_team = ''
        teams = []
        odds = {}

        for mkt_id in event['main_markets']:
            mkt = jdata['markets'][mkt_id]
            oddD = 0

            if mkt['name'] not in mkt_map:
                # Uknown market
                if isTest:
                    print 'Unknown market', mkt['name']
                continue

            for participant_id in mkt['entrant_ids']:
                participant = jdata['entrants'][participant_id]

                # Resolve teams
                if mkt_map[mkt['name']] == 'h2h' and participant['name'] != 'Draw':
                    # This condition means h2h must be present in order to collect other markets
                    # Could replace 'h2h' with list of markets that have team names
                    teams.append(participant['name'])
                    if participant.get('home_away', '') == 'HOME':
                        home_team = participant['name']

                # Resolve odds
                odd_components = jdata['prices'][participant_id]['odds']

                if mkt_map[mkt['name']] == 'h2h':
                    if 'h2h' not in odds:
                        odds['h2h'] = []

                    price = round(1 + odd_components['numerator'] * 1.00 / odd_components['denominator'], 2)
                    if participant['name'] == 'Draw':
                        oddD = price
                    else:
                        odds['h2h'].append(price)

                elif mkt_map[mkt['name']] == 'spreads':
                    if 'spreads' not in odds:
                        odds['spreads'] = {}
                    # TODO
                    pass

                elif mkt_map[mkt['name']] == 'totals':
                    if 'totals' not in odds:
                        odds['totals'] = {}
                    # TODO
                    pass

            if mkt_map[mkt['name']] == 'h2h' and oddD:
                odds['h2h'].append(oddD)

        if not teams:
            continue
        
        for mkt in odds:
            # Cleanup odds, otherwise validator trips
            if not odds[mkt]:
                odds.pop(mkt, None)

        results.append({
            'teams': teams,
            'odds': odds,
            'commence_time': start_time,
            'home_team': home_team
        })

    return results

                 

def restructure_prices(prices):
    # The prices dictionary key looks like "entrant_id:some_other_id"
    #   where some_other_id appears in every price key, but nowhere else
    # I say get rid of it

    new_prices = {}

    for k in prices:
        new_prices[ k.split(':')[0] ] = prices[k]

    return new_prices