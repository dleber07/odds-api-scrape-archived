
ENV = 'dev' # dev | prod

MONGO_1 = {
    'ENV': ENV,
    'HOST': '',
    'PORT': 27017,
    'USER': '',
    'PASS': ''
}

MONGO_TEST = {
    'ENV': ENV,
    'HOST': '',
    'PORT': 27017,
    'USER': '',
    'PASS': ''
}

REDIS_1 = {
    'ENV': ENV,
    'HOST':'',
    'PORT': '6379'
}

# For testing
API_KEY =''

# Google sheets id where scrape status is sent to
SCRAPE_STATUS_SPREADSHEET_ID = ''
