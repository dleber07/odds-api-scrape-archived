from components.Configs import Configs
import unittest

class TestConfigs(unittest.TestCase):

    def setUp(self):
        self.configs = Configs()

    def test_no_proxy(self):
        self.assertEqual(self.configs._format_proxy({}), {})

    def test_delimeted_proxy(self):
        site_config = {
            'proxy': 'https___0.0.0.0'
        }

        self.assertEqual(
            self.configs._format_proxy(site_config),
            {'https': '0.0.0.0'}
        )
    
    def test_non_delimited_proxy(self):
        site_config = {
            'proxy': '0.0.0.0'
        }

        self.assertEqual(
            self.configs._format_proxy(site_config),
            {'https': '0.0.0.0'}
        )


if __name__ == '__main__':
    unittest.main()
