from components.validate import normalize_team, resolveParticipant
import unittest
import fakeredis

class TestValidate(unittest.TestCase):

    def setUp(self):
        server = fakeredis.FakeServer()
        self.redisdb = fakeredis.FakeStrictRedis(server=server)

    def test_normalize_team(self):
        self.assertEqual(normalize_team('\nSoMe TeaM '), 'some team')
        self.assertEqual(normalize_team('\t  SoMe TeaM '), 'some team')

    def test_resolve_participant(self):
        sport = 'sport'
        pmap_name = 'sport:site'
        participant_list = [
            'Mista Dobalina',
            'pepper',
        ]

        # create pmaps
        self.redisdb.hmset('participantMap:' + pmap_name, {'team_key_site': 'found in site pmap'})
        self.redisdb.hmset('participantMap:' + sport, {'team_key_sport': 'found in sport pmap', 'team_key_site': 'found in sport pmap'})

        # test site pmap override
        # ensure this team is extracted from the site pmap
        team = 'team_key_site'
        self.assertEqual(
            resolveParticipant(self.redisdb, team, participant_list, sport, pmap_name),
            'found in site pmap'
        )
        
        # test sport pmap hit
        team = 'team_key_sport'
        self.assertEqual(
            resolveParticipant(self.redisdb, team, participant_list, sport, pmap_name),
            'found in sport pmap'
        )

        # test fuzzy good match
        team = 'm dobalina'
        self.assertEqual(
            resolveParticipant(self.redisdb, team, participant_list, sport, pmap_name),
            'Mista Dobalina'
        )

        # ensure the fuzzy result was cached in the sport pmap
        self.assertEqual(
            self.redisdb.hget('participantMap:' + sport, team),
            'Mista Dobalina'
        )

        # ensure the fuzzy result was not cached in the site pmap
        #  as this is for manual overring only
        self.assertIsNone(
            self.redisdb.hget('participantMap:' + pmap_name, team)
        )

        # test fuzzy no match
        team = 'fsdfffff'
        self.assertIsNone(
            resolveParticipant(self.redisdb, team, participant_list, sport, pmap_name),
        )

        # ensure we captured the unmatched team
        self.assertTrue(
            self.redisdb.sismember('nameMismatch:' + pmap_name, team)
        )

        # test none handling
        team = None
        self.assertIsNone(
            resolveParticipant(self.redisdb, team, participant_list, sport, pmap_name),
        )

if __name__ == '__main__':
    unittest.main()
