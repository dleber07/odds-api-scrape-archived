from components.validate import reverse_odds, reverse_totals
import unittest

class TestReverseOdds(unittest.TestCase):

    def test_reverse_h2h(self):
        odds = {
            'h2h': [1, 2, 9876]
        }

        self.assertEqual(reverse_odds(odds)['h2h'], [2, 1, 9876])

    def test_reverse_h2h_lay(self):
        odds = {
            'h2h_lay': [7.45, 2.876, 456.43]
        }

        self.assertEqual(reverse_odds(odds)['h2h_lay'], [2.876, 7.45, 456.43])

    def test_reverse_spreads(self):
        odds = {
            'spreads': {
                'odds': [11.22, 33.44],
                'points': ['5.5', '-5.5']
            }
        }

        self.assertEqual(reverse_odds(odds)['spreads'], {
            'odds': [33.44, 11.22],
            'points': ['-5.5', '5.5']
        })


    def test_reverse_totals(self):
        # should reverse since ordered by under / over
        odds = {
            'totals': {
                'position': ['under', 'over'],
                'odds': [11.22, 33.44],
                'points': ['242', '241']
            }
        }

        self.assertEqual(reverse_totals(odds['totals']), {
            'position': ['over', 'under'],
            'odds': [33.44, 11.22],
            'points': ['241', '242']
        })


if __name__ == '__main__':
    unittest.main()
