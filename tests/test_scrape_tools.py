from components.validate import frac_to_decimal
from components.scrape_tools import datetime_to_unix
import unittest

class TestScrapeTools(unittest.TestCase):

    def test_frac_to_decimal(self):
        self.assertEqual(frac_to_decimal(12, 5), 3.4)

    def test_without_timezone(self):
        self.assertEqual(datetime_to_unix('2019-09-09 03:00:00', '%Y-%m-%d %H:%M:%S'), 1567998000)

    def test_with_timezone(self):
        timezone = 'Australia/Sydney'
        self.assertEqual(datetime_to_unix('2019-09-09 03:00:00', '%Y-%m-%d %H:%M:%S', timezone), 1567962000)


if __name__ == '__main__':
    unittest.main()
