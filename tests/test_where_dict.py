from components.scrape_tools import where_dict
import unittest

class TestWhereDict(unittest.TestCase):

    def setUp(self):
        self.group_dicts = [
            {
                'name': 'sandra',
                'height': 564,
                'flag': True,
            },
            {
                'name': 'mark',
                'height': 654,
                'flag': True,
            },
            {
                'name': 'orange',
                'height': 23,
                'flag': False,
            },
        ]

    def test_filters_with_results(self):
        result = where_dict(self.group_dicts, 'flag', True)
        self.assertEqual(len(result), 2)
        self.assertListEqual(result, self.group_dicts[:2])
        

    def test_filters_without_results(self):
        result = where_dict(self.group_dicts, 'name', 'orion')
        self.assertListEqual(result, [])


if __name__ == '__main__':
    unittest.main()
