from results import main
from connectors.mongo_connect import Mongo
from connectors.redis_connect import get_redis_client

from . import rsettings as result_settings
from components.Configs import Configs
import settings


configs = Configs()
configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

print(main.get_live_sports(Mongo['scrape'], result_settings))

# site_config = configs.get_result_scrape( '{}:{}'.format('soccer_epl', 'betfair') )

configs = Configs()
configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

dbs = {
    'mongo': Mongo[settings.MONGO_EVENTS_DB],
    'redis': get_redis_client(db=settings.REDIS_STATUS_DB)  
}

main.main(dbs, configs)
