
SITE_PREFERENCES = [
    'betfair', # 1st
    'unibet' # 2nd
]


# Sleep time of the infinite while loop
WHILE_SLEEP_TIME_S = 5


# Accept events if commence time updated within the last mins
# Allow lots of time after live events to continue to collect results
# Odds might no longer get updated
COMMENCE_UPDATE_WITHIN_MINS = 120

# Accept events as live if within 5 minutes of their commence time
LIVE_IF_MINS_BEFORE_COMMENCE = 30

# Reject live events if live for longer than this
LIVE_FOR_MAX_HOURS = 72


# Redis expiry of http response key
#   Should be less than WHILE_SLEEP_TIME_S since the same response is used for multiple sports (reduces fetches)
#   but we still want it to be fresh on while loop
CACHE_EXPIRY_S = WHILE_SLEEP_TIME_S

