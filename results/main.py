import time
import importlib

# Scrape modules (to be replaced if results made into separate repo)
import settings
from components.Configs import Configs

from fetchers.default import fetch
from connectors.redis_connect import get_redis_client
from connectors.mongo_connect import Mongo
from env import ENV

# Results modules
from . import rsettings as result_settings
from .rcomponents import writer
from .rcomponents import tools

try:
    import sentry_sdk
    sentry_sdk.init("https://5b588b4da17e46ee9e7b6cc4c9d9c3f0@sentry.io/1393961")
except:
    print 'Failed to import sentry sdk'



def get_live_sports(mongodb, settings):
    """
    Args:
        mongodb (class) MongoDB cli
        settings (dict)
    
    Returns:
        list of sport keys with live events
    """

    pipeline = [
        {'$match': {
            'commence_time': { 
                '$lte': time.time() + settings.LIVE_IF_MINS_BEFORE_COMMENCE * 60,
                '$gte': time.time() - settings.LIVE_FOR_MAX_HOURS*3600 
            },
            'last_update': {'$gte': time.time() - settings.COMMENCE_UPDATE_WITHIN_MINS * 60 }}
        }, # consider 5 mins prematch live
        {'$project': {'sport_key': 1}},
        {'$group': {'_id' : '$sport_key'}}
    ]

    return [result['_id'] for result in mongodb['events'].aggregate(pipeline)]


def get_http_response(redisdb, settings, site_config, scrape_config):
    """
        Args:
            redisdb (oject)
            site_config (dict)
            scrape_config (dict)

        Returns (str)
    """
    site_key = site_config['key']
    response = redisdb.get('httpresponse:' + site_key)
    if not response:
        response = fetch(site_config, scrape_config)
        redisdb.setex('httpresponse:' + site_key, settings.CACHE_EXPIRY_S, response)

    return response


def get_site_configs(site_key):
    site_configs = {
        'betfair': {
            'key': 'betfair',
            "headers": {
                "Origin":"https://ls.betradar.com",
                "Referer":"https://ls.betradar.com/ls/livescore/?/betfair/en/page",
                "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
            },
            "ua":"chrome",
            "proxy_key":"",
            "proxy": {}
        },
    }

    return site_configs.get(site_key)

def main(dbs, configs, verbose=False):

    for sport_key in get_live_sports(dbs['mongo'], result_settings):

        sport_config = configs.get_sports()[sport_key]
        events_validated = []

        for site_key in result_settings.SITE_PREFERENCES:

            params = {
                'sport_config': sport_config,
                'site_config': get_site_configs(site_key), # NOTE hard coded - in future, consider configs.get_results_sites()
                'scrape_config': configs.get_result_scrape( '{}:{}'.format(sport_key, site_key) )
            }

            if not params['scrape_config']:
                if verbose:
                    print 'Missing result scrape config for ', sport_key, site_key
                continue

            response = get_http_response(
                dbs['redis'],
                result_settings,
                params['site_config'],
                params['scrape_config']
            )

            if not response:
                continue

            reader = importlib.import_module('results.readers.' + site_key)
            events_raw = reader.read(response, params)

            if not events_raw:
                continue

            # validate team names
            events_validated = tools.validate_result_events(params, events_raw)

            # lookup event ids
            for event in events_validated:
                event_id = tools.resolve_event_id(dbs['mongo'], event)
                if event_id:
                    # event id might be missing. This can happen towards end of game, where event no longer has odds and is removed, but scores still updating.
                    # 
                    event['event_id'] = event_id
                else:
                    # TODO
                    #   If not found, we have not choice but to ignore. Log it somewhere
                    pass
                
                event['sport_key'] = sport_key

            if verbose:
                for event in events_validated:
                    print
                    print sport_key, event

            if not events_validated:
                continue
            
            # We have valid results - no need to run other sites
            # Break the loop and write to db
            break

        if not events_validated:
            # try:
            #     sentry_sdk.capture_message('No results for ' + sport_key)
            # except:
            #     pass

            continue

        response = writer.write_results(dbs, params, events_validated)
        if verbose:
            print
            print sport_key
            print 'RESULTS'
            print 'matched', response['results'].matched_count
            print 'modified', response['results'].modified_count
            print 'upserted', response['results'].upserted_count
            print 'inserted', response['results'].inserted_count
            print
            print 'RESULTS'
            print 'matched', response['events'].matched_count
            print 'modified', response['events'].modified_count
            print 'upserted', response['events'].upserted_count
            print 'inserted', response['events'].inserted_count


if __name__ == "__main__":

    configs = Configs()
    configs.set_db(Mongo[settings.MONGO_CONFIG_DB])

    dbs = {
        'mongo': Mongo[settings.MONGO_EVENTS_DB],
        'redis': get_redis_client(db=settings.REDIS_STATUS_DB)  
    }

    verbose = ENV == 'dev'

    while True:
        main(dbs, configs, verbose)
        time.sleep(result_settings.WHILE_SLEEP_TIME_S)
