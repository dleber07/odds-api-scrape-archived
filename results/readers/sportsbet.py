import json

"""

    End time not available (only available for quarters, not for entire game)

"""

def _get_event_fields(period):
    """
    Args:
        period (dict)
    Returns:
        dict

    """
    return {
            'teams': [
                event['teams']['home']['name'],
                event['teams']['away']['name']
            ],
            'home_team': event['teams']['home']['name'],
            'commence_time': period['startTime'],
            # 'end_time': int(event['timeinfo']['ended']) if event.get('timeinfo') and event['timeinfo'].get('ended') else None,

            'scores': [
                event['result']['home'],
                event['result']['away'],
            ],

            'winning_team': winning_team

    }

def _get_period_fields():
    return {}

def read(html, params):
    events = json.loads(html)
    results = []

    for event in events:

        doc = {}
        for period in event['eventPeriods']:

            if period['periodCode'] == 'ALL':
                doc.update(
                    _get_event_fields(period)
                )



        for region in sport['realcategories']:

            if str(region['_id']) != region_id:
                continue

            for tournament in region['tournaments']:

                if tournament_id == str(tournament['_id']):
                    target_tournament = tournament
                    break

    for event in target_tournament.get('matches', []):

        if event['result']['winner'] == None:
            winning_team = None
        elif event['result']['winner'] == 'home':
            winning_team = event['teams']['home']['name']
        else:
            winning_team = event['teams']['away']['name']

        results.append({
            'teams': [
                event['teams']['home']['name'],
                event['teams']['away']['name']
            ],
            'home_team': event['teams']['home']['name'],
            'commence_time': event['_dt']['uts'],
            'end_time': int(event['timeinfo']['ended']) if event.get('timeinfo') and event['timeinfo'].get('ended') else None,
            'match_status': event['matchstatus'], # options? live | result | recentlyended
            'match_period': event['status']['name'], # optoins? 2nd half | Ended
            'scores': [
                event['result']['home'],
                event['result']['away'],
            ],
            'periods': _format_periods(event['periods'], event['teams']),
            'winning_team': winning_team
        })

    return results


def _format_periods(periods, teams):
    formatted_periods = {}
    for period_key in periods:
        formatted_periods[period_key] = [
            periods[period_key]['home'], # teams['home']['name']: 
            periods[period_key]['away'], # teams['away']['name']: 
        ]

    return formatted_periods
