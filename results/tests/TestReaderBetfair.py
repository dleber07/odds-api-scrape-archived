from results.readers import betfair
import unittest

class TestReaderBetfair(unittest.TestCase):

    def test_reader(self):
        with open('results/tests/data/betfair.txt') as file:
            data = file.read()

        results = betfair.read(data, 
            {
                'scrape_config': {
                    'custom1': '1_67_127'
                }
            })

        self.assertEquals(len(results), 3)
        self.assertIs(type(results[0]['scores']), list)
        self.assertIs(type(results[0]['teams']), list)
        self.assertTrue('home_team' in results[0])


if __name__ == '__main__':
    unittest.main()
