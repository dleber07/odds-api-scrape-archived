from bson import ObjectId, son
import pymongo
import time

def get_write_doc(params, event):
    return son.SON([
        ('event_id', event['event_id']),
        ('sport_key', params['sport_config']['key']),
        ('sport_nice', params['sport_config']['title']),
        ('sport_group_key', params['sport_config'].get('group_key')),
        ('sport_group', params['sport_config']['group']),
        ('name', event['name']),
        ('teams', event['teams']),
        ('home_team', event.get('home_team', '')),
        ('winning_team', event.get('winning_team', '')),
        ('commence_time', event['commence_time']),
        ('end_time', event['end_time']),
        ('match_status', event['match_status']),
        ('match_period', event['match_period']),
        ('scores', event['scores']),
        ('periods', event['periods']),

        ('last_update_site_key', params['site_config']['key']),
        ('last_update', int(time.time()))
    ])


def write_results(dbs, params, events):
    sport_config = params['sport_config']
    site_config = params['site_config']
    queries_results = []
    queries_events = []

    for event in events:

        if not event.get('event_id'):
            # TODO REMOVE - event_id should always be present at this stage
            #   regardless of whether it's from results or events collection
            continue

        # Write scores to events
        # Only if score don't exist, or if scores have increased
        queries_events.append(pymongo.UpdateOne(
            son.SON([
                ('_id', ObjectId(event['event_id'])),
                ('commence_time', {'$lt': time.time()}),
                ('$or', [
                    {'scores': {'$exists': False}},
                    {'scores.0': {'$lt': event['scores'][0]}},
                    {'scores.1': {'$lt': event['scores'][1]}},
                ])
            ]),
            {'$set': son.SON([
                ('scores', event['scores']),
                ('scores_periods', event['periods']),
                ('scores_last_update', int(time.time())),
            ])},
            upsert=False
        ))


        # Write results
        # NOTE - we manually perform the upsert since pymongo would NOT preserve the key order of the OrderedDict object
        #   See if this behaviour changes in python 3 - if so, remove the find and insert operations, and set upsert=True in the update operation
        if dbs['mongo']['results'].count({'event_id': event['event_id']}):
            queries_results.append(pymongo.UpdateOne(
                {'event_id': event['event_id']},
                {'$set': get_write_doc(params, event)},
                upsert=False
            ))
        else:
            queries_results.append(
                pymongo.InsertOne(get_write_doc(params, event))
            )


    if queries_results:
        response_results = dbs['mongo']['results'].bulk_write(queries_results)

    if queries_events:
        response_events = dbs['mongo']['events'].bulk_write(queries_events)

    return {
        'results': response_results,
        'events': response_events
    }

