from components.validate import resolveParticipant

def validate_result_events(params, events):
    sport_config = params['sport_config']
    scrape_config = params['scrape_config']
    particpant_map_name = scrape_config.get('participantmapname', scrape_config['key'])

    valid_result_events = []
    for event in events:

        teams = [resolveParticipant(team, sport_config['participant_list'], sport_config['key'], particpant_map_name) for team in event['teams']]
        
        if None in teams:
            continue

        if 'home_team' in event:
            event['home_team'] = resolveParticipant(event['home_team'], sport_config['participant_list'], sport_config['key'], particpant_map_name)
        
        if 'winning_team' in event:
            event['winning_team'] = resolveParticipant(event['winning_team'], sport_config['participant_list'], sport_config['key'], particpant_map_name)
        
        event['teams'] = teams
        event['name'], teams_reversed = make_event_name(*event['teams'])

        if teams_reversed:
            event['teams'] = event['teams'][::-1]
            event['scores'] = event['scores'][::-1]
            for period in event.get('periods', []):
                event['periods'][period] = event['periods'][period][::-1]


        if event.get('scores') and None in event['scores']:
            event['scores'] = [(score or 0) for score in event['scores']]

        valid_result_events.append(event)

    return valid_result_events


def resolve_event_id(db, event):

    valid_commence_range_hours = 4

    query = {
        'name': event['name'],
        'commence_time': { 
            '$gt': int(event['commence_time']) - ( valid_commence_range_hours * 3600 ),
            '$lt': int(event['commence_time']) + ( valid_commence_range_hours * 3600 )
         }
    }

    result = db['events'].find_one(query, projection=['_id'])

    if result:
        return str(result.get('_id', ''))

    # The event is not found in the events collection.
    # This could be because odds are no longer listed (near event end)
    # Check if it's already in the results collection
    result = db['results'].find_one(query, projection=['event_id'])
    
    if result:
        return result.get('event_id', '')

    return None


def make_event_name(team1, team2):
    """
    Concatenates teams in alphabetical order

    Args:
        team1 (str)
        team2 (str)
    Returns:
        Tuple of concatenated teams and flag indicating if they were reversed

    """

    if team1.replace(' ', '').lower() < team2.replace(' ','').lower():
        return (team1 + '_' + team2, False)

    return (team2 + '_' + team1, True)